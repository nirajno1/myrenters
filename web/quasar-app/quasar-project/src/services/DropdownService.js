import { api } from 'boot/axios'
export async function fetchDropdownElectricMeters() {

  const response = await api.get('/dropdown/emeters');
  return response.data;
}
export async function fetchDropdownFlats() {

  const response = await api.get('/dropdown/flats');
  return response.data;
}
export async function fetchDropdownRenters() {

  const response = await api.get('/dropdown/renters');
  return response.data;
}

export async function saveDropdownMaster(dropdownMaster) {
  const response = await api.post('/dropdown/dd', dropdownMaster);
  return response.data;
}

export async function fetchDropdownMaster(name) {

  const response = await api.get('/dropdown/dd/' + name);
  return response.data;
}

export async function fetchAllDropdownMaster() {

  const response = await api.get('/dropdown/dd');
  return response.data;
}



export async function fetchDropdownMasterVals(name) {

  const response = await api.get('/dropdown/dd/' + name + '/ddval');
  return response.data;
}
