import { api } from 'boot/axios'

export async function getAllRenters() {
  const response = await api.get('/renter');

  return response.data;
}
export async function getAllRentersList() {

  const response = await api.get('/renter/list');
  return response.data;
}

export async function getAllRentersNonDeleted() {
  const response = await api.get('/renter/listPage');
  return response.data;
}

export async function getRenterById(renterId) {

  const response = await api.get('/renter/' + renterId);
  return response.data;
}
/* This service will bring only 5 child records */
export async function findRenterPreviewById(renterId) {
  const response = await api.get('/renter/preview/' + renterId);
  return response.data;
}
export async function saveRenter(renter) {

  const response = await api.post('/renter', renter);
  return response.data;
}


export async function deleteRenterById(id) {
  const response = await api.delete('/renter/' + id);
  return response.data;
}

export async function getImage(imageName) {
  const response = await api.get('/renter/img/' + imageName);
  return response.data;
}
