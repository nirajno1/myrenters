import { api } from 'boot/axios'

export async function fetchAllInvoices() {
  const response = await api.get('/invoice');
  return response.data;
}

export async function getInvoiceById(invoiceId) {
  const response = await api.get('/invoice/' + invoiceId);
  return response.data;
}

export async function saveInvoicePayment(invoicePayment) {
  const response = await api.post('/invoice', invoicePayment);
  return response.data;
}


export async function getBalanceDueById(invoiceId) {
  const response = await api.get('/invoice/balanceDue/' + invoiceId);
  return response.data;
}
