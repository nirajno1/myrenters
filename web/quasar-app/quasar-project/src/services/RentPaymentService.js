import { api } from 'boot/axios'

export async function saveRentflat(rentFlat) {

  const response = await api.post('/rentflat', rentFlat);
  return response.data;
}


export async function getAllRentPayments() {

  const response = await api.get('/rentflat');
  return response.data;
}

export async function saveMeterReadings(meterDetail) {
  const response = await api.post('/elecmeter/em', meterDetail);
  return response.data;
}


export async function getAllPaymentDuesByRenterId(renterId) {
  const response = await api.get('/rentPayment/rentDueDtlByRenter/' + renterId);
  return response.data;
}

export async function getAllPaymentDuesByFlatId(flatId) {
  const response = await api.get('/rentPayment/rentDueDtlByFlat/' + flatId);
  return response.data;
}


export async function acceptPayment(paymentRequest) {
  const response = await api.post('/rentPayment/acceptPayment', paymentRequest);
  return response.data;
}
