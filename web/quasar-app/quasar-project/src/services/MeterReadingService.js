import { api } from 'boot/axios'

export async function fetchAllElectricMeterNames() {
  const response = await api.get('/elecmeter/used');
  return response.data;
}
export async function findLatest5MeterReadingsById(id) {
  const response = await api.get(('/elecmeter/' + id + '/readings'));
  return response.data;
}
export async function saveMeterReading(meterReading) {
  const response = await api.post('/meterReading', meterReading);
  return response.data;
}
