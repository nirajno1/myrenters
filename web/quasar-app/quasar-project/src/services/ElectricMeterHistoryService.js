import { api } from 'boot/axios'
export async function getElectricMetersByMeterId(meterId) {
  const response = await api.get('/emsearch/queryByMeter/' + meterId);
  return response.data;
}

