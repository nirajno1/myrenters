import { api } from 'boot/axios'

export async function getAllElectricMeters() {

  const response = await api.get('/elecmeter');
  return response.data;
}
export async function getAllUnUsedElectricMeters() {

  const response = await api.get('/elecmeter/unused');
  return response.data;
}
export async function saveElectricMeter(electricmeter) {

  const response = await api.post('/elecmeter', electricmeter);
  return response.data;
}


export async function getElectricMeterById(id) {

  const response = await api.get(('/elecmeter/' + id));
  return response.data;
}
export async function deleteElectricMeterById(id) {

  const response = await api.delete(('/elecmeter/' + id));
  return response.data;
}
export async function getAllValidElectricMeters() {

  const response = await api.get(('/elecmeter/valid'));
  return response.data;
}
