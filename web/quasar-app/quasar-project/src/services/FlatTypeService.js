import { api } from 'boot/axios'

export async function getAllFlatTypes() {

  const response = await api.get('/flatType');
  return response.data;
}
export async function saveFlatType(flattype) {

  const response = await api.post('/flatType', flattype);
  return response.data;
}
