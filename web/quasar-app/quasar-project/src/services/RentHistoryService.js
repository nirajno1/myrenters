import { api } from 'boot/axios'

export async function getRentByRenterId(renterId) {
  const response = await api.get('/rentsearch/queryByRenter/' + renterId);
  return response.data;
}

export async function getRentByFlatId(flatId) {
  const response = await api.get('/rentsearch/queryByFlat/' + flatId);
  return response.data;
}

