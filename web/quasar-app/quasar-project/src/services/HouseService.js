import { api } from 'boot/axios'
export async function getAllHouses() {

  const response = await api.get('/house');
  return response.data;
}

export async function getHouseById(id) {

  const response = await api.get('/house/' + id);
  return response.data;
}
export async function getAvailabeFlatsByHouseId(id) {

  const response = await api.get('/house/avlFlats/' + id);
  return response.data;
}
export async function saveHouse(house) {
  const response = await api.post('/house', house);
  return response.data;
}


export async function deleteHouseById(id) {
  const response = await api.delete('/house/' + id);
  return response.data;
}

export async function getAllValidHouses() {
  const response = await api.get('/house/valid');
  return response.data;
}
