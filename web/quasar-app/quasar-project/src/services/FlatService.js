import { api } from 'boot/axios'

export async function getAllFlats() {

  const response = await api.get('/flat');
  return response.data;
}
export async function saveFlat(flat) {

  const response = await api.post('/flat', flat);
  return response.data;
}
export async function getAllFlatsList() {

  const response = await api.get('/dropdown/flats');
  return response.data;
}


export async function getFlatById(id) {

  const response = await api.get('/flat/' + id);
  return response.data;
}



export async function deleteFlatById(id) {
  const response = await api.delete('/flat/' + id);
  return response.data;
}

export async function getAllValidFlats() {
  const response = await api.get('/flat/valid');
  return response.data;
}
