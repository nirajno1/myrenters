import { api } from 'boot/axios'

export async function getAllStates(coutnryCode) {
  const response = await api.get('/master/states/' + coutnryCode);
  return response.data;
}

export async function getAllStatesCities(coutnryCode, stateName) {
  const response = await api.get('/master/states/' + coutnryCode + '/' + stateName);
  return response.data;
}
export async function getAllCountries() {
  const response = await api.get('/master/countries');
  return response.data;
}

