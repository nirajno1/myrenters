import { api } from 'boot/axios'

export async function fetchAllDropdownVals(ddkey) {
  const response = await api.get('/dropdown/ddvals/' + ddkey);
  return response.data;
}

export async function fetchAllValidDropdowns(ddkey) {
  const response = await api.get('/dropdown/ddval/valid/' + ddkey);
  return response.data;
}

export async function getDropdownById(id) {
  const response = await api.get('/dropdown/ddval/' + id);
  return response.data;
}

export async function saveDropdownVal(valueObject) {
  const response = await api.post('/dropdown', valueObject);
  return response.data;
}

export async function fetchAllDropdownKeyVals() {
  const response = await api.get('/dropdown/dd/ddKeys');
  return response.data;
}

export async function deleteDdById(id) {
  const response = await api.delete('/dropdown/' + id);
  return response.data;
}

