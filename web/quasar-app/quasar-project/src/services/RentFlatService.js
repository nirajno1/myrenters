import { api } from 'boot/axios'

export async function saveRentflat(rentFlat) {
  console.log("got in saveRentFlat", rentFlat);
  const response = await api.post('/rentflat', rentFlat);
  return response.data;
}


export async function getAllRentflats() {

  const response = await api.get('/rentflat/view');
  return response.data;
}
export async function getAllValidRentflats() {

  const response = await api.get('/rentflat/validView');
  return response.data;
}
export async function getRentflatById(id) {

  const response = await api.get('/rentflat/' + id);
  return response.data;
}

export async function queryByFlatActualRent(id) {

  const response = await api.get('/rentsearch/queryFlatActualRent/' + id);
  return response.data;
}
export async function deleteRentflatById(id) {

  const response = await api.delete('/rentflat/' + id);
  return response.data;
}
