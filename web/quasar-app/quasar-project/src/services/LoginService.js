import { api } from 'boot/axios'

export async function login(loginObj) {
  const response = await api.post('/auth/jwtlogin', loginObj);
  return response.data;
}

export async function registerAppUser(registerObj) {
  const response = await api.post('/auth/register', registerObj);
  return response.data;
}

export async function findAllAppUser() {
  const response = await api.get('/findAllAppUser');
  return response.data;
}
