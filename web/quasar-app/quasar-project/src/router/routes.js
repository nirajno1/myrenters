
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: 'flat', component: () => import('pages/flat/Flat_management.vue') },
      { path: 'flattype', component: () => import('pages/flattype/FlatTypeView.vue') },
      { path: 'emeter', component: () => import('pages/ElectricMeter/ElectricMeter_management.vue') },
      { path: 'rentflat', component: () => import('pages/RentFlat/Rentflat_management.vue') },
      { path: 'rentpayment', component: () => import('pages/RentPayment/RentPaymentView.vue') },
      { path: 'meterReadingHistory', component: () => import('pages/ElectricMeterHistory/ElectricMeterHistory.vue') },
      { path: 'rentHistory', component: () => import('pages/PaymentHistory/PaymentHistory.vue') },
      { path: 'invoice', component: () => import('pages/Invoice/InvoiceManager.vue') },
      { path: 'house', component: () => import('pages/House/house_management.vue') },
      { path: 'ddValList', component: () => import('pages/DropDownMasterValue/DropDownManager.vue') },
      { path: 'renterManagement', component: () => import('pages/renter/renter_management.vue') },
      { path: 'meterReading', component: () => import('pages/MeterReading/MeterReadingMangement.vue') },
      { path: 'login', component: () => import('pages/Login/LoginPage.vue') },
      { path: 'register', component: () => import('pages/Login/RegisterUser.vue') },
      { path: 'homeCheck', component: () => import('pages/Login/HomeCheck.vue') },
      { path: 'forgotPassword', component: () => import('pages/Login/ForgotPassword.vue') },
    ]
  },


  // Always leave this as last one, src/pages/RentFlat/Rentflat_management.vue
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
