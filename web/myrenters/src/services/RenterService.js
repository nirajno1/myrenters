const axios = require('axios');

export async function getAllRenters() {

    const response = await axios.get('http://localhost:8080/renter');
    return response.data;
}
