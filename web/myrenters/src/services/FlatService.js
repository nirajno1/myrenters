const axios = require('axios');

export async function getAllFlats() {

    const response = await axios.get('http://localhost:8080/flat');
    return response.data;
}

export async function createFlat(data) {
    console.log('Create flat called with data : ', data);
   const response = await axios.post('http://localhost:8080/flat', data);
    return response;
}