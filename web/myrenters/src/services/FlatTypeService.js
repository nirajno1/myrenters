const axios = require('axios');

export async function getAllFlatTypes() {

    const response = await axios.get('http://localhost:8080/flatType');
    return response.data;
}

export async function createFlatType(data) {
    const response = await axios.post('http://localhost:8080/flatType', data);
    return response;
}