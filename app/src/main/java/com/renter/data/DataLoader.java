package com.renter.data;

import com.renter.master.entity.master.City;
import com.renter.master.entity.master.Country;
import com.renter.master.entity.master.State;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * DataLoader of demo
 *
 * @since 11-Jan-2023 4:26 PM
 */
public class DataLoader {
    private static final String COUNTRY = "/home/neeraj.kumar/devCode/myRenters/app/src/main/resources/data/country.csv";
    private static final String STATE = "/home/neeraj.kumar/devCode/myRenters/app/src/main/resources/data/country_states.csv";
    private static final String CITY = "/home/neeraj.kumar/devCode/myRenters/app/src/main/resources/data/IN.txt";

    public static List<State> getAllStates() {
        File state = new File(STATE);
        List<State> states = null;
        try (BufferedReader br = new BufferedReader(new FileReader(state))) {
            states = br.lines().map(DataLoader::processState).collect(Collectors.toList());
        } catch (IOException e) {
            //ToDo add exception handler
        }
        return states;
    }

    public static List<Country> getAllCountries() throws FileNotFoundException {
        File country = new File(COUNTRY);
        List<Country> countryList = null;
        try (BufferedReader br = new BufferedReader(new FileReader(country))) {
            countryList = br.lines().skip(1).map(DataLoader::processCountry).collect(Collectors.toList());
        } catch (IOException e) {
            //ToDo add exception handler
        }

        return countryList;
    }

    private static Country processCountry(String csvRow) {
        String[] columns = csvRow.split(",");
        Country country = new Country();
        if (columns.length > 0)
            country.setCodeISO31661(columns[0]);
        if (columns.length > 1)
            country.setName(columns[1]);
        return country;
    }

    private static State processState(String csvRow) {
        String[] columns = csvRow.split(",");
        State state = new State();
        if (columns.length > 0)
            state.setCountryCode(columns[0]);

        if (columns.length > 1)
            state.setCode(columns[1]);

        if (columns.length > 2)
            state.setName(columns[2]);

        if (columns.length > 3)
            state.setType(columns[3]);

        return state;
    }

    private static City processCity(String csvRow) {
        String[] columns = csvRow.split("\t");
        City city = new City();
        if (columns.length > 0)
            city.setCountryCode(columns[0]);
        if (columns.length > 1)
            city.setPostalCode(columns[1]);
        if (columns.length > 2)
            city.setPlaceName(columns[2]);
        if (columns.length > 3)
            city.setStateName(columns[3]);
        if (columns.length > 4)
            city.setStateCode(columns[4]);
        if (columns.length > 5)
            city.setCityName(columns[5]);
        if (columns.length > 6)
            city.setCityCode(columns[6]);
        if (columns.length > 7)
            city.setCommunityName(columns[7]);
        if (columns.length > 8)
            city.setCommunityCode(columns[8]);
        if (columns.length > 9)
            city.setLatitude(columns[9]);
        if (columns.length > 10)
            city.setLongitude(columns[10]);
        if (columns.length > 11)
            city.setAccuracy(columns[11]);

        return city;
    }

    public static List<City> getAllIndianCityPostalCode() throws FileNotFoundException {
        File cityFile = new File(CITY);
        List<City> cities = null;
        try (BufferedReader br = new BufferedReader(new FileReader(cityFile))) {
            cities = br.lines().map(DataLoader::processCity).collect(Collectors.toList());
        } catch (IOException e) {
            //ToDo add exception handler
        }

        return cities;
    }

}
