package com.renter.data;

import java.util.Calendar;

/**
 * DummyDataGenerator of my-renter
 *
 * @author Neeraj Kumar
 * @since 29-Mar-2023 11:29 AM
 */
public class DummyDataGenerator {
    private static String[] fname = new String[]{"Neeraj", "Ghanshyam", "Summit",
            "Vivek", "Amit", "Ritik"
    };
    private static String[] lname = new String[]{
            "Kumar", "Sharma", "Tiwari", "Yadav"
    };
    private static String[] cities = new String[]{
            "Patna", "Danapur", "Samstipur", "Begusaria"
    };
    private static String[] occupations = new String[]{
            "Doctor", "Clerk", "Manager", "Engineer"
    };

    private static String[] hosueNames = new String[]{
            "Chaitanya Krupa",
            "Ananda Nilaya",
            "Chandrika Nivasa",
            "Suvarna Nilaya",
            "Akshaya Kuteera",
            "Ullasa Nilaya",
            "Chandra Kuteera",
            "Kusum"
    };
    private static String[] mobileNumbers = new String[]{
            "68226041474",
            "68228402797",
            "68223437389",
            "68226207442",
            "68292232460",
            "68151088876",
            "68225429909",
            "95222783334",
            "95402740579",
            "95222287065",
            "95802221200",
            "95112643942",
            "95222361466",
            "95265241175",
            "98222579018",
            "98222430113",
            "98222642034",
            "98022244575",
            "98112327902",
            "98112644185",
            "98222528640",
            "78222807614",
            "78222859402",
            "78222789613",
            "78442535426",
            "78124404246",
            "78161508519",
            "78406615171",
            "94222614552",
            "94222534674",
            "94222534856",
            "94022261128",
            "94112461160",
            "94222386463",
            "94112259884",
            "93222386691",
            "93222806232",
            "93792630785",
            "93222637024",
            "93222502567",
    };


    private static int generateRandomInt(int from, int to) {
        int num = (int) (from + ((to - from) * (Math.random())));

        return num;
    }

/*    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            System.out.println(generateRandomNumberString(8));
        }
    }*/
    public static String generateRandomName() {
        return fname[generateRandomInt(0, fname.length)].concat(" ").concat(lname[generateRandomInt(0, lname.length)]);
    }

    public static String generateRandomMobileNum() {
        return mobileNumbers[generateRandomInt(0, mobileNumbers.length)];
    }
    public static String generateRandomCity() {
        return cities[generateRandomInt(0, cities.length)];
    }
    public static String generateRandomOccupations() {
        return occupations[generateRandomInt(0, occupations.length)];
    }
    public static Calendar generateRandomDob() {
        int randomYear = generateRandomInt(1970, 2010);
       int dayOfYear = generateRandomInt(1, 365);
                Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, randomYear);
        calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
        return calendar;
    }

    public static Boolean generateRandomBoolean() {
        int randomInt = generateRandomInt(1, 2000)%2;
        return (randomInt == 1)?Boolean.TRUE:Boolean.FALSE;
    }

    public static String generateHouseName(){
        return hosueNames[generateRandomInt(0, hosueNames.length)];
    }

    public static char generateRandomWord(){
        return (char)generateRandomInt('A', 'Z');
    }
    public static String generateRandomHouseNumber(){
        String num=""+generateRandomWord();
        return num.concat("-").concat(String.valueOf(generateRandomInt(10,99)));
    }

    public static String generateRandomNumberString(int length){
        StringBuilder sb= new StringBuilder();
        sb.append(10);
        for (int i = 0; i < length; i++) {
            sb.append(generateRandomInt(0,5));
        }
        return sb.toString();
    }

    public static String generateRandomMeterType(){
        String[] types=new String[]{"Digital","Mechnical","Smart"};
        int choice= generateRandomInt(0,100)%types.length;
        return types[choice];
    }

    public static Integer generateRandomNumber(Integer from) {
        return  from+generateRandomInt(10,1000);
    }
}
