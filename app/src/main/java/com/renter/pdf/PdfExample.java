package com.renter.pdf;

import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.LineSeparator;
import com.itextpdf.layout.element.List;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.kernel.colors.Color;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static com.itextpdf.kernel.colors.ColorConstants.DARK_GRAY;

/**
 * PdfExample of demo
 *
 * @author Neeraj Kumar
 * @since 20-Mar-2023 2:50 PM
 */
public class PdfExample {
    public static String PDF_FILE="/app/out/pdf/sample.pdf";
    public static void main(String[] args) throws FileNotFoundException {
        OutputStream outputStream= new FileOutputStream(new File(PDF_FILE));
        PdfDocument pdfDocument= new PdfDocument(new PdfWriter(outputStream));
        Document document= new Document(pdfDocument);

        document.add(new Paragraph("This is paragraph"));

        document.add(new Paragraph("This is second paragraph"));
        // Creating a list
        List list = new List();

        // Add elements to the list
        list.add("Java");
        list.add("JavaFX");
        list.add("Apache Tika");
        list.add("OpenCV");
        list.add("WebGL");
        list.add("Coffee Script");
        list.add("Java RMI");
        list.add("Apache Pig");

        document.add(list);

        // Creating a table object
        float [] pointColumnWidths = {150F, 150F, 150F};
        Table table = new Table(pointColumnWidths);
        // Populating row 1 and adding it to the table
        Cell c1 = new Cell();                        // Creating cell 1
        c1.add(new Paragraph("Name"));                              // Adding name to cell 1
        c1.setBackgroundColor(ColorConstants.GREEN);      // Setting background color
        c1.setBorder(new SolidBorder(ColorConstants.RED, 3));
        c1.setTextAlignment(TextAlignment.CENTER);   // Setting text alignment
        table.addCell(c1);                           // Adding cell 1 to the table

        table.addCell(new Cell().add(new Paragraph("Id")));
        table.addCell(new Cell().add(new Paragraph("Designation")));
        table.addCell(new Cell().add(new Paragraph("Raju")));
        table.addCell(new Cell().add(new Paragraph("1001")));
        table.addCell(new Cell().add(new Paragraph("Programmer")));

        document.add(table);


        pdfDocument.close();
    }
}
