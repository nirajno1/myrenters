package com.renter.security.service;

import com.renter.security.pojo.JwtAuthenticationResponse;
import com.renter.security.pojo.AppUserDto;
import com.renter.security.pojo.SigninRequest;
import org.springframework.security.core.userdetails.UserDetails;

public interface AuthenticationService {
    JwtAuthenticationResponse signup(AppUserDto request);

    JwtAuthenticationResponse signin(SigninRequest request);

    UserDetails authenticate(SigninRequest input);
}
