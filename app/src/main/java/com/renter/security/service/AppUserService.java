package com.renter.security.service;

import com.renter.security.model.AppUser;
import com.renter.security.pojo.AppUserDto;

import java.util.List;

public interface AppUserService {
    AppUserDto saveUser(AppUserDto regUser);
    AppUserDto findByEmail(String email);

    List<AppUser> findAllAppUser();
}
