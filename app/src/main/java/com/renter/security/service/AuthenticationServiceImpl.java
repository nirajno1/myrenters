package com.renter.security.service;

import com.renter.security.pojo.JwtAuthenticationResponse;
import com.renter.security.pojo.AppUserDto;
import com.renter.security.pojo.SigninRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * AuthenticationServiceImpl of my-renter
 *
 * @author Neeraj Kumar
 * @since 14-May-2024 9:45 am
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private AuthenticationManager authenticationManager;

    private UserDetailsService userDetailsService;

    public AuthenticationServiceImpl(AuthenticationManager authenticationManager, UserDetailsService userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public JwtAuthenticationResponse signup(AppUserDto request) {
        return null;
    }

    @Override
    public JwtAuthenticationResponse signin(SigninRequest request) {
        return null;
    }

    public UserDetails authenticate(SigninRequest input) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        input.getUserName(),
                        input.getPassword()
                )
        );

        return userDetailsService.loadUserByUsername(input.getUserName());
    }
}
