package com.renter.security.service;

import com.renter.security.converter.AppUserConverter;
import com.renter.security.model.AppUser;
import com.renter.security.pojo.AppUserDto;
import com.renter.security.repository.AppUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * AppUserServiceImpl of my-renter
 *
 * @author Neeraj Kumar
 * @since 15-May-2024 10:41 am
 */
@Slf4j
@Service
public class AppUserServiceImpl implements AppUserService {

    private AppUserRepository appUserRepository;
    private PasswordEncoder passwordEncoder;

    public AppUserServiceImpl(AppUserRepository appUserRepository, PasswordEncoder passwordEncoder) {
        this.appUserRepository = appUserRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public AppUserDto saveUser(AppUserDto regUser) {
        AppUser appUserUnsaved= AppUserConverter.dtoToEntity(regUser);
        appUserUnsaved.setPassword(passwordEncoder.encode(appUserUnsaved.getPassword()));
        AppUser appUserSaved = appUserRepository.save(appUserUnsaved);

        return AppUserConverter.entityToDto(appUserSaved);
    }

    @Override
    public AppUserDto findByEmail(String email) {
        AppUser appUserSaved= appUserRepository.findByEmail(email).orElseThrow(()->new UsernameNotFoundException("User not found"));
        return AppUserConverter.entityToDto(appUserSaved);
    }

    @Override
    public List<AppUser> findAllAppUser() {
        return appUserRepository.findAll();
    }
}
