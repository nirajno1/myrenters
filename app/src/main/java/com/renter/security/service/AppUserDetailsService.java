package com.renter.security.service;

import com.renter.security.pojo.AppUserDto;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * AppUserDetailsService of my-renter
 *
 * @author Neeraj Kumar
 * @since 13-May-2024 10:30 am
 */
@Service
public class AppUserDetailsService implements UserDetailsService {
    private AppUserService appUserService;

    public AppUserDetailsService(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        AppUserDto appUserDto = appUserService.findByEmail(email);
        return User.builder().username(appUserDto.getEmail())
                .password(appUserDto.getPassword())
                .authorities("ADMIN").build();
    }
}
