package com.renter.security.repository;

import com.renter.security.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * AppUserRepository of my-renter
 *
 * @author Neeraj Kumar
 * @since 15-May-2024 10:40 am
 */
@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Integer> {
    Optional<AppUser> findByEmail(String email);
}
