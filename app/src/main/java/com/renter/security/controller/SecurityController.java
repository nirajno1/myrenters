package com.renter.security.controller;

import com.renter.security.jwt.JwtService;
import com.renter.security.pojo.JwtAuthenticationResponse;
import com.renter.security.pojo.AppUserDto;
import com.renter.security.pojo.SigninRequest;
import com.renter.security.service.AppUserService;
import com.renter.security.service.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * SecurityController of my-renter
 *
 * @author Neeraj Kumar
 * @since 13-May-2024 9:46 am
 */
@RestController
@RequestMapping("/auth")
public class SecurityController {

    private AuthenticationService authenticationService;

    private AppUserService appUserService;

    private JwtService jwtService;

    public SecurityController(AuthenticationService authenticationService, AppUserService appUserService, JwtService jwtService) {
        this.authenticationService = authenticationService;
        this.appUserService = appUserService;
        this.jwtService = jwtService;
    }

    @GetMapping("/home")
    public String hello(){
        return "Hello world!!";
    }

    @PostMapping("/jwtlogin")
    public ResponseEntity<JwtAuthenticationResponse> authenticate(@RequestBody SigninRequest loginUserDto) {
        UserDetails authenticatedUser = authenticationService.authenticate(loginUserDto);

        String jwtToken = jwtService.generateToken(authenticatedUser);

        JwtAuthenticationResponse loginResponse = new JwtAuthenticationResponse(jwtToken);

        return ResponseEntity.ok(loginResponse);
    }
    @PostMapping("/register")
    public ResponseEntity<AppUserDto> registerAppUser(@RequestBody AppUserDto regUser) {
        return ResponseEntity.ok(appUserService.saveUser(regUser));
    }
}
