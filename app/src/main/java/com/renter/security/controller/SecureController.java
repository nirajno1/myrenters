package com.renter.security.controller;

import com.renter.security.model.AppUser;
import com.renter.security.pojo.AppUserDto;
import com.renter.security.service.AppUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * SecureController of my-renter
 *
 * @author Neeraj Kumar
 * @since 15-May-2024 2:44 pm
 */
@Slf4j
@RestController
public class SecureController {

    private AppUserService appUserService;

    public SecureController(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @GetMapping("/findAllAppUser")
    public List<AppUser> findAllAppUser() {
        return appUserService.findAllAppUser();
    }

}
