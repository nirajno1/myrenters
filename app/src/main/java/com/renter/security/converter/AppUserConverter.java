package com.renter.security.converter;

import com.renter.security.model.AppUser;
import com.renter.security.pojo.AppUserDto;

/**
 * AppUserConverter of my-renter
 *
 * @author Neeraj Kumar
 * @since 15-May-2024 11:09 am
 */
public class AppUserConverter {
    public static AppUser dtoToEntity(AppUserDto appUserDto){
        AppUser entity= new AppUser();
        entity.setId(appUserDto.getId());
        entity.setFirstName(appUserDto.getFirstName());
        entity.setLastName(appUserDto.getLastName());
        entity.setEmail(appUserDto.getEmail());
        entity.setPassword(appUserDto.getPassword());
        return entity;
    }

    public static AppUserDto entityToDto(AppUser appUser){
        AppUserDto dto= new AppUserDto();
        dto.setId(appUser.getId());
        dto.setFirstName(appUser.getFirstName());
        dto.setLastName(appUser.getLastName());
        dto.setEmail(appUser.getEmail());
        dto.setPassword(appUser.getPassword());
        return dto;
    }
}
