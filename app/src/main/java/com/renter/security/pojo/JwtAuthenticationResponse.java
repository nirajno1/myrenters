package com.renter.security.pojo;

/**
 * JwtAuthenticationResponse of my-renter
 *
 * @author Neeraj Kumar
 * @since 13-May-2024 5:28 pm
 */
public class JwtAuthenticationResponse {
    private String token;

    public JwtAuthenticationResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
