package com.renter.security.pojo;

/**
 * sing of my-renter
 *
 * @author Neeraj Kumar
 * @since 13-May-2024 5:27 pm
 */
public class SigninRequest {
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}