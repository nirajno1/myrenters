package com.renter.transaction.service;

import com.renter.master.converter.FlatConverter;
import com.renter.master.converter.RenterConverter;
import com.renter.master.entity.ElectricMeter;
import com.renter.master.entity.Flat;
import com.renter.master.entity.MeterReading;
import com.renter.master.entity.Renter;
import com.renter.master.repository.ElectricMeterRepository;
import com.renter.master.repository.FlatRepository;
import com.renter.master.repository.RenterRepository;
import com.renter.transaction.converter.RentFlatConverter;
import com.renter.transaction.dto.FlatRenterDto;
import com.renter.transaction.dto.RentFlatDto;
import com.renter.transaction.entity.RentFlat;
import com.renter.transaction.repository.InvoiceRepository;
import com.renter.transaction.repository.RentFlatRepository;
import com.renter.transaction.util.MeterReadingUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * FlatServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class RentFlatServiceImpl implements RentFlatService {

    @Autowired
    private FlatRepository flatRepository;
    @Autowired
    private RenterRepository renterRepository;
    @Autowired
    private RentFlatRepository rentFlatRepository;
    @Autowired
    private ElectricMeterRepository electricMeterRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private RentFlatConverter rentFlatConverter;
    @Autowired
    private FlatConverter flatConverter;
    @Autowired
    private RenterConverter renterConverter;

    @Autowired
    private InvoiceService invoiceService;

    /**
     * Flow :
     * find flat >> update flat with occupied >> get flat electric meter mark it used >>
     * >> update the electric meter with new meter reading >> save flat and electric meter >> assign it to rent flat object
     * >> find renter >> assign it to rent flat object >> generate invoice
     *
     * @param rentFlatDto rent-flat dto
     * @return rent-flat dto
     */
    @Override
    public RentFlatDto save(RentFlatDto rentFlatDto) {
        RentFlat rentFlat = rentFlatConverter.dtoToEntity(rentFlatDto);
        Flat flat = rentFlat.getFlat();
        Optional<Flat> flatOptional = flatRepository.findById(flat.getId());


        if (flatOptional.isPresent()) {
            flat = flatOptional.get();
            flat.setOccupied(Boolean.TRUE);
            //create meter reading and assign it to electricMeter
            ElectricMeter electricMeter = flat.getElectricMeter();
            electricMeter.setUsed(true);
            List<MeterReading> meterReadings = electricMeter.getMeterReadings();
            Integer currentReading = rentFlatDto.getCurrentMeterReading();

            MeterReadingUtil.createMeterReadings(
                    meterReadings, currentReading,
                    rentFlatDto.getRenter().getId(),
                    rentFlatDto.getElectricRateAgreed());

            electricMeterRepository.save(electricMeter);
            flatRepository.save(flat);
            rentFlat.setFlat(flat);
        } else {
            throw new RuntimeException("Flat not available");
        }

        Renter renter = rentFlat.getRenter();
        Optional<Renter> renterOptional = renterRepository.findById(renter.getId());


        if (renterOptional.isPresent()) {
            renter = renterOptional.get();
            if (rentFlat.getId() == null) {
                var invoice = invoiceService.generateInvoice(rentFlat);
                invoice.setRenter(renter);
                invoiceRepository.save(invoice);
            }
            rentFlat.setRenter(renter);
        } else {
            throw new RuntimeException("Renter not available");
        }
        rentFlat.initAudit("system");

        rentFlat = rentFlatRepository.save(rentFlat);
        return rentFlatConverter.entityToDto(rentFlat);
    }


    @Override
    public List<FlatRenterDto> getAllFlatRenterLists() {
        return rentFlatRepository.getAllRentFlats();
    }

    @Override
    public List<RentFlatDto> findAll() {
        List<RentFlat> rentFlats = rentFlatRepository.findAll();
        return rentFlatConverter.entityToDtoList(rentFlats);
    }

    @Override
    public RentFlatDto findById(Long id) {
        Optional<RentFlat> optionalRentFlat = rentFlatRepository.findById(id);
        if (optionalRentFlat.isPresent()) {
            return rentFlatConverter.entityToDto(optionalRentFlat.get());
        } else {
            return null;
        }
    }

    @Override
    public Boolean deleteById(Long id) {
        Optional<RentFlat> optional = rentFlatRepository.findById(id);
        if (optional.isPresent()) {
            RentFlat rentFlat = optional.get();
            if (Boolean.TRUE.equals(rentFlat.getDeleted())) {
                return Boolean.FALSE;
            }
            rentFlat.setDeleted(true);
            rentFlat.setDeletedOn(Calendar.getInstance());
            rentFlatRepository.save(rentFlat);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public List<RentFlatDto> findAllActiveAndUnDeleted() {
        List<RentFlat> rentFlats = rentFlatRepository.findAllByActiveAndDeleted(true, false);
        return rentFlatConverter.entityToDtoList(rentFlats);
    }
}
