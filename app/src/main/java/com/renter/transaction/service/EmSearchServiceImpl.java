package com.renter.transaction.service;

import com.renter.master.converter.ElectricMeterConverter;
import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.dropdown.EmeterDropDownDto;
import com.renter.master.dto.dropdown.FlatDropDownDto;
import com.renter.master.dto.dropdown.RenterDropDownDto;
import com.renter.master.dto.search.EMQueryByFlatDto;
import com.renter.master.dto.search.EMQueryByRenterDto;
import com.renter.master.entity.ElectricMeter;
import com.renter.master.repository.ElectricMeterRepository;
import com.renter.master.repository.FlatRepository;
import com.renter.master.repository.RenterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmSearchServiceImpl implements EmSearchService{

    @Autowired
    ElectricMeterRepository electricMeterRepository;
    @Autowired
    ElectricMeterConverter electricMeterConverter;

    @Autowired
    RenterRepository renterRepository;


    @Autowired
    FlatRepository flatRepository;

    @Override
    public ElectricMeterDto queryByRenter(EMQueryByRenterDto emQueryByRenterDto) {
        return null;
    }

    @Override
    public ElectricMeterDto queryByFlat(EMQueryByFlatDto emQueryByFlatDto) {
        return null;
    }

    @Override
    public ElectricMeterDto queryByMeter(Long id) {
        Optional<ElectricMeter> electricMeterOptional=electricMeterRepository.findById(id);
        ElectricMeter electricMeter= null;
        if(electricMeterOptional.isPresent()){
            electricMeter= electricMeterOptional.get();
        }
        return electricMeter!= null? electricMeterConverter.entityToDto(electricMeter) : null;
    }

    @Override
    public List<EmeterDropDownDto> findAllElectricMeters() {
        return electricMeterRepository.findAllElectricMeter();
    }

    @Override
    public List<FlatDropDownDto> findAllFlats() {
        return flatRepository.findAllFlat();
    }

    @Override
    public List<RenterDropDownDto> findAllRenters() {
        return renterRepository.findAllRenters();
    }
}
