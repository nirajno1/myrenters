package com.renter.transaction.service;

public interface EmailSenderService {
    void sendEmailAsText(String to, String subject, String message);
    void sendEmailWithAttachment(String to, String subject, String text, String attachmentFileName, byte[] attachmentData);
    void sendEmailAsHtml(String to, String subject, String htmlText);
}
