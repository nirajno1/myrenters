package com.renter.transaction.service;

import com.renter.master.service.BaseMasterService;
import com.renter.transaction.dto.FlatRenterDto;
import com.renter.transaction.dto.RentFlatDto;

import java.util.List;

public interface RentFlatService extends BaseMasterService<RentFlatDto> {
    List<FlatRenterDto> getAllFlatRenterLists();

}
