package com.renter.transaction.service;

import com.itextpdf.io.exceptions.IOException;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * EmailSenderServiceImpl of my-renter
 *
 * @author Neeraj Kumar
 * @since 14-Jun-2023 3:50 PM
 */
@Service
public class EmailSenderServiceImpl implements EmailSenderService {
    Logger logger = Logger.getLogger(EmailSenderServiceImpl.class.getName());
    private final JavaMailSender mailSender;

    @Value("${app.admin.email}")
    private String adminEmail;

    @Autowired
    public EmailSenderServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendEmailAsText(String to, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        message.setFrom(adminEmail);
        mailSender.send(message);
    }

    public void sendEmailWithAttachment(String to, String subject, String text, String attachmentFileName, byte[] attachmentData) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(to);
            helper.setSubject(subject);
            helper.setFrom(adminEmail);
            helper.setText(text, true); // Enable HTML content

            // Attach the file
            InputStreamSource attachmentSource = new ByteArrayResource(attachmentData);
            helper.addAttachment(attachmentFileName, attachmentSource);
            mailSender.send(message);
        } catch (MessagingException | IOException e) {
            logger.log(Level.SEVERE, "error in sending email", e);
        }
    }
    public void sendEmailAsHtml(String to, String subject, String htmlText) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setFrom(adminEmail);
            helper.setText(htmlText, true); // Enable HTML content
            mailSender.send(message);
        } catch (MessagingException | IOException e) {
            logger.log(Level.SEVERE, "error in sending email", e);
        }
    }
}
