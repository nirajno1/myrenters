package com.renter.transaction.service;

import com.renter.master.entity.MeterReading;
import com.renter.transaction.dto.InvoicePaymentDto;
import com.renter.transaction.dto.InvoiceResponse;
import com.renter.transaction.entity.RentFlat;
import com.renter.transaction.entity.payment.Invoice;

import java.util.List;

public interface InvoiceService {
    Invoice generateInvoice(RentFlat rentFlat);
    Invoice generateInvoice(MeterReading meterReading);
    List<InvoiceResponse> getAllInvoice();

    InvoiceResponse getInvoiceById(Long invoiceId);
    InvoicePaymentDto saveInvoicePayment(InvoicePaymentDto paymentDto);

    Double getInvoiceBalanceDueById(Long invoiceId);

    void generateInvoice();
}
