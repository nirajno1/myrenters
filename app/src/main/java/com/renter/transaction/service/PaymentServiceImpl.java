package com.renter.transaction.service;

import com.renter.master.converter.BaseConverter;
import com.renter.master.converter.MeterReadingConverter;
import com.renter.master.converter.MonthlyRentConverter;
import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.dto.em.MeterReadingCaptureDto;
import com.renter.master.dto.rent.MonthlyRentDto;
import com.renter.transaction.entity.payment.Invoice;
import com.renter.transaction.entity.payment.PaymentMaster;
import com.renter.master.entity.ValueObject;
import com.renter.master.repository.MeterReadingRepository;
import com.renter.master.repository.MonthlyRentRepository;
import com.renter.master.repository.PaymentMasterRepository;
import com.renter.master.service.ElectricMeterService;
import com.renter.master.service.FlatService;
import com.renter.master.service.RenterService;
import com.renter.master.dto.PaymentMasterDto;
import com.renter.transaction.dto.PaymentRequestDto;
import com.renter.transaction.dto.RentDueDetailDto;
import com.renter.transaction.dto.RentDueSummaryDto;
import com.renter.transaction.entity.RentFlat;
import com.renter.transaction.util.MeterReadingDtoDateDescComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static com.renter.transaction.util.PaymentServiceUtil.createPayment;
import static com.renter.transaction.util.PaymentServiceUtil.findLastMeterReading;
import static com.renter.transaction.util.PaymentServiceUtil.initDueDetails;

/**
 * PaymentServiceImpl of demo
 *
 * @author Neeraj
 * @since 20-Feb-2023 4:19 PM
 */
@Service
public class PaymentServiceImpl implements PaymentService {
    public static final double ELECTRIC_UNIT_RATE = 8.5;
    private static final String MONTHLY_RENT = "rent";
    private static final String MONTHLY_ELEC_BILL = "ebill";
    @Autowired
    private ElectricMeterService electricMeterService;

    @Autowired
    private RenterService renterService;

    @Autowired
    private FlatService flatService;
    @Autowired
    private PaymentMasterRepository paymentMasterRepository;

    @Autowired
    private BaseConverter<PaymentMasterDto, PaymentMaster> paymentMasterConverter;

    @Autowired
    private MonthlyRentConverter monthlyRentConverter;
    @Autowired
    private MonthlyRentRepository monthlyRentRepository;

    @Autowired
    private MeterReadingConverter meterReadingConverter;
    @Autowired
    private MeterReadingRepository meterReadingRepository;

    @Override
    public ElectricMeterDto calculateElectricBill(MeterReadingCaptureDto meterReadingCaptureDto) {
        ElectricMeterDto dto = electricMeterService.findElectricMeterBySn(meterReadingCaptureDto.getMeterSn());
        if (Objects.nonNull(dto)) {
            Long id = dto.getId();
            List<MeterReadingDto> meterReadings = electricMeterService.findAllMeterReadingsByMeterId(id);
            MeterReadingDto meterReadingDto = findLastMeterReading(meterReadings);

            MeterReadingDto currentMeterReadingDto = new MeterReadingDto();
        //    currentMeterReadingDto.setPreviousReading(meterReadingDto.getCurrentReading());
        //    currentMeterReadingDto.setPreviousReadingDate(meterReadingDto.getCurrentReadingDate());
            currentMeterReadingDto.setCurrentReading(meterReadingCaptureDto.getMeterReading());
            currentMeterReadingDto.setCurrentReadingDate(meterReadingCaptureDto.getMeterReadingDate());

            List<MeterReadingDto> modifiableList = new ArrayList<MeterReadingDto>(meterReadings);
            modifiableList.add(currentMeterReadingDto);
            modifiableList.sort(new MeterReadingDtoDateDescComparator());
            if (modifiableList.size() > 5) {
                dto.setMeterReadings(modifiableList.subList(0, 5));
            } else {
                dto.setMeterReadings(modifiableList);
            }
            dto = electricMeterService.save(dto);
        }
        return dto;
    }

    @Override
    public List<RentDueSummaryDto> findAmountDueSmryByRenterId(Long renterId) {
        final var renters = renterService.findRenterById(renterId);
        //final var flats = renters.getRentedFlat();
        List<RentDueSummaryDto> dues = new ArrayList<>();
        /*flats.forEach(flat -> {
            initRentDueSmry(renterId, dues, flat);
        });*/
        return dues;
    }

    @Override
    public List<RentDueDetailDto> findAmountDueDtlByRenterId(Long renterId) {
        final var renters = renterService.findRenterById(renterId);
        // final var flats = renters.getRentedFlat();
        List<RentDueDetailDto> dues = new ArrayList<>();
        // flats.forEach(flat -> initDueDetails(flat, dues));
        return dues;
    }

    public List<RentDueDetailDto> findAmountDueDtlByFlatId(Long flatId) {
        final var flatDto = flatService.findById(flatId);
        List<RentDueDetailDto> dues = new ArrayList<>();
        initDueDetails(flatDto, dues);
        return dues;
    }

    @Override
    public void acceptPayment(PaymentRequestDto paymentRequest) {
        final var selectedRents = paymentRequest.getSelectedRents();
        double receivedAmount = paymentRequest.getAmountReceived();
        double processedAmount = 0.0;
        double balancedAmount = receivedAmount;
        List<PaymentMasterDto> payments = new ArrayList<>();

        if (selectedRents != null && !selectedRents.isEmpty()) {
            for (MonthlyRentDto e : selectedRents) {
                double paymentDue = e.getAmount();
                balancedAmount = receivedAmount - processedAmount;
                if (balancedAmount >= paymentDue) {
                    e.setPaid(Boolean.TRUE);
                    payments.add(createPayment(MONTHLY_RENT, e.getId(),
                            paymentDue, Double.valueOf("0"), e.getRenterId(),
                            paymentRequest.getFlatId(), paymentRequest.getPaymentType()));
                    processedAmount += e.getAmount();
                } else {
                    e.setPaid(Boolean.FALSE);
                    payments.add(createPayment(MONTHLY_RENT, e.getId(),
                            paymentDue, balancedAmount, e.getRenterId(),
                            paymentRequest.getFlatId(), paymentRequest.getPaymentType()));
                    processedAmount += e.getAmount();
                    break;
                }

            }
        }
        final var selectedMeterReadings = paymentRequest.getSelectedMeterReadings();

        if (selectedMeterReadings != null && !selectedMeterReadings.isEmpty()) {
            selectedMeterReadings.forEach(e -> e.setPaid(Boolean.TRUE));
            for (MeterReadingDto e : selectedMeterReadings) {
                double paymentDue = 0;// e.getAmountDue();
                balancedAmount = receivedAmount - processedAmount;
                if (balancedAmount >= paymentDue) {
                    e.setPaid(Boolean.TRUE);
                    //e.setAmountPaid(paymentDue);
                    //e.setAmountPaidDate(Calendar.getInstance());

                    payments.add(createPayment(MONTHLY_ELEC_BILL, e.getId(),
                            paymentDue, Double.valueOf("0"), e.getConsumedByRenter(),
                            paymentRequest.getFlatId(), paymentRequest.getPaymentType()));
                    processedAmount += 0;//e.getAmountDue();
                } else {
                    e.setPaid(Boolean.FALSE);
                 //   e.setAmountPaid(balancedAmount);
                 //   e.setBalanceAmount(paymentDue - balancedAmount);
                 //   e.setAmountPaidDate(Calendar.getInstance());
                    payments.add(createPayment(MONTHLY_ELEC_BILL, e.getId(),
                            paymentDue, balancedAmount, e.getConsumedByRenter(),
                            paymentRequest.getFlatId(), paymentRequest.getPaymentType()));
                    processedAmount += 0;//e.getAmountDue();
                    break;
                }

            }
        }
        balancedAmount = receivedAmount - processedAmount;
        System.out.printf("Balanced amount: %.2f \nReceived amount: %.2f ", balancedAmount, receivedAmount);
        payments.forEach(System.out::println);

        List<com.renter.master.entity.MeterReading> meterReadings = meterReadingConverter.dtoToEntityList(selectedMeterReadings);
        List<Invoice> monthlyRents = monthlyRentConverter.dtoToEntityList(selectedRents);
        monthlyRentRepository.saveAll(monthlyRents);
        meterReadingRepository.saveAll(meterReadings);

        paymentMasterRepository.saveAll(paymentMasterConverter.dtoToEntityList(payments));


    }

    @Override
    public List<PaymentMasterDto> paymentHistory(Long renterid) {
        List<PaymentMaster> payments = paymentMasterRepository.findAllByRenterId(renterid);
        return paymentMasterConverter.entityToDtoList(payments);
    }

    private int getRandom(int start, int end) {
        Random random = new Random();
        return random.nextInt(start, end);
    }

    @Override
    public Boolean generateFirstBill(RentFlat rentFlat) {
        Calendar rentStartDate=rentFlat.getRentStartDate();
        Double rentAmount= rentFlat.getRentAmountAgreed();
        ValueObject pmntfreq=rentFlat.getPmntFrequency();
        ValueObject pmntType=rentFlat.getPmntAdvance();



        return Boolean.FALSE;
    }
}
