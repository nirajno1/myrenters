package com.renter.transaction.service;

import com.renter.master.dto.rent.MonthlyRentDto;

import java.util.List;

public interface RentSearchService {
    public List<MonthlyRentDto> queryByRenter(Long id);
    public List<MonthlyRentDto> queryByFlat(Long id);

    Double queryByFlatForData(Long id);
}
