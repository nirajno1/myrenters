package com.renter.transaction.service;

import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.dropdown.EmeterDropDownDto;
import com.renter.master.dto.dropdown.FlatDropDownDto;
import com.renter.master.dto.dropdown.RenterDropDownDto;
import com.renter.master.dto.search.EMQueryByFlatDto;
import com.renter.master.dto.search.EMQueryByRenterDto;

import java.util.List;

public interface EmSearchService {
    public ElectricMeterDto queryByRenter(EMQueryByRenterDto emQueryByRenterDto);
    public ElectricMeterDto queryByFlat(EMQueryByFlatDto emQueryByFlatDto);
    public ElectricMeterDto queryByMeter(Long id);

    public List<EmeterDropDownDto> findAllElectricMeters();
    List<FlatDropDownDto> findAllFlats();
    List<RenterDropDownDto> findAllRenters();
}
