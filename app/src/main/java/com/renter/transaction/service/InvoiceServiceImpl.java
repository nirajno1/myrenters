package com.renter.transaction.service;

import com.renter.master.entity.Flat;
import com.renter.master.entity.MeterReading;
import com.renter.master.entity.Renter;
import com.renter.master.repository.MeterReadingRepository;
import com.renter.master.repository.RenterRepository;
import com.renter.transaction.converter.LocalDateCalConverter;
import com.renter.transaction.dto.InvoicePaymentDto;
import com.renter.transaction.dto.InvoicePmntResponse;
import com.renter.transaction.dto.InvoiceResponse;
import com.renter.transaction.entity.RentFlat;
import com.renter.transaction.entity.payment.Invoice;
import com.renter.transaction.entity.payment.InvoicePayment;
import com.renter.transaction.repository.InvoicePaymentRepository;
import com.renter.transaction.repository.InvoiceRepository;
import com.renter.transaction.repository.RentFlatRepository;
import com.renter.transaction.util.FormatCalendar;
import com.renter.transaction.util.InvoiceIssuedDateDescComparator;
import com.renter.transaction.util.InvoiceUtil;
import com.renter.transaction.util.SameDayOfMonthAdjuster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static com.renter.transaction.common.Constants.INV_TYP_ELEC;
import static com.renter.transaction.common.Constants.INV_TYP_RENT;
import static com.renter.transaction.common.Constants.PMT_ADV;
import static com.renter.transaction.common.Constants.PMT_DAILY;
import static com.renter.transaction.common.Constants.PMT_MONTHLY;
import static com.renter.transaction.common.Constants.PMT_NO_ADV;
import static com.renter.transaction.common.Constants.PMT_WEEKLY;
import static com.renter.transaction.common.Constants.PMT_YEARLY;
import static com.renter.transaction.common.Constants.USER_ID;

/**
 * InvoiceServiceImpl of my-renter
 *
 * @author Neeraj Kumar
 * @since 24-Apr-2023 2:37 PM
 */
@Service
public class InvoiceServiceImpl implements InvoiceService {

    private static final long NOTIFICATION_DAYS = 2;
    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private InvoicePaymentRepository invoicePaymentRepository;

    @Autowired
    private RenterRepository renterRepository;

    @Autowired
    private MeterReadingRepository meterReadingRepository;

    @Autowired
    private EmailSenderServiceImpl emailSenderService;

    @Override
    public Invoice generateInvoice(RentFlat rentFlat) {
        Invoice invoice = createFirstInvoice(rentFlat);
        return invoiceRepository.save(invoice);
    }

    @Override
    public Invoice generateInvoice(MeterReading meterReading) {
        Invoice invoice = createInvoice(meterReading);
        return invoiceRepository.save(invoice);
    }

    private Invoice createInvoice(MeterReading meterReading) {
        Invoice invoice = null;
        if (Objects.nonNull(meterReading.getPreviousReading())
                && Objects.nonNull(meterReading.getPaid())
                && Boolean.FALSE.equals(meterReading.getPaid())) {
            invoice = new Invoice();
            final var currentReading = meterReading.getCurrentReading();
            final var previousReading = meterReading.getPreviousReading().getCurrentReading();
            int unitConsumed = currentReading - previousReading;
            //todo get actual electric rate
            double amount = unitConsumed * 8.0;
            invoice.setAmount(amount);
            invoice.setIssueDate(Calendar.getInstance());
            invoice.setPaid(Boolean.FALSE);
            invoice.setType(INV_TYP_ELEC);
            invoice.setItemId(meterReading.getId());

            final var renter = renterRepository.findById(meterReading.getConsumedByRenter()).orElseThrow();
            invoice.setRenter(renter);

            final var endDate = meterReading.getCurrentReadingDate();
            final var startDate = meterReading.getPreviousReading().getCurrentReadingDate();

            invoice.setFromDate(startDate);
            invoice.setToDate(endDate);
            invoice.setPaymentDueDate(endDate);//todo need to fetch rent due date

            String meterNickName = meterReading.getElectricMeter().getNickName();
            String frmDateStr = FormatCalendar.formatCal(invoice.getFromDate());
            String toDateStr = FormatCalendar.formatCal(invoice.getToDate());
            invoice.setParticular(MessageFormat.format("Electric Meter: {0} - {1} to {2}.", meterNickName, frmDateStr, toDateStr));

            invoice.initAudit(USER_ID);

        }
        return invoice;
    }

    @Override
    public List<InvoiceResponse> getAllInvoice() {
        final var all = invoiceRepository.findAll();
        return all.stream().map(e -> {
            Renter renter = e.getRenter();
            Long renterId = null;
            if (renter != null) {
                renterId = renter.getId();
            }

            return new InvoiceResponse(e.getId(), e.getAmount(), e.getDueAmount(), e.getIssueDate(),
                    e.getPaymentDueDate(), e.getFromDate(), e.getToDate(),
                    e.getPaid(), renterId, e.getParticular(), transformPmntResponse(e));
        }).toList();
    }

    private List<InvoicePmntResponse> transformPmntResponse(Invoice invoice) {
        final var invoicePayments = invoice.getInvoicePayments();
        return invoicePayments.stream().map(pmnt ->
                new InvoicePmntResponse(pmnt.getId(), pmnt.getInvoice().getId(),
                        pmnt.getAmountReceived(), pmnt.getDateReceived(), pmnt.getPaymentMethod())
        ).toList();
    }

    @Override
    public InvoiceResponse getInvoiceById(Long invoiceId) {
        final var e = invoiceRepository.findById(invoiceId).orElseThrow();
        Renter renter = e.getRenter();
        return new InvoiceResponse(e.getId(), e.getAmount(), e.getDueAmount(),
                e.getIssueDate(), e.getPaymentDueDate(), e.getFromDate(),
                e.getToDate(), e.getPaid(), renter.getId(), e.getParticular(),
                transformPmntResponse(e));
    }

    @Override
    public InvoicePaymentDto saveInvoicePayment(InvoicePaymentDto paymentDto) {
        final var invoice = invoiceRepository.findByIdAndPaid(paymentDto.getInvoiceId(), false).orElseThrow();
        Double invoiceAmnt = invoice.getAmount();
        //update balanceDue amount

        InvoicePayment payment = new InvoicePayment(paymentDto.getId(),
                paymentDto.getAmountReceived(),
                paymentDto.getDateReceived(), paymentDto.getPaymentMethod(), invoice);
        payment.initAudit(USER_ID);
        payment = invoicePaymentRepository.save(payment);

        Double paid = calculateBalanceAmount(invoice);

        Double balance = invoiceAmnt - paid;

        invoice.setDueAmount(balance);

        if (invoiceAmnt <= paid) {
            invoice.setPaid(true);
        }
        invoice.initAudit(USER_ID);
        invoiceRepository.save(invoice);
        if (INV_TYP_ELEC.equals(invoice.getType()) && invoice.getPaid()) {
            final var meterReadingId = invoice.getItemId();
            final var meterReadingOptional = meterReadingRepository.findById(meterReadingId);
            if (meterReadingOptional.isPresent()) {
                final var meterReading = meterReadingOptional.get();
                meterReading.setPaid(true);
                meterReading.initAudit(USER_ID);
                meterReadingRepository.save(meterReading);
            }
        }
        paymentDto.setId(payment.getId());
        return paymentDto;
    }

    private Double calculateBalanceAmount(Invoice invoice) {
        return invoice.getInvoicePayments().stream()
                .map(InvoicePayment::getAmountReceived)
                .reduce(0.0, Double::sum);
    }

    @Override
    public Double getInvoiceBalanceDueById(Long invoiceId) {
        final var invoice = invoiceRepository.findByIdAndPaid(invoiceId, false).orElseThrow();
        Double invoiceAmnt = invoice.getAmount();
        Double paid = calculateBalanceAmount(invoice);
        Double balance = invoiceAmnt - paid;
        if (Objects.nonNull(invoice.getDueAmount())
                && !(invoice.getDueAmount().equals(balance))) {
            invoice.initAudit(USER_ID);
            invoice.setDueAmount(balance);
            invoiceRepository.save(invoice);
        }
        return balance;
    }

    private Invoice createFirstInvoice(RentFlat rentFlat) {
        Invoice invoice = new Invoice();
        invoice.setAmount(rentFlat.getRentAmountAgreed());
        invoice.setIssueDate(Calendar.getInstance());

        invoice.setPaid(Boolean.FALSE);
        String pmntFreq = rentFlat.getPmntFrequency().getDdValue();
        String pmntAdv = rentFlat.getPmntAdvance().getDdValue();
        Calendar startDate = rentFlat.getRentStartDate();

        invoice.setType(INV_TYP_RENT);

        Calendar endDate = Calendar.getInstance();
        endDate.setTime(startDate.getTime());

        if (PMT_MONTHLY.equals(pmntFreq)) {
            endDate.add(Calendar.MONTH, 1);
        }
        if (PMT_WEEKLY.equals(pmntFreq)) {
            endDate.add(Calendar.WEEK_OF_YEAR, 1);
        }
        if (PMT_DAILY.equals(pmntFreq)) {
            endDate.add(Calendar.DATE, 1);
        }
        if (PMT_YEARLY.equals(pmntFreq)) {
            endDate.add(Calendar.YEAR, 1);
        }
        invoice.setFromDate(startDate);
        invoice.setToDate(endDate);

        if (PMT_ADV.equals(pmntAdv)) {
            invoice.setPaymentDueDate(startDate);
        } else if (PMT_NO_ADV.equals(pmntAdv)) {
            invoice.setPaymentDueDate(endDate);
        }
        Flat flat = rentFlat.getFlat();
        String flatNumber = flat.getFlatNumber();
        String frmDateStr = FormatCalendar.formatCal(invoice.getFromDate());
        String toDateStr = FormatCalendar.formatCal(invoice.getToDate());
        invoice.setParticular(MessageFormat.format("Flat Number: {0} {1} to {2} rent.", flatNumber, frmDateStr, toDateStr));

        invoice.initAudit(USER_ID);
        return invoice;
    }

    public Invoice generateScheduledInvoice(RentFlat rentFlat) {
        Calendar currentDate= Calendar.getInstance();
        currentDate.set(Calendar.DAY_OF_MONTH,rentFlat.getRentStartDate().get(Calendar.DAY_OF_MONTH));
        // generate invoice
        var invoice = new Invoice();
        invoice.setAmount(rentFlat.getRentAmountAgreed());
        invoice.setDueAmount(rentFlat.getRentAmountAgreed());
        invoice.setIssueDate(Calendar.getInstance());
        invoice.setItemId(rentFlat.getId());
        invoice.setIssueDate(Calendar.getInstance());
        invoice.setPaid(Boolean.FALSE);
        invoice.setType(INV_TYP_RENT);
        /* rent From date */
        LocalDate rentStartLocalDate = LocalDateCalConverter.calendarToLocalDate(rentFlat.getRentStartDate());
        LocalDate rentDueDate = createSameDayForCurrentMonth(rentStartLocalDate);
        Calendar startDate=LocalDateCalConverter.localDateToCalendar(rentDueDate);
        invoice.setFromDate(startDate);


        String pmntAdv = rentFlat.getPmntAdvance().getDdValue();
        Calendar endDate = calculateEndDate(rentFlat,startDate);

        invoice.setToDate(endDate);
        if (PMT_ADV.equals(pmntAdv)) {
            invoice.setPaymentDueDate(startDate);
        } else if (PMT_NO_ADV.equals(pmntAdv)) {
            invoice.setPaymentDueDate(endDate);
        }

        invoice.initAudit(USER_ID);
        String msg="Renter Name: {0}, {1} to {2} rent.";
        invoice.setParticular(MessageFormat.format(msg,rentFlat.getRenter().getName(), FormatCalendar.formatCal(invoice.getFromDate()),
                FormatCalendar.formatCal(invoice.getToDate())));
        invoice.setRenter(rentFlat.getRenter());
        return invoice;
    }

    private Calendar calculateEndDate(RentFlat rentFlat,Calendar startDate) {
        Calendar endDate=Calendar.getInstance();
        endDate.setTime(startDate.getTime());
        String pmntFreq = rentFlat.getPmntFrequency().getDdValue();
        if (PMT_MONTHLY.equals(pmntFreq)) {
            endDate.add(Calendar.MONTH, 1);
        }else if (PMT_WEEKLY.equals(pmntFreq)) {
            endDate.add(Calendar.WEEK_OF_YEAR, 1);
        }else if (PMT_DAILY.equals(pmntFreq)) {
            endDate.add(Calendar.DATE, 1);
        }else if(PMT_YEARLY.equals(pmntFreq)) {
            endDate.add(Calendar.YEAR, 1);
        }
        return endDate;
    }

    public void findRent(RentFlatRepository rentFlatRepository) {
        List<RentFlat> rentFlats = rentFlatRepository.findAllByActiveAndDeleted(true, false);
        Calendar today = Calendar.getInstance();//todo remove for actual implementation
        rentFlats.stream().forEach(rentFlat -> {
                    Renter renter = rentFlat.getRenter();
                    final var invoices = renter.getInvoices();
                    Invoice latestInvoice = invoices.stream()
                            .sorted(new InvoiceIssuedDateDescComparator())
                            .findFirst().orElseThrow();

                }
        );
    }

    @Autowired
    private RentFlatRepository rentFlatRepository;

    @Override
    @Scheduled(cron = "${invoice.generation.cron}")
    public void generateInvoice() {
        LocalDateTime dateTime = LocalDateTime.now();
        //find all rent-flat object which are candidate for rent payments
        final var allByActiveAndDeleted = rentFlatRepository.findAllByActiveAndDeleted(true, false);
        final var rentFlats = allByActiveAndDeleted.stream().filter(eligibleForInvoiceGeneration).toList();
        List<Invoice> newInvoices= new ArrayList<>();
        List<Invoice> oldInvoices= new ArrayList<>();
        for(RentFlat rentFlat:rentFlats){
            Renter renter=rentFlat.getRenter();
            //find all invoices for renter unpaid
            oldInvoices = invoiceRepository.findAllByRenterAndPaid(renter,false);
            //check invoice if it is already generated earlier
            InvoiceUtil.isInvoiceGeneratedEarlier(oldInvoices,rentFlat);
            //newInvoices.add(generateScheduledInvoice(rentFlat));
        }
        if(!newInvoices.isEmpty() || !oldInvoices.isEmpty()) {
            invoiceRepository.saveAll(newInvoices);
            oldInvoices.addAll(newInvoices);
            String message = generateEmailMessage(oldInvoices);
            emailSenderService.sendEmailAsHtml("Renter <renter@email.com>",
                    "Invoice generated for " + dateTime.toString(),
                    message);
        }
    }

    private String generateEmailMessage(List<Invoice> newInvoices) {
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("Dear Sir,<br> Your unpaid invoice is due. <br>");
        stringBuilder.append("<table>");
        for(Invoice invoice:newInvoices) {
            stringBuilder.append("<tr><td>" +
                    FormatCalendar.formatCal(invoice.getPaymentDueDate())+
                    "</td><td >" +
                    invoice.getDueAmount()
                    +"</td ></tr >");
        }
        stringBuilder.append("</table > ");
        return stringBuilder.toString();
    }

    Predicate<RentFlat> eligibleForInvoiceGeneration = (e) -> {
        LocalDate rentStartDate = LocalDateCalConverter.calendarToLocalDate(e.getRentStartDate());
        LocalDate rentDueDate = createSameDayForCurrentMonth(rentStartDate);
        // rentStartDate 5-2-2023
        // rentDueDate 5-6-2023
        // current date 3-6-2023
        LocalDate notifyDate=rentDueDate.minusDays(NOTIFICATION_DAYS);
        return notifyDate.equals(LocalDate.now());
    };


    private LocalDate createSameDayForCurrentMonth(LocalDate oldDate) {
        LocalDate currentDate = LocalDate.now();
        return currentDate.with(new SameDayOfMonthAdjuster(oldDate.getDayOfMonth()));
    }

    public static void main(String[] args) {


        int desiredDayOfMonth = 32; // Example desired day of the month

        // Specify the year
        int year = 2023; // Example year
        //LocalDate currentDate = LocalDate.of(2023,1,31);
        LocalDate now = LocalDate.now();
        LocalDate addedDate = now;
        // Iterate through the months of the year
        for (Month month : Month.values()) {
            // Create a LocalDate object for the desired date in the current month and year
            addedDate = addedDate.plusMonths(1);
            final var with = addedDate.with(new SameDayOfMonthAdjuster(desiredDayOfMonth));
            System.out.println(with);
        }
    }
}
