package com.renter.transaction.service;

import com.renter.master.dto.PaymentMasterDto;
import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.em.MeterReadingCaptureDto;
import com.renter.transaction.dto.PaymentRequestDto;
import com.renter.transaction.dto.RentDueDetailDto;
import com.renter.transaction.dto.RentDueSummaryDto;
import com.renter.transaction.entity.RentFlat;

import java.util.List;

public interface PaymentService {
    ElectricMeterDto calculateElectricBill(MeterReadingCaptureDto meterReadingCaptureDto);
    List<RentDueSummaryDto> findAmountDueSmryByRenterId(Long renterId);
    public  List<RentDueDetailDto> findAmountDueDtlByRenterId(Long renterId);

    List<RentDueDetailDto> findAmountDueDtlByFlatId(Long flatId);
    void acceptPayment(PaymentRequestDto paymentRequest);

    List<PaymentMasterDto> paymentHistory(Long renterid);

    Boolean generateFirstBill(RentFlat rentFlat);

}
