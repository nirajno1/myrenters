package com.renter.transaction.service;

import com.renter.master.converter.MonthlyRentConverter;
import com.renter.master.dto.rent.MonthlyRentDto;
import com.renter.master.entity.Flat;
import com.renter.transaction.entity.payment.Invoice;
import com.renter.master.entity.Renter;
import com.renter.master.repository.FlatRepository;
import com.renter.master.repository.RenterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * RentSearchServiceImpl of demo
 *
 * @author Neeraj Kumar
 * @since 27-Feb-2023 3:13 PM
 */
@Service
public class RentSearchServiceImpl implements RentSearchService {
    @Autowired
    private RenterRepository renterRepository;
    @Autowired
    private FlatRepository flatRepository;
    @Autowired
    private MonthlyRentConverter monthlyRentConverter;

    @Override
    public List<MonthlyRentDto> queryByRenter(Long id) {
        final Optional<Renter> renterOptional = renterRepository.findById(id);
        if (renterOptional.isPresent()) {
            List<MonthlyRentDto> rentDtos = new ArrayList<>();
            Renter renter = renterOptional.get();
            /*if (renter.getRentedFlat() != null && !renter.getRentedFlat().isEmpty()) {
                List<Flat> flats = renter.getRentedFlat();
                flats.forEach(e -> {
                    List<Invoice> monthlyRents = e.getMonthlyRents();
                    if (monthlyRents != null && !monthlyRents.isEmpty()) {
                        rentDtos.addAll(monthlyRentConverter.entityToDtoList(monthlyRents));
                    }
                });
            }*/
            return rentDtos;
        }
        return Collections.emptyList();
    }

    @Override
    public List<MonthlyRentDto> queryByFlat(Long id) {
        final Optional<Flat> flatOptional = flatRepository.findById(id);
        if (flatOptional.isPresent()) {
            List<MonthlyRentDto> rentDtos = new ArrayList<>();
            Flat flat = flatOptional.get();
            if (flat != null) {
                List<Invoice> monthlyRents = flat.getInvoices();
                if (monthlyRents != null && !monthlyRents.isEmpty()) {
                    rentDtos.addAll(monthlyRentConverter.entityToDtoList(monthlyRents));
                }
            }
            return rentDtos;
        }
        return Collections.emptyList();
    }

    @Override
    public Double queryByFlatForData(Long id) {
        final Optional<Flat> flatOptional = flatRepository.findById(id);
        if (flatOptional.isPresent()) {
            return flatOptional.get().getRentAmount();
        }
        return null;
    }
}
