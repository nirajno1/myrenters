package com.renter.transaction.repository;

import com.renter.transaction.dto.FlatRenterDto;
import com.renter.transaction.entity.RentFlat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RentFlatRepository extends JpaRepository<RentFlat, Long> {

    @Query(value ="select new com.renter.transaction.dto.FlatRenterDto(" +
            "rentFlat.id, rentFlat.flat.flatNumber, rentFlat.renter.name," +
            " rentFlat.rentStartDate, rentFlat.pmntFrequency.ddValue," +
            "rentFlat.pmntAdvance.ddValue, rentFlat.rentAmountAgreed ," +
            "rentFlat.electricRateAgreed , rentFlat.active)" +
            "  from com.renter.transaction.entity.RentFlat as rentFlat ")
    List<FlatRenterDto> getAllRentFlats();

    List<RentFlat> findAllByActiveAndDeleted(boolean active, boolean deleted);
}