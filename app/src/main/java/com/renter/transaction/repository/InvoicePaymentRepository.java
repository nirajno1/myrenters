package com.renter.transaction.repository;

import com.renter.transaction.entity.payment.InvoicePayment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoicePaymentRepository extends JpaRepository<InvoicePayment, Long> {
}