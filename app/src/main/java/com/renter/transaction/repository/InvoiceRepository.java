package com.renter.transaction.repository;

import com.renter.master.entity.Renter;
import com.renter.transaction.entity.payment.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
    Optional<Invoice> findByIdAndPaid(Long id, boolean paid);

    List<Invoice> findAllByRenterAndPaid(Renter renter, boolean paid);
}