package com.renter.transaction.entity;

import com.renter.master.entity.Audit;
import com.renter.master.entity.Flat;
import com.renter.master.entity.Renter;
import com.renter.master.entity.ValueObject;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;

/**
 * RentFlat of demo
 *
 * @author Neeraj Kumar
 * @since 23-Mar-2023 1:45 PM
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class RentFlat extends Audit {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "flat_id")
    private Flat flat;

    @OneToOne
    @JoinColumn(name = "renter_id")
    private Renter renter;

    private Calendar rentStartDate;
    private Calendar rentEndDate;

    @ManyToOne
    @JoinColumn(name = "pmnt_frequency_id")
    private ValueObject pmntFrequency;

    @ManyToOne
    @JoinColumn(name = "pmnt_advance_id")
    private ValueObject pmntAdvance;

    private Double rentAmountAgreed;
    private Double electricRateAgreed;

    public void initAudit(String userId) {
        super.initAudit(id, userId);
    }
}
