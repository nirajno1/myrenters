package com.renter.transaction.entity.payment;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.renter.master.entity.Audit;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;

/**
 * Invoice of demo
 *
 * @author Neeraj
 * @since 09-May-2023 1:26 PM
 */

@Entity
@Setter
@Getter
@NoArgsConstructor
public class InvoicePayment extends Audit {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private Double amountReceived;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar dateReceived;
    private String paymentMethod;

    public InvoicePayment(Long id, Double amountReceived, Calendar dateReceived, String paymentMethod, Invoice invoice) {
        this.id = id;
        this.amountReceived = amountReceived;
        this.dateReceived = dateReceived;
        this.paymentMethod = paymentMethod;
        this.invoice = invoice;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private Invoice invoice;

    public void initAudit(String userId) {
        super.initAudit(id, userId);
    }
}
