package com.renter.transaction.entity.payment;

import com.renter.master.entity.Audit;
import com.renter.master.entity.Renter;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;
import java.util.List;

/**
 * Invoice of demo
 *
 * @author Neeraj
 * @since 23-Feb-2023 1:26 PM
 */

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Invoice extends Audit {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String type;
    private Double amount;
    private Double dueAmount;
    private Calendar issueDate;
    private Calendar paymentDueDate;
    private Calendar fromDate;
    private Calendar toDate;
    private String particular;
    private Long itemId;
    private Boolean paid;

    @ManyToOne
    @JoinColumn(name = "renter_invoice_id")
    private Renter renter;

    @OneToMany(mappedBy = "invoice", fetch = FetchType.LAZY)
    private List<InvoicePayment> invoicePayments = new java.util.ArrayList<>();

    public void initAudit(String userId) {
        super.initAudit(id, userId);
    }
}
