package com.renter.transaction.entity.payment;

import com.renter.master.entity.Audit;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;

/**
 * PaymentMaster of demo
 *
 * @author Neeraj Kumar
 * @since 01-Mar-2023 11:42 AM
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
public class PaymentMaster extends Audit {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long renterId;
    private Long flatId;
    private Calendar paymentReceivedDate;
    private String paymentType;
    private Double receivedAmount;
    private Double balanceAmount;
    private String paymentFor;
    private Long paymentForId;

}
