package com.renter.transaction.entity.payment;

import com.renter.master.entity.Renter;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * RentPayment of demo
 *
 * @since 11-Dec-2022 10:06 AM
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
public class RentPayment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private Double rentAmount;
    private Calendar rentStartDate;
    private Calendar rentEndDate;

    @ManyToOne
    @JoinColumn(name = "renter_id")
    private Renter renter;
}
