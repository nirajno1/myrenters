package com.renter.transaction.util;


import com.renter.transaction.entity.payment.Invoice;

import java.util.Comparator;

/**
 * MeterReadingDtoDateDescComparator of demo
 *
 * @author Neeraj
 * @since  15-May-2023 9:22 AM
 */
public class InvoiceIssuedDateDescComparator implements Comparator<Invoice> {
    @Override
    public int compare(Invoice e2, Invoice e1) {
        if(e1.getIssueDate()!= null && e2.getIssueDate()!= null){
            return e1.getIssueDate().compareTo(e2.getIssueDate());
        }else if(e1.getIssueDate()!= null){
            return -1;
        }else if(e2.getIssueDate()!= null){
            return 1;
        }else {
            return 0;
        }
    }
}
