package com.renter.transaction.util;

import com.renter.master.entity.Image;
import org.bouncycastle.util.Arrays;

import java.util.Base64;
import java.util.Objects;

/**
 * ImageUtil of my-renter
 *
 * @author Neeraj Kumar
 * @since 29-May-2023 3:24 PM
 */
public class ImageUtil {
    public static Image createImageObject(Long id,String imgBase64) {
        Image image = null;
        if (Objects.nonNull(imgBase64) && !imgBase64.isBlank()) {
            String[] imgBases = imgBase64.split(",");
            image = new Image();
            image.setImgBase64Prefix(imgBases[0]);
            byte[] imgBytes = Base64.getDecoder().decode(imgBases[1]);
            image.setImgBase64(imgBytes);
            image.setId(id);
        }
        return image;
    }

    public static String createImageBase64String(Image image) {
        if (Objects.nonNull(image)) {
            byte[] imgBytes = image.getImgBase64();
            if (!Arrays.isNullOrEmpty(imgBytes)) {
                String prefix = image.getImgBase64Prefix();
                byte[] data = image.getImgBase64();
                String imgBase64 = Base64.getEncoder().encodeToString(data);
                return prefix.concat(",").concat(imgBase64);
            }
        }
        return null;
    }
}
