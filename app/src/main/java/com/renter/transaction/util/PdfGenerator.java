package com.renter.transaction.util;

import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.em.MeterReadingDto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * PdfGenerator of demo
 *
 * @author Neeraj Kumar
 * @since 20-Mar-2023 4:50 PM
 */
public class PdfGenerator {
    public static String PDF_FILE = "/app/out/pdf/sample.pdf";

    public static void generateEbillPdf(ElectricMeterDto electricMeterDto) throws FileNotFoundException {
        OutputStream outputStream = new FileOutputStream(new File(PDF_FILE));
        PdfDocument pdfDocument = new PdfDocument(new PdfWriter(outputStream));
        Document document = new Document(pdfDocument);
        document.add(new Paragraph("Bill date: " + FormatCalendar.formatCal(Calendar.getInstance())));
        document.add(new Paragraph("Electric Meter Number: " + electricMeterDto.getSerialNumber()));
        List<MeterReadingDto> meterReadingDtos = electricMeterDto.getMeterReadings();
        createTable("Due Electric Bill", document, meterReadingDtos,
                meterReadingDto -> !meterReadingDto.getPaid(), 100, true);
        createHistoryTable("Electric Bill History", document, meterReadingDtos,
                meterReadingDto -> meterReadingDto.getPaid(), 5, false);
        pdfDocument.close();
    }

    private static void createHistoryTable(String tableHeader, Document document,
                                           List<MeterReadingDto> meterReadingDtos,
                                           Predicate<MeterReadingDto> paid, int limit, boolean footer) {
        final var meterReadings = meterReadingDtos.stream().filter(paid)
                .limit(limit).collect(Collectors.toList());
        if (meterReadings != null && !meterReadings.isEmpty()) {
            document.add(new Paragraph(tableHeader));
            float[] pointColumnWidths = {50F, 150F, 100F, 150F, 40F, 50F, 50F, 150F};
            Table table = new Table(pointColumnWidths);

            table.addCell(createHeaderCell("Id"));
            table.addCell(createHeaderCell("Reading Date"));
            table.addCell(createHeaderCell("Reading"));
            table.addCell(createHeaderCell("Billing Days"));
            table.addCell(createHeaderCell("Units"));
            table.addCell(createHeaderCell("Amount Due"));
            table.addCell(createHeaderCell("Amount Paid"));
            table.addCell(createHeaderCell("Date Paid"));

            AtomicInteger units = new AtomicInteger(0);

            meterReadings.forEach(e -> {
                table.addCell(createDataCell(String.valueOf(e.getId())));
                table.addCell(createDataCell(String.valueOf(FormatCalendar.formatCal(e.getCurrentReadingDate()))));
                table.addCell(createDataCell(String.valueOf(e.getCurrentReading())));
             //   table.addCell(createDataCell(String.valueOf(e.getNumberOfDays())));
             //   table.addCell(createDataCell(String.valueOf(e.getUnitConsumed())));

             //   table.addCell(createDataCell(String.valueOf(e.getAmountDue())));
              //  table.addCell(createDataCell(String.valueOf(e.getAmountPaid())));
             //   table.addCell(createDataCell(String.valueOf(FormatCalendar.formatCal(e.getAmountPaidDate()))));

             //   units.addAndGet(e.getUnitConsumed());
            });
            if (footer) {

                table.addCell(createFooterCell(5,"Total : "));
                table.addCell(createFooterCell(3,String.valueOf(units.get())));

            }
            document.add(table);
        }
    }

    private static void createTable(String tableHeader, Document document,
                                    List<MeterReadingDto> meterReadingDtos,
                                    Predicate<MeterReadingDto> paid, int limit, boolean footer) {
        final var meterReadings = meterReadingDtos.stream().filter(paid)
                .limit(limit).collect(Collectors.toList());
        if (meterReadings != null && !meterReadings.isEmpty()) {
            document.add(new Paragraph(tableHeader));
            float[] pointColumnWidths = {50F, 150F, 100F, 150F, 100F, 50F, 50F, 50F};
            Table table = new Table(pointColumnWidths);

            table.addCell(createHeaderCell("id"));
            table.addCell(createHeaderCell("previous Reading Date"));
            table.addCell(createHeaderCell("previous Reading"));
            table.addCell(createHeaderCell("current Reading Date"));
            table.addCell(createHeaderCell("current Reading"));
            table.addCell(createHeaderCell("unit Consumed"));
            table.addCell(createHeaderCell("amount Due"));
            table.addCell(createHeaderCell("Billing Days"));

            MyDecimal amount = new MyDecimal();

            meterReadings.forEach(e -> {
                table.addCell(createDataCell(String.valueOf(e.getId())));
               // table.addCell(createDataCell(String.valueOf(FormatCalendar.formatCal(e.getPreviousReadingDate()))));
              //  table.addCell(createDataCell(String.valueOf(e.getPreviousReading())));
                table.addCell(createDataCell(String.valueOf(FormatCalendar.formatCal(e.getCurrentReadingDate()))));
                table.addCell(createDataCell(String.valueOf(e.getCurrentReading())));
              //  table.addCell(createDataCell(String.valueOf(e.getUnitConsumed())));
                table.addCell(createDataCell(String.valueOf(findAmountDue(e))));
              //  table.addCell(createDataCell(String.valueOf(e.getNumberOfDays())));
                amount.add(findAmountDue(e));
            });
            if (footer) {
                final var footerCell = createFooterCell(6, "Total Amount: ");
                footerCell.setTextAlignment(TextAlignment.RIGHT);
                table.addCell(footerCell);
                table.addCell(createFooterCell(1,String.valueOf(amount.getDecimal())));
                table.addCell(createFooterCell(1,""));

            }
            document.add(table);
        }
    }

    private static double findAmountDue(MeterReadingDto e) {
        return 0;//(e.getBalanceAmount() != null && !e.getBalanceAmount().equals(0.0)) ? e.getBalanceAmount() : e.getAmountDue();
    }

    private static Cell createHeaderCell(String value) {
        Cell c1 = createCell(value);                   // Creating cell 1
        setCellColor(c1, headerColors, 0.2f);
        return c1;
    }

    private static Cell createDataCell(String value) {
        Cell c1 = createCell(value);                 // Creating cell 1
        setCellColor(c1, dataRowColors, 0.2f);
        return c1;
    }

    private static Cell createFooterCell(int colspan,String value) {

        Cell cell = new Cell(1,colspan);
        cell.add(new Paragraph(value));
        cell.setTextAlignment(TextAlignment.CENTER);// Creating cell 1
        setCellColor(cell, footerColors, 0f);
        cell.setBorder(Border.NO_BORDER);
        return cell;
    }

    /**
     * Method to set color in cell,
     * index 0 background and index 1 is border color
     */

    private static Color[] footerColors = new Color[]{ColorConstants.ORANGE, ColorConstants.DARK_GRAY};
    private static Color[] headerColors = new Color[]{ColorConstants.LIGHT_GRAY, ColorConstants.DARK_GRAY};
    private static Color[] dataRowColors = new Color[]{ColorConstants.WHITE, ColorConstants.DARK_GRAY};

    private static void setCellColor(Cell cell, Color[] colors, float borderWidth) {
        cell.setBackgroundColor(colors[0]);      // Setting background color
        cell.setBorder(new SolidBorder(colors[1], borderWidth));
    }

    private static Cell createCell(String value) {
        Cell cell = new Cell();
        cell.add(new Paragraph(value));
        cell.setTextAlignment(TextAlignment.CENTER);
        return cell;
    }

}

class MyDecimal {
    private double decimal;

    public void add(double number) {
        this.decimal += number;
    }

    public double getDecimal() {
        return this.decimal;
    }
}