package com.renter.transaction.util;

import com.renter.master.entity.Flat;
import com.renter.transaction.entity.payment.Invoice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * MonthlyRentBuilder of demo
 *
 * @author Neeraj Kumar
 * @since 28-Feb-2023 4:15 PM
 */
public class MonthlyRentBuilder {

    public static List<Invoice> createMonthlyRent(Flat flat){
        List<Invoice> monthlyRents = flat.getInvoices();

            if(monthlyRents!= null && !monthlyRents.isEmpty()) {
                Invoice previousMonthlyRent=null;
                monthlyRents.add(initMonthlyRent(previousMonthlyRent,flat));
            }else{
                monthlyRents=new ArrayList<>();
                monthlyRents.add(initMonthlyRent( null,flat));
            }

        return monthlyRents;
    }
    private static Invoice initMonthlyRent(Invoice oldMonthlyRent, Flat flat){
        Invoice monthlyRentCurrent=  new Invoice();
        Calendar lastMonthRentFrom=(oldMonthlyRent != null)? oldMonthlyRent.getToDate():Calendar.getInstance();
        monthlyRentCurrent.setFromDate(lastMonthRentFrom);
        Calendar cal= Calendar.getInstance();
        cal.setTime(lastMonthRentFrom.getTime());
        cal.add(Calendar.MONTH,1);
        monthlyRentCurrent.setToDate(cal);
        monthlyRentCurrent.setAmount(flat.getRentAmount());
        monthlyRentCurrent.setPaid(Boolean.FALSE);
        return  monthlyRentCurrent;
    }
}
