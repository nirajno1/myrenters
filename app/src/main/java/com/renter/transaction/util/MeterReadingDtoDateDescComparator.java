package com.renter.transaction.util;

import com.renter.master.dto.em.MeterReadingDto;

import java.util.Comparator;

/**
 * MeterReadingDtoDateDescComparator of demo
 *
 * @author Neeraj
 * @since 21-Feb-2023 9:22 AM
 */
public class MeterReadingDtoDateDescComparator implements Comparator<MeterReadingDto> {
    @Override
    public int compare(MeterReadingDto e2, MeterReadingDto e1) {
        if(e1.getCurrentReadingDate()!= null && e2.getCurrentReadingDate()!= null){
            return e1.getCurrentReadingDate().compareTo(e2.getCurrentReadingDate());
        }else if(e1.getCurrentReadingDate()!= null){
            return -1;
        }else if(e2.getCurrentReadingDate()!= null){
            return 1;
        }else {
            return 0;
        }
    }
}
