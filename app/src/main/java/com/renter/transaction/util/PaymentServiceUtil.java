package com.renter.transaction.util;

import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.dto.flat.FlatDto;
import com.renter.master.dto.rent.MonthlyRentDto;
import com.renter.master.dto.PaymentMasterDto;
import com.renter.transaction.dto.RentDueDetailDto;
import com.renter.transaction.dto.RentDueSummaryDto;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * PaymentServiceUtil of demo
 *
 * @author Neeraj Kumar
 * @since 01-Mar-2023 3:01 PM
 */
public class PaymentServiceUtil {
    public static PaymentMasterDto createPayment(String type, Long id, Double paymentDue,Double balanceAmount,
                                           Long renterId, Long flatId, String paymentType){
        PaymentMasterDto paymentMasterDto= new PaymentMasterDto();
        paymentMasterDto.setPaymentFor(type);
        paymentMasterDto.setPaymentForId(id);
        paymentMasterDto.setReceivedAmount(paymentDue);
        paymentMasterDto.setPaymentReceivedDate(Calendar.getInstance());
        paymentMasterDto.setBalanceAmount(balanceAmount);
        paymentMasterDto.setRenterId(renterId);
        paymentMasterDto.setFlatId(flatId);
        paymentMasterDto.setPaymentType(paymentType);

        paymentMasterDto.setCreatedBy("SYSTEM");
        paymentMasterDto.setCreatedOn(Calendar.getInstance());
        paymentMasterDto.setDeleted(Boolean.FALSE);
        paymentMasterDto.setActive(Boolean.TRUE);

        return paymentMasterDto;

    }
    public static void initDueDetails(FlatDto flatDto, List<RentDueDetailDto> dues) {
        List<MonthlyRentDto> unpaidRentes = findUnPaidMonthlyRents(flatDto.getInvoices());
        List<MeterReadingDto> unPaidMeterReadings = findUnPaidMeterReadings(flatDto.getElectricMeter());
        RentDueDetailDto dueDetailDto = new RentDueDetailDto();
        // rent
        dueDetailDto.setUnPaidRents(unpaidRentes);
        dueDetailDto.setRentDue(calcMonthlyRentAmtDue(unpaidRentes));
        // electric bill
        dueDetailDto.setUnPaidMeterReadings(unPaidMeterReadings);
        dueDetailDto.setElectricBillDue(calcMeterReadingAmtDue(unPaidMeterReadings));
        // general info
        dueDetailDto.setDueCalculationDate(Calendar.getInstance());
        dueDetailDto.setFlatId(flatDto.getId());
        dueDetailDto.setRenterId(flatDto.getRenterId());
        dues.add(dueDetailDto);
    }

    public static void initRentDueSmry(Long renterId, List<RentDueSummaryDto> dues, FlatDto flat) {
        List<MonthlyRentDto> unpaidRentes = findUnPaidMonthlyRents(flat.getInvoices());
        List<MeterReadingDto> unPaidMeterReadings = findUnPaidMeterReadings(flat.getElectricMeter());
        RentDueSummaryDto rentDueSummary = new RentDueSummaryDto();
        rentDueSummary.setRentDue(calcMonthlyRentAmtDue(unpaidRentes));
        rentDueSummary.setElectricBillDue(calcMeterReadingAmtDue(unPaidMeterReadings));
        rentDueSummary.setDueCalculationDate(Calendar.getInstance());
        rentDueSummary.setFlatId(flat.getId());
        rentDueSummary.setRenterId(renterId);
        dues.add(rentDueSummary);
    }

    private static List<MonthlyRentDto> findUnPaidMonthlyRents(List<MonthlyRentDto> monthlyRents) {
        if (monthlyRents != null && !monthlyRents.isEmpty()) {
            return monthlyRents.stream().filter(e -> !e.getPaid()).toList();
        }
        return Collections.emptyList();
    }

    private static Double calcMonthlyRentAmtDue(final List<MonthlyRentDto> unPaidRents) {
        double rentDue = 0.0;
        for (MonthlyRentDto rentDto : unPaidRents) {
            rentDue += rentDto.getAmount();
        }
        return rentDue;
    }

    public static MeterReadingDto findLastMeterReading(List<MeterReadingDto> meterReadings) {
        List<MeterReadingDto> modifiableList = new ArrayList<MeterReadingDto>(meterReadings);
        modifiableList.sort(new MeterReadingDtoDateDescComparator());
        return modifiableList.get(0);
    }
    private static List<MeterReadingDto> findUnPaidMeterReadings(final ElectricMeterDto electricMeter) {
        return electricMeter.getMeterReadings().stream()
                .filter(e -> !e.getPaid()).toList();
    }

    private static Double calcMeterReadingAmtDue(final List<MeterReadingDto> meterReadingDtos) {
        double electricBillDue = 0.0;
        for (MeterReadingDto meterReading : meterReadingDtos) {
     //       electricBillDue += meterReading.getAmountDue();
        }
        return electricBillDue;
    }

}
