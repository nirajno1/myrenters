package com.renter.transaction.util;

import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.entity.MeterReading;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;


/**
 * MeterReadingUtil of my-renter
 * This class will create a meter reading for a given renter with new reading
 *
 * @author Neeraj Kumar
 * @since 06-Apr-2023 2:27 PM
 */
public class MeterReadingUtil {

    public static List<MeterReading> createMeterReadings(List<MeterReading> meterReadings,
                                                         Integer currentReading,
                                                         Long consumedByRenterId, Double unitRate) {
        if (!meterReadings.isEmpty()) {
            MeterReading oldReading = getLatestMeterReading(meterReadings);
            meterReadings.add(getInstance(oldReading, currentReading, consumedByRenterId, unitRate));
        } else {
            meterReadings.add(getInstance(null, currentReading, consumedByRenterId, unitRate));
        }
        return meterReadings;
    }


    public static MeterReading getLatestMeterReading(List<MeterReading> meterReadings) {
        if (meterReadings != null && !meterReadings.isEmpty()) {
            final var meterReading = meterReadings.stream().sorted(new MeterReadingDateDescComparator()).findFirst();
            return meterReading.isPresent() ? meterReading.get() : null;
        }
        return null;
    }

    public static MeterReadingDto getLatestMeterReadingDto(List<MeterReadingDto> meterReadings) {
        if (meterReadings != null && !meterReadings.isEmpty()) {
            final var meterReading = meterReadings.stream().sorted(new MeterReadingDtoDateDescComparator()).findFirst();
            return meterReading.isPresent() ? meterReading.get() : null;
        }
        return null;
    }

    private static MeterReading getInstance(MeterReading oldMeterReadingObj,
                                            Integer currentReading, Long consumedByRenterId, Double unitRate) {
        MeterReading meterReading = new MeterReading();
        Integer oldReading = 0;
        Calendar lastReadingDate = null;
        if(oldMeterReadingObj != null){
            oldReading =   oldMeterReadingObj.getCurrentReading();
            lastReadingDate =oldMeterReadingObj.getCurrentReadingDate();
        }else {
            lastReadingDate=Calendar.getInstance();
        }

        meterReading.setCurrentReading(currentReading);
     //   meterReading.setPreviousReading(oldReading);
        if(Objects.nonNull(currentReading) && Objects.nonNull(oldReading)) {
           // meterReading.setAmountDue((currentReading - oldReading) * unitRate);
        }
       // meterReading.setPreviousReadingDate(lastReadingDate);
        Calendar currentDateTime = Calendar.getInstance();
        meterReading.setCurrentReadingDate(currentDateTime);
        meterReading.setPaid(Boolean.FALSE);
        meterReading.setConsumedByRenter(consumedByRenterId);

        return meterReading;
    }

}
