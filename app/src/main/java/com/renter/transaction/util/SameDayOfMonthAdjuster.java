package com.renter.transaction.util;

import java.time.LocalDate;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;

public class SameDayOfMonthAdjuster implements TemporalAdjuster {
    private final int dayOfMonth;
    public static final int FEBRUARY = 2;
    public SameDayOfMonthAdjuster(int desireDate) {
        this.dayOfMonth = desireDate;
    }

    @Override
    public Temporal adjustInto(Temporal dateToBeAdjusted) {
        LocalDate date = LocalDate.from(dateToBeAdjusted);
        int tmpYear = date.getYear();
        int adjustedDayOfMonth=dayOfMonth;
        final var month = date.getMonth();
        if(month.getValue() == FEBRUARY && dayOfMonth > 28){
            if(date.isLeapYear()) {
                adjustedDayOfMonth = 29;
            }else {
                adjustedDayOfMonth = 28;
            }
        }else if( dayOfMonth > 30 ){
            adjustedDayOfMonth= month.maxLength();
        }
        return LocalDate.of(tmpYear, month.getValue(), adjustedDayOfMonth);
    }
}
