package com.renter.transaction.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

/**
 * FormatCalendar of demo
 *
 * @author
 * @since 20-Feb-2023 4:58 PM
 */
public class FormatCalendar {
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

    public static String formatCal(Calendar calendar) {
        if(Objects.nonNull(calendar)) {
            return sdf.format(calendar.getTime());
        }else {
            return null;
        }
    }
}
