package com.renter.transaction.util;

import com.renter.data.DummyDataGenerator;
import com.renter.master.entity.DropDownList;
import com.renter.master.entity.ElectricMeter;
import com.renter.master.entity.Flat;
import com.renter.master.entity.FlatType;
import com.renter.master.entity.House;
import com.renter.master.repository.CityRepository;
import com.renter.master.repository.CountryRepository;
import com.renter.master.repository.DropDownListRepository;
import com.renter.master.repository.ElectricMeterRepository;
import com.renter.master.repository.FlatRepository;
import com.renter.master.repository.FlatTypeRepository;
import com.renter.master.repository.HouseRepository;
import com.renter.master.repository.RentPaymentRepository;
import com.renter.master.repository.RenterRepository;
import com.renter.master.repository.StateRepository;
import com.renter.transaction.entity.payment.Invoice;
import com.renter.master.entity.MeterReading;
import com.renter.transaction.entity.payment.RentPayment;
import com.renter.master.entity.Renter;
import com.renter.master.entity.ValueObject;
import org.springframework.boot.CommandLineRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.renter.transaction.service.PaymentServiceImpl.ELECTRIC_UNIT_RATE;

/**
 * GenerateDummyData of demo
 *
 * @author Neeraj
 * @since 20-Feb-2023 5:04 PM
 */
public class GenerateDummyData {

    public static RentPayment createRentPayment() {
        RentPayment rent = new RentPayment();
        rent.setRentAmount(2000d);
        Calendar oneMonth = Calendar.getInstance();
        oneMonth.add(Calendar.MONTH, 1);
       // rent.setPaymentDate(Calendar.getInstance());
        //rent.setPaymentType("Account");
        return rent;
    }

    public static List<FlatType> createFlatType() {
        FlatType flatType1 = new FlatType("1BHK", "1 BHK", "Single bed room, hall and kitchen");
        FlatType flatType2 = new FlatType("2BHK", "2 BHK", "2 bed room, hall and kitchen");
        FlatType flatType3 = new FlatType("3BHK", "3 BHK", "3 bed room, hall and kitchen");
        FlatType flatType4 = new FlatType("SingleRoom", "Single Room", "Single bed room");
        return List.of(flatType1, flatType2, flatType3, flatType4);
    }

    public static Flat createFlat(String flatType, ElectricMeter electricMeter) {
        Flat flat = new Flat();
        flat.setFlatNumber("101");
        flat.setFlatType(flatType);
        flat.setElectricMeter(electricMeter);
        flat.setOccupied(Boolean.FALSE);
        flat.setRentAmount(3500.0);
        flat.setInvoices(createMonthlyRent(3, flat));
        return flat;
    }

    public static Renter createRenter() {
        Renter renter = new Renter();
        renter.setName(DummyDataGenerator.generateRandomName());
        renter.setMobileNumber(DummyDataGenerator.generateRandomMobileNum());
        renter.setDob(DummyDataGenerator.generateRandomDob());
        renter.setAddress("address line 2");
        renter.setCity(DummyDataGenerator.generateRandomCity());
        renter.setOccupation(DummyDataGenerator.generateRandomOccupations());
        renter.setState("Bihar");
        renter.setActive(Boolean.TRUE);
        renter.setDeleted(false);
        renter.setCreatedBy("system");
        renter.setCreatedOn(Calendar.getInstance());
        return renter;
    }
    public static List<Renter> createRenters() {
        List<Renter> renters= new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            renters.add(createRenter());
        }
        return renters;
    }

    public static List<ElectricMeter> createElectricMeters() {
        List<ElectricMeter> electricMeters= new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ElectricMeter electricMeter1 =createElectricMeter();
            electricMeters.add(electricMeter1);
        }
        return electricMeters;
    }

    public static ElectricMeter createElectricMeter(){
        return createElectricMeter(
                DummyDataGenerator.generateRandomNumberString(10),
                "Flat-".concat(DummyDataGenerator.generateRandomNumberString(1)).concat("-meter"),
                DummyDataGenerator.generateRandomMeterType());
    }
    private static ElectricMeter createElectricMeter(String sno, String nickName, String type) {
        ElectricMeter electricMeter = new ElectricMeter();
        electricMeter.setSerialNumber(sno);
        electricMeter.setNickName(nickName);
        electricMeter.setType(type);
        electricMeter.setCreatedBy("System");
        electricMeter.setCreatedOn(Calendar.getInstance());
        electricMeter.setActive(Boolean.TRUE);
        electricMeter.setDeleted(Boolean.FALSE);
        return electricMeter;
    }

    private static MeterReading initMeterReading(MeterReading meterReading, MeterReading oldReading, Long consumedByRenterId) {

        Double start = (oldReading != null) ? oldReading.getCurrentReading() : Math.random() * 1000000;
        Double end = start + Math.random() * 1000;

        meterReading.setCurrentReading(Double.valueOf(end).intValue());
      //  meterReading.setPreviousReading(Double.valueOf(start).intValue());
       // meterReading.setAmountDue((Double.valueOf(end).intValue() - Double.valueOf(start).intValue()) * ELECTRIC_UNIT_RATE);

        Calendar lastReadingDate = (oldReading != null) ? oldReading.getCurrentReadingDate() : Calendar.getInstance();

     //   meterReading.setPreviousReadingDate(lastReadingDate);

        Calendar cal = Calendar.getInstance();
        cal.setTime(lastReadingDate.getTime());
        cal.add(Calendar.DAY_OF_YEAR, (int) ((end % 30) + 1));
        meterReading.setCurrentReadingDate(cal);

        meterReading.setPaid(Boolean.FALSE);

        meterReading.setConsumedByRenter(consumedByRenterId);

        return meterReading;
    }

    public static List<MeterReading> createMeterReadings(Integer count, Long consumedByRenterId) {
        List<MeterReading> meterReadings = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            if (i > 0) {
                meterReadings.add(initMeterReading(new MeterReading(), meterReadings.get(i - 1), consumedByRenterId));
            } else {
                meterReadings.add(initMeterReading(new MeterReading(), null, consumedByRenterId));
            }
        }
        return meterReadings;
    }

    public static House createHouse() {
        House house = new House();
        house.setName(DummyDataGenerator.generateHouseName());
        house.setNumber(DummyDataGenerator.generateRandomHouseNumber());
        house.setState("Bihar");
        house.setCity(DummyDataGenerator.generateRandomCity());
        house.setAddress("My lane one");
        house.setActive(Boolean.TRUE);
        house.setDeleted(Boolean.FALSE);
        house.setCreatedOn(Calendar.getInstance());
        house.setCreatedBy(DummyDataGenerator.generateRandomName());
        return house;
    }

    public static List<House> createHouseList() {
        List<House> houses= new ArrayList<>();
        //for(int i=0;i<1;i++){
            houses.add(createHouse());
        //}
        return houses;
    }



    public static List<Invoice> createMonthlyRent(Integer count, Flat flat) {
        List<Invoice> meterReadings = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            if (i > 0) {
                meterReadings.add(initMonthlyRent(new Invoice(), meterReadings.get(i - 1), flat));
            } else {
                meterReadings.add(initMonthlyRent(new Invoice(), null, flat));
            }
        }
        return meterReadings;
    }

    private static Invoice initMonthlyRent(Invoice monthlyRentCurrent, Invoice oldMonthlyRent, Flat flat) {
        Calendar lastMonthRentFrom = (oldMonthlyRent != null) ? oldMonthlyRent.getToDate() : Calendar.getInstance();
        monthlyRentCurrent.setFromDate(lastMonthRentFrom);
        Calendar cal = Calendar.getInstance();
        cal.setTime(lastMonthRentFrom.getTime());
        cal.add(Calendar.MONTH, 1);
        monthlyRentCurrent.setToDate(cal);

        monthlyRentCurrent.setAmount(flat.getRentAmount());

        monthlyRentCurrent.setPaid(Boolean.FALSE);
        return monthlyRentCurrent;
    }

    /**
     * MasterCode should be abbreviated code in upper case
     * and should be prefixed with valueCode
     *
     * @return DropDownlist with values
     */
    public static List<DropDownList> createDropDownList() {
        List<DropDownList> ddLists = new ArrayList<>();
        DropDownList pmntFrequency = createDropDownListObj("PMNTFQ", "Payment Frequency",
                "Rent payment Frequency");
        List<ValueObject> valueObjects = new ArrayList<>();
        valueObjects.add(createValObj("PMNTFQ_MONTH", "Monthly Payment", "Rent payment per month",Boolean.FALSE));
        valueObjects.add(createValObj("PMNTFQ_DAILY", "Daily Payment", "Rent payment per day",Boolean.FALSE));
        valueObjects.add(createValObj("PMNTFQ_YEAR", "Yearly Payment", "Rent payment per year",Boolean.FALSE));
        valueObjects.add(createValObj("PMNTFQ_WEEK", "Weekly Payment", "Rent payment per week",Boolean.TRUE));
        pmntFrequency.setValues(valueObjects);
        ddLists.add(pmntFrequency);

        DropDownList pmntType = createDropDownListObj("PMNTTYP", "Payment Type", "Rent payment type");
        valueObjects = new ArrayList<>();
        valueObjects.add(createValObj("PMNTTYP_ADV", "Advance Payment", "Rent payment in advance",Boolean.TRUE));
        valueObjects.add(createValObj("PMNTTYP_NO_ADV", "No Advance Payment", "Rent not paid in advance",Boolean.FALSE));
        pmntType.setValues(valueObjects);
        ddLists.add(pmntType);
        return ddLists;
    }

    private static DropDownList createDropDownListObj(String name, String displayName, String description) {
        DropDownList ddList = new DropDownList();
        ddList.setName(name);
        ddList.setDisplayName(displayName);
        ddList.setDescription(description);
        return ddList;
    }

    private static ValueObject createValObj(String name, String displayName, String description, Boolean defaultVal) {
        ValueObject vo = new ValueObject();
/*        vo.setKey();Name(name);
        vo.setDisplayName(displayName);
        vo.setDescription(description);*/
        vo.setDefaultVal(defaultVal);
        return vo;
    }
    public CommandLineRunner run(RentPaymentRepository rentPaymentRepository,
                                 RenterRepository renterRepository,
                                 FlatRepository flatRepository,
                                 FlatTypeRepository flatTypeRepository,
                                 ElectricMeterRepository electricMeterRepository,
                                 CountryRepository countryRepository,
                                 StateRepository stateRepository,
                                 CityRepository cityRepository,
                                 HouseRepository houseRepository,
                                 DropDownListRepository dropDownListRepository) throws Exception {
        return (String[] args) -> {

			/*final var allCountries = DataLoader.getAllCountries();
			if(allCountries!=null) {
				countryRepository.saveAll(allCountries);
			}

			final var allStates = DataLoader.getAllStates();
			if(allStates!= null) {
				stateRepository.saveAll(allStates);
			}
			final var allIndianCityPostalCode = DataLoader.getAllIndianCityPostalCode();
			if(allIndianCityPostalCode!= null) {
				cityRepository.saveAll(allIndianCityPostalCode);
			}*/
			/*RentPayment rent= createRentPayment();
			rent=rentPaymentRepository.save(rent);*/

            List<FlatType> flatTypes=flatTypeRepository.saveAll(createFlatType());


            //List<Renter> renters= createRenters();

			/*List<RentPayment> rents=new ArrayList<>();
			rents.add(rent);
			renter.setRentPayments(rents);*/
            //renterRepository.saveAll(renters);
            var renter = createRenter();
            renter=renterRepository.save(renter);

            //List<ElectricMeter> electricMeters=createElectricMeters();
            var electricMeter = createElectricMeter();
            List<MeterReading> meterReadings= createMeterReadings(3,renter.getId());
            electricMeter.setMeterReadings(meterReadings);



            electricMeter=electricMeterRepository.save(electricMeter);

            House house=createHouse();
            houseRepository.save(house);
            houseRepository.saveAll(createHouseList());
            Flat flat=createFlat("flatTypes.get(2)",electricMeter);
            flat.setHouse(house);
            flat=flatRepository.save(flat);
            //renter.setRentedFlat(List.of(flat));
            //renterRepository.save(renter);
            List<DropDownList> dropDownLists=GenerateDummyData.createDropDownList();
            dropDownListRepository.saveAll(dropDownLists);
        };
    }

}
