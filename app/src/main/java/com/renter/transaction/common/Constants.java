package com.renter.transaction.common;

/**
 * Constants of my-renter
 *
 * @author Neeraj Kumar
 * @since 02-May-2023 5:36 PM
 */
public final class Constants {
     private Constants() {
     }

     public static final String PMT_MONTHLY = "PMT_MNT";
     public static final String PMT_YEARLY = "PMT_YER";
     public static final String PMT_WEEKLY = "PMT_WEK";
     public static final String PMT_DAILY = "PMT_DLY";
     public static final String PMT_ADV = "PMT_ADV";
     public static final String PMT_NO_ADV = "PMT_NO_ADV";

     public static final String INV_TYP_RENT="INV_TYP_RENT";
     public static final String INV_TYP_ELEC="INV_TYP_ELEC";

     public static final String USER_ID = "system";
}
