package com.renter.transaction.controller;

import com.renter.master.controller.BaseMasterController;
import com.renter.master.dto.flat.FlatDto;
import com.renter.transaction.dto.FlatRenterDto;
import com.renter.transaction.dto.RentFlatDto;
import com.renter.transaction.service.RentFlatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * RentPaymentController of demo
 *
 * @since 11-Dec-2022 3:47 PM
 */
@RestController
@RequestMapping(value = "/rentflat")
public class RentFlatController implements BaseMasterController<RentFlatDto> {

    @Autowired
    private RentFlatService rentFlatService;

    @PostMapping()
    public ResponseEntity<RentFlatDto> save(@RequestBody RentFlatDto rentFlatDto){
        RentFlatDto  rentFlatDto1 = rentFlatService.save(rentFlatDto);
        return Objects.nonNull(rentFlatDto1)?new ResponseEntity<>(rentFlatDto1,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @GetMapping("/view")
    public List<FlatRenterDto> getAllRentersList(){
        return rentFlatService.getAllFlatRenterLists();
    }

    @GetMapping()
    public ResponseEntity<List<RentFlatDto>> findAll(){
        List<RentFlatDto> list= rentFlatService.findAll();
        return Objects.nonNull(list)?new ResponseEntity<>(list,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }



    @Override
    @GetMapping("/{id}")
    public ResponseEntity<RentFlatDto> findById(@PathVariable Long id) {
        RentFlatDto rentFlatDto=rentFlatService.findById(id);
        return Objects.nonNull(rentFlatDto)?new ResponseEntity<>(rentFlatDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    @GetMapping("/valid")
    public ResponseEntity<List<RentFlatDto>> findAllActiveAndUnDeleted() {
        List<RentFlatDto> flatDtos=rentFlatService.findAllActiveAndUnDeleted();
        return Objects.nonNull(flatDtos)?new ResponseEntity<>(flatDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping("/validView")
    public ResponseEntity<List<FlatRenterDto>> findAllActiveAndUnDeletedView() {
        List<FlatRenterDto> flatDtos=rentFlatService.getAllFlatRenterLists();
        return Objects.nonNull(flatDtos)?new ResponseEntity<>(flatDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        Boolean deleted= rentFlatService.deleteById(id);
        return  deleted?new ResponseEntity<>("Deleted Successfully",HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
