package com.renter.transaction.controller;

import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.service.MeterReadingService;
import com.renter.transaction.service.EmSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * EMeterSearchController of demo
 *
 * @author
 * @since 22-Feb-2023 1:49 PM
 */
@RestController
@RequestMapping(value = "/emsearch")
public class EMeterSearchController {
    @Autowired
    private EmSearchService emSearchService;
    @Autowired
    private MeterReadingService meterReadingService;


    @GetMapping(value = "/queryByRenter/{id}")
    public ResponseEntity<ElectricMeterDto> queryByRenter(@PathVariable Long id) {
        ElectricMeterDto electricMeterDto= null;
        return Objects.nonNull(electricMeterDto)?new ResponseEntity<>(electricMeterDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/queryByFlat/{id}")
    public ResponseEntity<ElectricMeterDto> queryByFlat(@PathVariable Long id) {
        ElectricMeterDto electricMeterDto= null;
        return Objects.nonNull(electricMeterDto)?new ResponseEntity<>(electricMeterDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping(value = "/queryByMeter/{id}")
    public ResponseEntity<ElectricMeterDto> queryByMeter(@PathVariable Long id) {
        ElectricMeterDto electricMeterDto= emSearchService.queryByMeter(id);
        return Objects.nonNull(electricMeterDto)?new ResponseEntity<>(electricMeterDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
