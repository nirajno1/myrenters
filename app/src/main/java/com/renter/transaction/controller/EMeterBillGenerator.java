package com.renter.transaction.controller;


import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.transaction.service.EmSearchService;
import com.renter.transaction.util.PdfGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Objects;

/**
 * EMeterBillGenerator of demo
 *
 * @author Neeraj Kumar
 * @since 20-Mar-2023 12:04 PM
 */
@RestController
@RequestMapping(value = "/ebill")
public class EMeterBillGenerator {
    @Autowired
    private EmSearchService emSearchService;

    @GetMapping(value = "/pdf/{meterId}")
    public ResponseEntity<ElectricMeterDto> generateEBillByMeterId(@PathVariable Long meterId) {
        ElectricMeterDto electricMeterDto= emSearchService.queryByMeter(meterId);
        try {
            PdfGenerator.generateEbillPdf(electricMeterDto);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return Objects.nonNull(electricMeterDto)?new ResponseEntity<>(electricMeterDto, HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
