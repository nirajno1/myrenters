package com.renter.transaction.controller;

import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.dto.flat.FlatDto;
import com.renter.master.dto.rent.MonthlyRentDto;
import com.renter.master.service.FlatService;
import com.renter.transaction.service.RentSearchService;
import com.renter.transaction.util.MeterReadingUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * EMeterSearchController of demo
 *
 * @author
 * @since 22-Feb-2023 1:49 PM
 */
@RestController
@RequestMapping(value = "/rentsearch")
public class RentSearchController {
    @Autowired
    private RentSearchService rentSearchService;

    @Autowired
    private FlatService flatService;
    @GetMapping(value = "/queryByRenter/{id}")
    public ResponseEntity<List<MonthlyRentDto>> queryByRenter(@PathVariable Long id) {
        List<MonthlyRentDto> rentDtos = rentSearchService.queryByRenter(id);
        return Objects.nonNull(rentDtos) ? new ResponseEntity<>(rentDtos, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(value = "/queryByFlat/{id}")
    public ResponseEntity<List<MonthlyRentDto>> queryByFlat(@PathVariable Long id) {
        List<MonthlyRentDto> rentDtos = rentSearchService.queryByFlat(id);
        return Objects.nonNull(rentDtos) ? new ResponseEntity<>(rentDtos, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @Value("${electric.unit.rate}") Double electricUnitRate;
    record BookFlatDataDto(Double actualRent, Integer previousMeterReading,
                           String meterNickName,
                           Double electricUnitRate){}

    @GetMapping(value = "/queryFlatActualRent/{id}")
    public ResponseEntity<BookFlatDataDto> queryByFlatActualRent(@PathVariable Long id) {
        FlatDto flatDto = flatService.findById(id);
        Double actualRent=flatDto.getRentAmount();
        ElectricMeterDto electricMeterDto=flatDto.getElectricMeter();
        String meterName="";
        Integer previousMeterReading=0;
        if(electricMeterDto!= null ){
            meterName= electricMeterDto.getNickName();
            List<MeterReadingDto> meterReadingDtos= electricMeterDto.getMeterReadings();
            final var latestMeterReadingDto = MeterReadingUtil.getLatestMeterReadingDto(meterReadingDtos);
            if(latestMeterReadingDto != null) {
                previousMeterReading = latestMeterReadingDto.getCurrentReading();
            }
        }
        var dto= new BookFlatDataDto(actualRent,previousMeterReading,meterName,electricUnitRate);




        return Objects.nonNull(dto) ? new ResponseEntity<>(dto, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
