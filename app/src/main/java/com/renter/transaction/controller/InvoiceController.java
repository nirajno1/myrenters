package com.renter.transaction.controller;

import com.renter.transaction.dto.InvoicePaymentDto;
import com.renter.transaction.dto.InvoiceResponse;
import com.renter.transaction.service.InvoiceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * InvoiceController of my-renter
 *
 * @author Neeraj Kumar
 * @since 02-May-2023 4:13 PM
 */
@RestController
@RequestMapping("/invoice")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @GetMapping
    ResponseEntity<List<InvoiceResponse>> getInvoices(){
        return  new ResponseEntity<>(invoiceService.getAllInvoice(), HttpStatus.OK);
    }
    @GetMapping("/{invoiceId}")
    ResponseEntity<InvoiceResponse> getInvoiceById(@PathVariable Long invoiceId){
        return  new ResponseEntity<>(invoiceService.getInvoiceById(invoiceId), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<InvoicePaymentDto> saveInvoicePayment(@RequestBody InvoicePaymentDto paymentDto){
       return new ResponseEntity<>(invoiceService.saveInvoicePayment(paymentDto), HttpStatus.OK);
    }

    @GetMapping("/balanceDue/{invoiceId}")
    ResponseEntity<Double> getInvoiceBalanceDueById(@PathVariable Long invoiceId){
        return  new ResponseEntity<>(invoiceService.getInvoiceBalanceDueById(invoiceId), HttpStatus.OK);
    }

}
