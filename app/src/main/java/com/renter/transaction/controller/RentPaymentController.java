package com.renter.transaction.controller;

import com.renter.master.dto.PaymentMasterDto;
import com.renter.transaction.dto.PaymentRequestDto;
import com.renter.transaction.dto.RentDueDetailDto;
import com.renter.transaction.dto.RentDueSummaryDto;
import com.renter.transaction.dto.RentPaymentDto;
import com.renter.master.service.RentPaymentService;
import com.renter.transaction.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * RentPaymentController of demo
 *
 * @since 11-Dec-2022 3:47 PM
 */
@RestController
@RequestMapping(value = "/rentPayment")
public class RentPaymentController {
    @Autowired
    RentPaymentService rentPaymentService;

    @Autowired
    PaymentService paymentService;

    @GetMapping(value = "/health")
    public ResponseEntity<String> healthTest() {
        return new ResponseEntity<>("RentDto Payment Controller Health is fine", HttpStatus.OK);
    }
    @GetMapping()
    public List<RentPaymentDto> getAllRents(){
        return rentPaymentService.getAllRentPayments();
    }

    @GetMapping(value = "/rentDue/{renterId}")
    public List<RentDueSummaryDto> findAmountDueSmryByRenterId(@PathVariable Long renterId){
        return paymentService.findAmountDueSmryByRenterId(renterId);
    }

    @GetMapping(value = "/rentDueDtlByRenter/{renterId}")
    public List<RentDueDetailDto> findAmountDueDtlByRenterId(@PathVariable Long renterId){
        return paymentService.findAmountDueDtlByRenterId(renterId);
    }

    @GetMapping(value = "/rentDueDtlByFlat/{flatId}")
    public List<RentDueDetailDto> findAmountDueDtlByFlatId(@PathVariable Long flatId){
        return paymentService.findAmountDueDtlByFlatId(flatId);
    }

    @PostMapping(value = "/acceptPayment")
    public void acceptPayment(@RequestBody PaymentRequestDto paymentRequest){
        System.out.println("Received paymentRequest: "+paymentRequest);
        paymentService.acceptPayment(paymentRequest);
    }

    @GetMapping(value = "/history/{renterid}")
    public ResponseEntity<List<PaymentMasterDto>> paymentHistory(@PathVariable Long renterid){
        System.out.println("Received renterid: "+renterid);
        return new ResponseEntity<>(paymentService.paymentHistory(renterid), HttpStatus.OK);
    }
}

