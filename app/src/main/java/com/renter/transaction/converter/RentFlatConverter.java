package com.renter.transaction.converter;

import com.renter.master.converter.BaseConverter;
import com.renter.master.dto.ValueObjectDto;
import com.renter.master.dto.flat.FlatDto;
import com.renter.master.dto.renter.AddressDto;
import com.renter.master.dto.renter.RenterDto;
import com.renter.master.entity.Address;
import com.renter.master.entity.Flat;
import com.renter.master.entity.Renter;
import com.renter.master.entity.ValueObject;
import com.renter.transaction.dto.RentFlatDto;
import com.renter.transaction.entity.RentFlat;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * FlatConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class RentFlatConverter implements BaseConverter<RentFlatDto, RentFlat> {
    @Autowired
    BaseConverter<FlatDto, Flat> flatConverter;

    @Autowired
    BaseConverter<AddressDto, Address> adderssConverter;
    @Autowired
    BaseConverter<RenterDto, Renter> renterConverter;
    @Autowired
    BaseConverter<ValueObjectDto, ValueObject> valueObjectConverter;
    @Override
    public RentFlatDto entityToDto(RentFlat rentFlat) {
        RentFlatDto rentFlatDto = new RentFlatDto();
        BeanUtils.copyProperties(rentFlat, rentFlatDto);
        Flat flat=rentFlat.getFlat();
        if(Objects.nonNull(flat)) {
            FlatDto flatDto = flatConverter.entityToDto(flat);
            rentFlatDto.setFlat(flatDto);
        }
        Renter rent= rentFlat.getRenter();
        if(Objects.nonNull(rent)){
            RenterDto renterDto=renterConverter.entityToDto(rent);
            rentFlatDto.setRenter(renterDto);
        }
        rentFlatDto.setPmntAdvance(valueObjectConverter.entityToDto(rentFlat.getPmntAdvance()));
        rentFlatDto.setPmntFrequency(valueObjectConverter.entityToDto(rentFlat.getPmntFrequency()));
        return rentFlatDto;
    }

    @Override
    public RentFlat dtoToEntity(RentFlatDto rentFlatDto) {
        RentFlat rentFlat = new RentFlat();
        BeanUtils.copyProperties(rentFlatDto, rentFlat);
        FlatDto flatDto=rentFlatDto.getFlat();
        if(Objects.nonNull(flatDto)) {
            Flat flat = flatConverter.dtoToEntity(rentFlatDto.getFlat());
            rentFlat.setFlat(flat);
        }
        RenterDto renterDto= rentFlatDto.getRenter();
        if(Objects.nonNull(renterDto)){
            Renter renter=renterConverter.dtoToEntity(renterDto);
            rentFlat.setRenter(renter);
        }

        rentFlat.setPmntAdvance(valueObjectConverter.dtoToEntity(rentFlatDto.getPmntAdvance()));
        rentFlat.setPmntFrequency(valueObjectConverter.dtoToEntity(rentFlatDto.getPmntFrequency()));
        return rentFlat;
    }
}
