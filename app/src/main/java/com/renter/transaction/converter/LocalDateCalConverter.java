package com.renter.transaction.converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * LocalDateTimeCalanderConverter of my-renter
 *
 * @author Neeraj Kumar
 * @since 23-Jun-2023 4:28 PM
 */
public class LocalDateCalConverter {
    public static LocalDate calendarToLocalDate(Calendar cal) {
        if(Objects.nonNull(cal)) {
            int year = cal.get(Calendar.YEAR);
            // calendar month starts with 0, but in LocalDate month starts with 1
            int month = cal.get(Calendar.MONTH) + 1;
            int day = cal.get(Calendar.DAY_OF_MONTH);
            return LocalDate.of(year, month, day);
        }
        return null;
    }

    public static  Calendar localDateToCalendar(LocalDate localDate) {
        Calendar calendar = null;
        if(Objects.nonNull(localDate)) {
            ZoneId zoneId = ZoneId.systemDefault();
            Date date = Date.from(localDate.atStartOfDay(zoneId).toInstant());
            calendar = Calendar.getInstance();
            calendar.setTime(date);
        }
        return calendar;
    }
}
