package com.renter.transaction.dto;

import java.util.Calendar;

public record InvoicePmntResponse(Long id, Long invoiceId, Double amountReceived, Calendar dateReceived, String paymentMethod){}