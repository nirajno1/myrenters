package com.renter.transaction.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;

/**
 * RentDueSummaryDto of demo
 *
 * @author Neeraj Kumar
 * @since 26-Feb-2023 4:11 PM
 */
@Data
@NoArgsConstructor
public class RentDueSummaryDto {
    private Double rentDue;
    private Double electricBillDue;
    private Calendar dueCalculationDate;
    private Long renterId;
    private Long flatId;
}
