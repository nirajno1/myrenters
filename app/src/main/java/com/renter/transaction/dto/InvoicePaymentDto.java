package com.renter.transaction.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;

/**
 * InvoicePaymentDto of my-renter
 *
 * @author Neeraj Kumar
 * @since 09-May-2023 3:41 PM
 */
@Getter
@Setter
@NoArgsConstructor
public class InvoicePaymentDto {
    private Long id;
    private Long invoiceId;
    private Double amountReceived;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar dateReceived;
    private String paymentMethod;


    public InvoicePaymentDto(Long id, Long invoiceId, Double amountReceived, Calendar dateReceived, String paymentMethod) {
        this.id = id;
        this.invoiceId = invoiceId;
        this.amountReceived = amountReceived;
        this.dateReceived = dateReceived;
        this.paymentMethod = paymentMethod;
    }
}
