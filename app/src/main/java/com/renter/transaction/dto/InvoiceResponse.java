package com.renter.transaction.dto;

import java.util.Calendar;
import java.util.List;

public record InvoiceResponse(Long id, Double amount,Double amountDue, Calendar issueDate,
                              Calendar paymentDueDate, Calendar fromDate, Calendar toDate,
                              Boolean paid, Long renterId, String particular, List<InvoicePmntResponse> pmnts){}