package com.renter.transaction.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

/**
 * RentPaymentDto of demo
 *
 * @since 13-Dec-2022 11:51 AM
 */
@Data
@NoArgsConstructor
public class RentPaymentDto implements Serializable {
    private Long id;
    private BigDecimal rentAmount;
    private BigDecimal electricBillAmount;
    private String rentMonth;
    private Calendar billGenerationDate;
    private Calendar rentStartDate;
    private Calendar rentEndDate;
    private Calendar paymentDate;
    private String paymentType;
    private RentPaymentDto lastPayment;
}
