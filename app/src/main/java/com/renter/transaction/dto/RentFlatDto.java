package com.renter.transaction.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.renter.master.dto.AuditDto;
import com.renter.master.dto.ValueObjectDto;
import com.renter.master.dto.flat.FlatDto;
import com.renter.master.dto.renter.RenterDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Calendar;

/**
 * FlatDto of MyRenters
 *
 * @since 12-Dec-2022 3:49 PM
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class RentFlatDto extends AuditDto implements Serializable {
    private Long id;
    private FlatDto flat;
    private RenterDto renter;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar rentStartDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar rentEndDate;

    private ValueObjectDto pmntFrequency;
    private ValueObjectDto pmntAdvance;
    private Double rentAmountAgreed;
    private Double electricRateAgreed;
    private Integer currentMeterReading;
}
