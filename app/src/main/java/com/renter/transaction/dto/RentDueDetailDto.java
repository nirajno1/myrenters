package com.renter.transaction.dto;

import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.dto.rent.MonthlyRentDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;
import java.util.List;

/**
 * RentDueSummaryDto of demo
 *
 * @author Neeraj Kumar
 * @since 26-Feb-2023 4:11 PM
 */
@Data
@NoArgsConstructor
public class RentDueDetailDto {
    private Double rentDue;
    private Double electricBillDue;
    private Calendar dueCalculationDate;
    private Long renterId;
    private Long flatId;
    List<MeterReadingDto> unPaidMeterReadings;
    List<MonthlyRentDto> unPaidRents;
}
