package com.renter.transaction.dto;

import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.dto.rent.MonthlyRentDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * PaymentRequestDto of demo
 *
 * @author Neeraj Kumar
 * @since 28-Feb-2023 6:14 PM
 */
@Data
@NoArgsConstructor
public class PaymentRequestDto {
    private Double amountReceived;
    private Long renterId;
    private Long flatId;
    private String paymentType;
    List<MeterReadingDto> selectedMeterReadings;
    List<MonthlyRentDto> selectedRents;
}
