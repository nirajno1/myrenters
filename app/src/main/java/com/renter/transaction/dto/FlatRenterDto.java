package com.renter.transaction.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;

/**
 * FlatRenterDto of demo
 *
 * @since 11-Jan-2023 10:11 AM
 */
@Data
@NoArgsConstructor
public class FlatRenterDto {
    Long id;
    String rentedFlat;
    String renterName;
    private Calendar rentStartDate;
    private String pmntFrequency;
    private String pmntAdvance;

    private Double rentAmountAgreed;

    private Double electricRateAgreed;
    private Boolean active;

    public FlatRenterDto(String rentedFlat, String renterName) {
        this.rentedFlat = rentedFlat;
        this.renterName = renterName;

    }

    public FlatRenterDto(Long id, String rentedFlat, String renterName,
                         Calendar rentStartDate, String pmntFrequency,
                         String pmntAdvance, Double rentAmountAgreed,
                         Double electricRateAgreed, Boolean active) {
        this.id = id;
        this.rentedFlat = rentedFlat;
        this.renterName = renterName;
        this.rentStartDate = rentStartDate;
        this.pmntFrequency = pmntFrequency;
        this.pmntAdvance = pmntAdvance;
        this.rentAmountAgreed=rentAmountAgreed;
        this.electricRateAgreed=electricRateAgreed;
        this.active= active;
    }
}
