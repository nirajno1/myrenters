package com.renter.camel;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;

/**
 * FileRoute of demo
 *
 * @since 11-Jan-2023 3:52 PM
 */
public class FileRoute extends RouteBuilder {
    String COUNTRY="file://home/neeraj.kumar/devCode/myRenters/app/src/main/resources/country.csv?noop=true";
    String STATE="/home/neeraj.kumar/devCode/myRenters/app/src/main/resources/country_states.csv";
    @Override
    public void configure() throws Exception {
        System.out.println("I am in configure");
        from(COUNTRY).process(this::process);
    }

    public void process(Exchange exchange) {
        final String body = exchange.getMessage().getBody(String.class);
        System.out.println("Updated body: " + body);
    }



}
