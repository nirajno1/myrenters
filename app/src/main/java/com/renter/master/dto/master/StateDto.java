package com.renter.master.dto.master;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CountryDto of demo
 *
 * @since 11-Jan-2023 3:29 PM
 */
@Data
@NoArgsConstructor
public class StateDto {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String countryCode;
    private String code;
    private String name;
    private String type;
}
