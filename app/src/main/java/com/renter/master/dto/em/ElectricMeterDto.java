package com.renter.master.dto.em;

import com.renter.master.dto.AuditDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * FlatDto of MyRenters
 *
 * @since 12-Dec-2022 3:49 PM
 */
@Getter
@Setter
@NoArgsConstructor
public class ElectricMeterDto extends AuditDto implements Serializable {
    private Long id;
    private String serialNumber;
    private String type;
    private String nickName;
    private Boolean used = Boolean.FALSE;
    private List<MeterReadingDto> meterReadings;
}
