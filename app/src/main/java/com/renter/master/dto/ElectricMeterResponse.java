package com.renter.master.dto;

public record ElectricMeterResponse(Long id, String serialNumber, String nickName) {
}
