package com.renter.master.dto.search;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EMQueryByFlatDto of demo
 *
 * @author
 * @since 22-Feb-2023 1:55 PM
 */
@Data
@NoArgsConstructor
public class EMQueryByRenterDto {
    private Long id;
    private String name;
    private Boolean active;
}
