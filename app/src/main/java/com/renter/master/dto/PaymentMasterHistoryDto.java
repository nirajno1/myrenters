package com.renter.master.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;

/**
 * PaymentMaster of demo
 *
 * @author Neeraj Kumar
 * @since 01-Mar-2023 11:42 AM
 */
@Data
@NoArgsConstructor
public class PaymentMasterHistoryDto {
    private Integer historyId;
    private Integer id;
    private Calendar paymentReceivedDate;
    private String paymentType;
    private Double receivedAmount;
    private Double balanceAmount;
    private Long renterId;
    private Long flatId;
    private Boolean deleted;
    private Boolean active;
    private Calendar createdOn;
    private Calendar modifiedOn;
    private String createdBy;
    private String modifiedBy;
}
