package com.renter.master.dto.dropdown;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.management.ConstructorParameters;

/**
 * EmeterDto of demo
 *
 * @author
 * @since 22-Feb-2023 2:58 PM
 */
@Data
@NoArgsConstructor
public class EmeterDropDownDto {
    private Long id;
    private String nickName;

    private Boolean active ;

    public EmeterDropDownDto(Long id, String nickName,Boolean active) {
        this.id = id;
        this.nickName = nickName;
        this.active= active;
    }
}
