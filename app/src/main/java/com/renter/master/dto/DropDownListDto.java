package com.renter.master.dto;

import com.renter.master.entity.DropDownList;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link DropDownList} entity
 */
@Data
public class DropDownListDto implements Serializable {
    private Long id;
    private String name;
    private String displayName;
    private String description;
    private List<ValueObjectDto> values;
}