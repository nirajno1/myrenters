package com.renter.master.dto.dropdown;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EmeterDto of demo
 *
 * @author
 * @since 22-Feb-2023 2:58 PM
 */
@Data
@NoArgsConstructor
public class RenterDropDownDto {
    private Long id;
    private String name;
    private Boolean active;

    public RenterDropDownDto(Long id, String name, Boolean active) {
        this.id = id;
        this.name = name;
        this.active = active;
    }
}
