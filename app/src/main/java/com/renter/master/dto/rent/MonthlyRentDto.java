package com.renter.master.dto.rent;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;

/**
 * Invoice of demo
 *
 * @author Neeraj
 * @since 23-Feb-2023 1:26 PM
 */

@Setter @Getter @NoArgsConstructor
public class MonthlyRentDto {
    private Long id;
    private Double amount;
    private Calendar issueDate;
    private Calendar fromDate;
    private Calendar toDate;
    private Long renterId;
    private Boolean paid;

}
