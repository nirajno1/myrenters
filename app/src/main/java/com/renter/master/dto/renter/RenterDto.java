package com.renter.master.dto.renter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.renter.master.dto.AuditDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Calendar;

/**
 * RenterDto of MyRenters
 *
 * @since 12-Dec-2022 4:53 PM
 */
@Getter
@Setter
@NoArgsConstructor
public class RenterDto extends AuditDto implements Serializable {
    private Long id;
    private String name;
    private String mobileNumber;
    private String occupation;
    private String address;
    private String city;
    private String state;
    private String imgBase64;
    private Long renterImageId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar dob;
    private Boolean vacated = Boolean.FALSE;
    private Calendar vacatedOn;
}
