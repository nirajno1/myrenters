package com.renter.master.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Calendar;

/**
 * AuditDto of my-renter
 *
 * @author Neeraj Kumar
 * @since 29-Mar-2023 11:05 AM
 */
@Data
public class AuditDto {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar createdOn;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar modifiedOn;

    private String createdBy;
    private String modifiedBy;
    private Boolean active;
    private Boolean deleted;
    private Calendar deletedOn;
}
