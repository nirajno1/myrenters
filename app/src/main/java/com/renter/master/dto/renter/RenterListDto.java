package com.renter.master.dto.renter;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * RenterDto of MyRenters
 *
 * @since 12-Dec-2022 4:53 PM
 */
@Data
@NoArgsConstructor
public class RenterListDto implements Serializable {
    private Long id;
    private String name;
}
