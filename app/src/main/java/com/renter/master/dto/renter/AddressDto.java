package com.renter.master.dto.renter;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * AddressDto of MyRenters
 *
 * @since 12-Dec-2022 1:56 PM
 */
@Data
@NoArgsConstructor
public class AddressDto implements Serializable {
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String distric;
    private String state;
    private String pincode;
}
