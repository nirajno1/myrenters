package com.renter.master.dto.dropdown;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EmeterDto of demo
 *
 * @author
 * @since 22-Feb-2023 2:58 PM
 */
@Data
@NoArgsConstructor
public class FlatDropDownDto {
    private Long id;
    private String flatNumber;
    private Boolean active;

    public FlatDropDownDto(Long id, String flatNumber,Boolean active) {
        this.id = id;
        this.flatNumber = flatNumber;
        this.active= active;
    }
}
