package com.renter.master.dto.dropdown;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EmeterDto of demo
 *
 * @author
 * @since 22-Feb-2023 2:58 PM
 */
@Data
@NoArgsConstructor
public class RentFlatDropDownDto {
    private Long id;
    private String flatNumber;

    public RentFlatDropDownDto(Long id, String flatNumber) {
        this.id = id;
        this.flatNumber = flatNumber;
    }
}
