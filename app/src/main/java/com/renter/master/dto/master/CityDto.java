package com.renter.master.dto.master;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * City of demo
 *
 * @since 12-Jan-2023 10:30 AM
 */
@Data
@NoArgsConstructor
public class CityDto {
   private Long id;
   private String countryCode;
   private String postalCode;
   private String placeName;
   private String stateName;
   private String cityName;
   private String communityName;

}
