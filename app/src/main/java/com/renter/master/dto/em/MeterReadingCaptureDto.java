package com.renter.master.dto.em;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.renter.transaction.util.FormatCalendar;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;

/**
 * MeterReadingCaptureDto of demo
 *
 * @author Neeraj
 * @since 21-Feb-2023 11:33 AM
 */
@Data
@NoArgsConstructor
public class MeterReadingCaptureDto {
    private Integer meterReading;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar meterReadingDate;
    private String meterSn;


    @Override
    public String toString() {
        return "MeterReadingCaptureDto{" +
                "meterReading=" + meterReading +
                ", meterReadingDate=" + FormatCalendar.formatCal(meterReadingDate) +
                ", meterSn='" + meterSn + '\'' +
                '}';
    }
}
