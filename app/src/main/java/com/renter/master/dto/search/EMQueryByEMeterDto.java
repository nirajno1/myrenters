package com.renter.master.dto.search;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EMQueryByFlatDto of demo
 *
 * @author
 * @since 22-Feb-2023 1:55 PM
 */
@Data
@NoArgsConstructor
public class EMQueryByEMeterDto {
    private Long id;
    private String nickName;
    private Boolean active ;
}
