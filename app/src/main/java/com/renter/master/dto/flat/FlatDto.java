package com.renter.master.dto.flat;

import com.renter.master.dto.HouseDto;
import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.rent.MonthlyRentDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 * FlatDto of MyRenters
 * Entity Flat
 *
 * @since 12-Dec-2022 3:49 PM
 */
@Data
@NoArgsConstructor
public class FlatDto implements Serializable {
    private Long id;
    private String flatNumber;
    private String flatType;
    private ElectricMeterDto electricMeter;
    private Boolean occupied;
    private Boolean active = Boolean.TRUE;
    private Double rentAmount;
    private Calendar rentDueDate;
    private Long renterId;
    private List<MonthlyRentDto> invoices;
    private HouseDto house;

}
