package com.renter.master.dto.em;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.renter.transaction.util.FormatCalendar;
import jakarta.persistence.Basic;
import jakarta.persistence.FetchType;
import jakarta.persistence.Lob;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

/**
 * FlatDto of MyRenters
 *
 * @since 12-Dec-2022 3:49 PM
 */
@Data
@NoArgsConstructor
public class MeterReadingDto implements Serializable {
    private Long id;
    private Integer currentReading;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar currentReadingDate;

    private Integer previousReading;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar previousReadingDate;
    private Long consumedByRenter;
    private Boolean paid;

    /* Image related properties */
    private String imgBase64;
    private Long imgId;

    /* electric meter related to establish relationship */
    private Long emId;
    private String emSerialNumber;
    private String emNickName;
}
