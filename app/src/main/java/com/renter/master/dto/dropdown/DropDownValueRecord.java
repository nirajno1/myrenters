package com.renter.master.dto.dropdown;

public record DropDownValueRecord(Long id, String name, String displayName) {
}
