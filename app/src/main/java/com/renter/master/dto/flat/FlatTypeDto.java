package com.renter.master.dto.flat;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * FlatDto of MyRenters
 *
 * @since 12-Dec-2022 3:49 PM
 */
@Data
@NoArgsConstructor
public class FlatTypeDto implements Serializable {
    private Integer id;
    private String flatTypeCode;
    private String flatTypeName;
    private String description;
}
