package com.renter.master.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * A DTO for the {@link com.renter.master.entity.ValueObject} entity
 * This is used for dropdown lists value list objects
 */
@Getter
@Setter
@NoArgsConstructor
public class ValueObjectDto extends AuditDto implements Serializable {
    private Long id;
    private String ddKey;
    private String ddValue;
    private String display;
    private Boolean defaultVal;
}