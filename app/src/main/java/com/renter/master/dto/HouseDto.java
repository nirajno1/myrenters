package com.renter.master.dto;

import com.renter.master.dto.flat.FlatDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link com.renter.master.entity.House} entity
 */
@Getter
@Setter
@NoArgsConstructor
public class HouseDto extends AuditDto implements Serializable {
    private Long id;
    private String name;
    private String number;
    private String address;
    private String city;
    private String state;
    private List<FlatDto> flats;
}