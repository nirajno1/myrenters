package com.renter.master.converter;

import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.entity.ElectricMeter;
import com.renter.master.entity.Image;
import com.renter.master.entity.MeterReading;
import com.renter.transaction.util.ImageUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;

/**
 * MeterReadingConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class MeterReadingConverter implements BaseConverter<MeterReadingDto, com.renter.master.entity.MeterReading> {

    @Override
    public MeterReadingDto entityToDto(com.renter.master.entity.MeterReading meterReading) {
        MeterReadingDto meterReadingDto = new MeterReadingDto();
        BeanUtils.copyProperties(meterReading, meterReadingDto);
        Image image= meterReading.getMeterImage();
        if(Objects.nonNull(image)) {
            String imgBase64 = ImageUtil.createImageBase64String(image);
            meterReadingDto.setImgBase64(imgBase64);
            meterReadingDto.setImgId(image.getId());
        }
        final var electricMeter = meterReading.getElectricMeter();
        meterReadingDto.setEmId(electricMeter.getId());
        meterReadingDto.setEmSerialNumber(electricMeter.getSerialNumber());
        meterReadingDto.setEmNickName(electricMeter.getNickName());

        /* update previous reading details */
        MeterReading previousReading= meterReading.getPreviousReading();
        if(Objects.nonNull(previousReading)) {
            meterReadingDto.setPreviousReading(previousReading.getCurrentReading());
            meterReadingDto.setPreviousReadingDate(previousReading.getCurrentReadingDate());
        }else{
            meterReadingDto.setPreviousReading(meterReading.getCurrentReading());
            meterReadingDto.setPreviousReadingDate(meterReading.getCurrentReadingDate());
        }
        return meterReadingDto;
    }

    @Override
    public com.renter.master.entity.MeterReading dtoToEntity(MeterReadingDto meterReadingDto) {
        com.renter.master.entity.MeterReading meterReading = new com.renter.master.entity.MeterReading();
        ElectricMeter electricMeter= new ElectricMeter();
        electricMeter.setId(meterReadingDto.getEmId());
        if(StringUtils.hasText(meterReadingDto.getImgBase64())) {
            Image meterReadingImage = ImageUtil.createImageObject(meterReadingDto.getImgId(),
                    meterReadingDto.getImgBase64());
            meterReading.setMeterImage(meterReadingImage);
        }
        BeanUtils.copyProperties(meterReadingDto, meterReading);
        meterReading.setElectricMeter(electricMeter);
        return meterReading;
    }

    @Override
    public List<MeterReadingDto> entityToDtoList(List<com.renter.master.entity.MeterReading> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<com.renter.master.entity.MeterReading> dtoToEntityList(List<MeterReadingDto> list) {
        return list.stream().map(this::dtoToEntity).toList();
    }
}
