package com.renter.master.converter;

import com.renter.master.dto.master.CountryDto;
import com.renter.master.entity.master.Country;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * CountryConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class CountryConverter implements BaseConverter<CountryDto, Country> {

    @Override
    public CountryDto entityToDto(Country country) {
        CountryDto countryDto = new CountryDto();
        BeanUtils.copyProperties(country, countryDto);
        return countryDto;
    }

    @Override
    public Country dtoToEntity(CountryDto countryDto) {
        Country country = new Country();
        BeanUtils.copyProperties(countryDto, country);
        return country;
    }

    @Override
    public List<CountryDto> entityToDtoList(List<Country> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<Country> dtoToEntityList(List<CountryDto> list) {
        return list.stream().map(this::dtoToEntity).toList();
    }
}
