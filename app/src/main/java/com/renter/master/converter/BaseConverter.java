package com.renter.master.converter;

import java.util.List;

public interface BaseConverter<D,E> {
    D entityToDto(E e);
    E dtoToEntity(D d);

    default List<D> entityToDtoList(List<E> list){
        return list.stream().map(this::entityToDto).toList();
    }
    default List<E> dtoToEntityList(List<D> list) {
        return list.stream().map(this::dtoToEntity).toList();
    }
}
