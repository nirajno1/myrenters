package com.renter.master.converter;

import com.renter.master.dto.flat.FlatTypeDto;
import com.renter.master.entity.FlatType;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * FlatConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class FlatTypeConverter implements BaseConverter<FlatTypeDto, FlatType> {
    @Override
    public FlatTypeDto entityToDto(FlatType flatType) {
        FlatTypeDto flatTypeDto = new FlatTypeDto();
        BeanUtils.copyProperties(flatType, flatTypeDto);
        return flatTypeDto;
    }

    @Override
    public FlatType dtoToEntity(FlatTypeDto flatTypeDto) {
        FlatType flatType=null;
        if(Objects.nonNull(flatTypeDto)) {
            flatType = new FlatType();
            BeanUtils.copyProperties(flatTypeDto, flatType);
        }
        return flatType;
    }

    @Override
    public List<FlatTypeDto> entityToDtoList(List<FlatType> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<FlatType> dtoToEntityList(List<FlatTypeDto> list) {
        return list.stream().map(this::dtoToEntity).toList();
    }
}
