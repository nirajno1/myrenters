package com.renter.master.converter;

import com.renter.master.dto.master.CityDto;
import com.renter.master.entity.master.City;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * CityConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class CityConverter implements BaseConverter<CityDto, City> {

    @Override
    public CityDto entityToDto(City city) {
        CityDto cityDto = new CityDto();
        BeanUtils.copyProperties(city, cityDto);
        return cityDto;
    }

    @Override
    public City dtoToEntity(CityDto cityDto) {
        City city = new City();
        BeanUtils.copyProperties(cityDto, city);
        return city;
    }

    @Override
    public List<CityDto> entityToDtoList(List<City> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<City> dtoToEntityList(List<CityDto> list) {
        return list.stream().map(this::dtoToEntity).toList();
    }
}
