package com.renter.master.converter;

import com.renter.master.dto.renter.RenterDto;
import com.renter.master.entity.Image;
import com.renter.master.entity.Renter;
import com.renter.transaction.util.ImageUtil;
import org.bouncycastle.util.Arrays;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Base64;
import java.util.Objects;

/**
 * FlatConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class RenterConverter implements BaseConverter<RenterDto, Renter> {

    @Autowired
    private AuditConverter auditConverter;

    @Override
    public RenterDto entityToDto(Renter renter) {
        RenterDto renterDto = new RenterDto();
        BeanUtils.copyProperties(renter, renterDto);
        final var renterImage = renter.getRenterImage();
        if(Objects.nonNull(renterImage)) {
            String imgBase64 = ImageUtil.createImageBase64String(renterImage);
            renterDto.setImgBase64(imgBase64);
            renterDto.setRenterImageId(renterImage.getId());
        }
        return renterDto;
    }

    @Override
    public Renter dtoToEntity(RenterDto renterDto) {
        Renter renter = new Renter();
        BeanUtils.copyProperties(renterDto, renter);
        String imgBase64 = renterDto.getImgBase64();
        if(StringUtils.hasText(imgBase64)) {
            Image renterImage = ImageUtil.createImageObject(renterDto.getRenterImageId(), imgBase64);
            renter.setRenterImage(renterImage);
        }
        return renter;
    }
}
