package com.renter.master.converter;

import com.renter.master.dto.PaymentMasterDto;
import com.renter.transaction.entity.payment.PaymentMaster;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * StateConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class PaymentMasterConverter implements BaseConverter<PaymentMasterDto, PaymentMaster> {
    @Override
    public PaymentMasterDto entityToDto(PaymentMaster paymentMaster) {
        PaymentMasterDto paymentMasterDto = new PaymentMasterDto();
        BeanUtils.copyProperties(paymentMaster, paymentMasterDto);
        return paymentMasterDto;
    }

    @Override
    public PaymentMaster dtoToEntity(PaymentMasterDto paymentMasterDto) {
        PaymentMaster paymentMaster = new PaymentMaster();
        BeanUtils.copyProperties(paymentMasterDto, paymentMaster);
        return paymentMaster;
    }

}
