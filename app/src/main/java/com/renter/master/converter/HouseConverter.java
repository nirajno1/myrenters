package com.renter.master.converter;

import com.renter.master.dto.HouseDto;
import com.renter.master.dto.flat.FlatDto;
import com.renter.master.dto.renter.AddressDto;
import com.renter.master.entity.Address;
import com.renter.master.entity.Flat;
import com.renter.master.entity.House;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * HouseConverter of demo
 *
 * @author Neeraj Kumar
 * @since 22-Mar-2023 2:15 PM
 */
@Component
public class HouseConverter implements BaseConverter<HouseDto, House>{

    @Override
    public HouseDto entityToDto(House house) {
        HouseDto houseDto= new HouseDto();
        BeanUtils.copyProperties(house,houseDto,"flats");
        // do not use flat converter as it creates cyclic dependency
        List<Flat> flatList=house.getFlats();

        if(flatList!= null && !flatList.isEmpty()){
            List<FlatDto> flatDtos = flatList.stream().map(flat -> {
                FlatDto flatDto = new FlatDto();
                BeanUtils.copyProperties(flat, flatDto);
                return flatDto;
            }).collect(Collectors.toList());
            houseDto.setFlats(flatDtos);
        }
        return houseDto;
    }

    @Override
    public House dtoToEntity(HouseDto houseDto) {
        House entity = new House();
        BeanUtils.copyProperties(houseDto, entity, "flats");
        List<FlatDto> flatList = houseDto.getFlats();
        if (flatList != null && !flatList.isEmpty()) {
            List<Flat> flats = flatList.stream().map(dto -> {
                Flat flat = new Flat();
                BeanUtils.copyProperties(dto, flat);
                return flat;
            }).collect(Collectors.toList());
            entity.setFlats(flats);
        }
        return entity;
    }
}
