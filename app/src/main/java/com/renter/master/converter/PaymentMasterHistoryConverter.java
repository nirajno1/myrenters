package com.renter.master.converter;

import com.renter.master.dto.PaymentMasterHistoryDto;
import com.renter.transaction.entity.payment.PaymentMasterHistory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * StateConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class PaymentMasterHistoryConverter implements BaseConverter<PaymentMasterHistoryDto, PaymentMasterHistory> {
    @Override
    public PaymentMasterHistoryDto entityToDto(PaymentMasterHistory paymentMasterHistory) {
        PaymentMasterHistoryDto paymentMasterHistoryDto = new PaymentMasterHistoryDto();
        BeanUtils.copyProperties(paymentMasterHistory, paymentMasterHistoryDto);
        return paymentMasterHistoryDto;
    }

    @Override
    public PaymentMasterHistory dtoToEntity(PaymentMasterHistoryDto paymentMasterHistoryDto) {
        PaymentMasterHistory paymentMasterHistory = new PaymentMasterHistory();
        BeanUtils.copyProperties(paymentMasterHistoryDto, paymentMasterHistory);
        return paymentMasterHistory;
    }

}
