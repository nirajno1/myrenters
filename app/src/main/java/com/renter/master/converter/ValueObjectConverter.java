package com.renter.master.converter;

import com.renter.master.dto.DropDownListDto;
import com.renter.master.dto.ValueObjectDto;
import com.renter.master.entity.DropDownList;
import com.renter.master.entity.ValueObject;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * StateConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class ValueObjectConverter implements BaseConverter<ValueObjectDto, ValueObject> {

    @Override
    public ValueObjectDto entityToDto(ValueObject entity) {
        ValueObjectDto dto = new ValueObjectDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public ValueObject dtoToEntity(ValueObjectDto dto) {
        ValueObject entity = new ValueObject();
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }
}
