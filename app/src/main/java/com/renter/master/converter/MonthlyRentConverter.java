package com.renter.master.converter;

import com.renter.master.dto.rent.MonthlyRentDto;
import com.renter.transaction.entity.payment.Invoice;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * FlatConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class MonthlyRentConverter implements BaseConverter<MonthlyRentDto, Invoice> {

    @Override
    public MonthlyRentDto entityToDto(Invoice entity) {
        MonthlyRentDto dto = new MonthlyRentDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public Invoice dtoToEntity(MonthlyRentDto dto) {
        Invoice entity = new Invoice();
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }
}
