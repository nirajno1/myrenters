package com.renter.master.converter;

import com.renter.master.dto.master.StateDto;
import com.renter.master.entity.master.State;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * StateConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class StateConverter implements BaseConverter<StateDto, State> {

    @Override
    public StateDto entityToDto(State state) {
        StateDto stateDto = new StateDto();
        BeanUtils.copyProperties(state, stateDto);
        return stateDto;
    }

    @Override
    public State dtoToEntity(StateDto stateDto) {
        State state = new State();
        BeanUtils.copyProperties(stateDto, state);
        return state;
    }

    @Override
    public List<StateDto> entityToDtoList(List<State> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<State> dtoToEntityList(List<StateDto> list) {
        return list.stream().map(this::dtoToEntity).toList();
    }
}
