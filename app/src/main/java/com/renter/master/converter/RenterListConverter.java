package com.renter.master.converter;

import com.renter.master.dto.renter.RenterListDto;
import com.renter.master.entity.Renter;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * FlatConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class RenterListConverter implements BaseConverter<RenterListDto, Renter>{


    @Override
    public RenterListDto entityToDto(Renter renter) {
        RenterListDto renterListDto= new RenterListDto();
        renterListDto.setId(renter.getId());
        renterListDto.setName(renter.getName());
        return renterListDto;
    }

    @Override
    public Renter dtoToEntity(RenterListDto renterListDto) {
        return null;
    }

    @Override
    public List<RenterListDto> entityToDtoList(List<Renter> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<Renter> dtoToEntityList(List<RenterListDto> list) {
        return Collections.EMPTY_LIST;
    }
}
