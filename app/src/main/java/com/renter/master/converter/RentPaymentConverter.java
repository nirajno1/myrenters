package com.renter.master.converter;

import com.renter.transaction.dto.RentPaymentDto;
import com.renter.transaction.entity.payment.RentPayment;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * FlatConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class RentPaymentConverter implements BaseConverter<RentPaymentDto, RentPayment>{

    @Override
    public RentPaymentDto entityToDto(RentPayment rent) {
        RentPaymentDto dto = new RentPaymentDto();
        BeanUtils.copyProperties(rent, dto);
        return dto;
    }

    @Override
    public RentPayment dtoToEntity(RentPaymentDto rentDto) {
        return null;
    }

    @Override
    public List<RentPaymentDto> entityToDtoList(List<RentPayment> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<RentPayment> dtoToEntityList(List<RentPaymentDto> list) {
        return null;
    }

}
