package com.renter.master.converter;

import com.renter.master.dto.AuditDto;
import com.renter.master.dto.renter.AddressDto;
import com.renter.master.entity.Address;
import com.renter.master.entity.Audit;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * AuditConverter of my-renter
 *
 * @author Neeraj Kumar
 * @since 29-Mar-2023 2:43 PM
 */
@Component
public class AuditConverter {

    public void entityToDto(Audit audit,AuditDto auditDto) {
        BeanUtils.copyProperties(audit, auditDto);
    }


    public void dtoToEntity(AuditDto auditDto,Audit audit) {
        BeanUtils.copyProperties(auditDto, audit);
    }
}
