package com.renter.master.converter;

import com.renter.master.dto.renter.AddressDto;
import com.renter.master.entity.Address;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * FlatConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class AddressConverter implements BaseConverter<AddressDto, Address>{

    @Override
    public AddressDto entityToDto(Address address) {
        AddressDto dto = new AddressDto();
        BeanUtils.copyProperties(address, dto);
        return dto;
    }

    @Override
    public Address dtoToEntity(AddressDto addressDto) {
        return null;
    }

    @Override
    public List<AddressDto> entityToDtoList(List<Address> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<Address> dtoToEntityList(List<AddressDto> list) {
        return null;
    }
}
