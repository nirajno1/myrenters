package com.renter.master.converter;

import com.renter.master.dto.HouseDto;
import com.renter.master.dto.flat.FlatDto;
import com.renter.master.dto.flat.FlatTypeDto;
import com.renter.master.entity.Flat;
import com.renter.master.entity.FlatType;
import com.renter.master.entity.House;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * FlatConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class FlatConverter implements BaseConverter<FlatDto, Flat> {
    @Autowired
    private FlatTypeConverter flatTypeConverter;
    @Autowired
    private ElectricMeterConverter electricMeterConverter;

    @Autowired
    private BaseConverter<HouseDto, House> hosueConverter;

    @Autowired
    private MonthlyRentConverter monthlyRentConverter;
    @Override
    public FlatDto entityToDto(Flat flat) {
        FlatDto flatDto = new FlatDto();
        BeanUtils.copyProperties(flat, flatDto);
        final var electricMeter = flat.getElectricMeter();
        final var electricMeterDto = electricMeterConverter.entityToDto(electricMeter);
        flatDto.setElectricMeter(electricMeterDto);
        if(flat.getInvoices() != null && !flat.getInvoices().isEmpty()) {
            flatDto.setInvoices(monthlyRentConverter.entityToDtoList(flat.getInvoices()));
        }
        if(flat.getHouse() != null) {
            flatDto.setHouse(hosueConverter.entityToDto(flat.getHouse()));
        }
        return flatDto;
    }

    @Override
    public Flat dtoToEntity(FlatDto flatDto) {
        Flat flat = new Flat();
        BeanUtils.copyProperties(flatDto, flat);
        flat.setElectricMeter(electricMeterConverter.dtoToEntity(flatDto.getElectricMeter()));
        if(flatDto.getInvoices() != null && !flatDto.getInvoices().isEmpty()) {
            flat.setInvoices(monthlyRentConverter.dtoToEntityList(flatDto.getInvoices()));
        }
        if(flatDto.getHouse() != null) {
            flat.setHouse(hosueConverter.dtoToEntity(flatDto.getHouse()));
        }
        return flat;
    }

    @Override
    public List<FlatDto> entityToDtoList(List<Flat> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<Flat> dtoToEntityList(List<FlatDto> list) {
        return list.stream().map(this::dtoToEntity).toList();
    }
}
