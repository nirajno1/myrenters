package com.renter.master.converter;

import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.entity.ElectricMeter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * ElectricMeterConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class ElectricMeterConverter implements BaseConverter<ElectricMeterDto, ElectricMeter> {
    @Autowired MeterReadingConverter meterReadingConverter;
    @Override
    public ElectricMeterDto entityToDto(ElectricMeter electricMeter) {
        ElectricMeterDto electricMeterDto= null;
        if(Objects.nonNull(electricMeter)) {
            electricMeterDto = new ElectricMeterDto();
            BeanUtils.copyProperties(electricMeter, electricMeterDto, "meterReadings");
            if(electricMeter.getMeterReadings() != null && !electricMeter.getMeterReadings().isEmpty()) {
                List<MeterReadingDto> meterReadingDtos = meterReadingConverter.entityToDtoList(electricMeter.getMeterReadings());

                electricMeterDto.setMeterReadings(meterReadingDtos);
            }
        }
        return electricMeterDto;
    }

    @Override
    public ElectricMeter dtoToEntity(ElectricMeterDto electricMeterDto) {
        ElectricMeter electricMeter= null;
        if(Objects.nonNull(electricMeterDto)) {
            electricMeter = new ElectricMeter();
            BeanUtils.copyProperties(electricMeterDto, electricMeter, "meterReadings");
            if (electricMeterDto.getMeterReadings() != null && !electricMeterDto.getMeterReadings().isEmpty()) {
                List<com.renter.master.entity.MeterReading> meterReadings = meterReadingConverter.dtoToEntityList(electricMeterDto.getMeterReadings());
                electricMeter.setMeterReadings(meterReadings);
            }
        }
        return electricMeter;
    }

    @Override
    public List<ElectricMeterDto> entityToDtoList(List<ElectricMeter> list) {
        return list.stream().map(this::entityToDto).toList();
    }

    @Override
    public List<ElectricMeter> dtoToEntityList(List<ElectricMeterDto> list) {
        return list.stream().map(this::dtoToEntity).toList();
    }
}
