package com.renter.master.converter;

import com.renter.master.dto.DropDownListDto;
import com.renter.master.dto.ValueObjectDto;
import com.renter.master.dto.master.StateDto;
import com.renter.master.entity.DropDownList;
import com.renter.master.entity.ValueObject;
import com.renter.master.entity.master.State;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * StateConverter of demo
 *
 * @since 13-Dec-2022 10:13 AM
 */
@Component
public class DropDownListConverter implements BaseConverter<DropDownListDto, DropDownList> {

    @Autowired
    BaseConverter<ValueObjectDto,ValueObject> valueObjectConverter;
    @Override
    public DropDownListDto entityToDto(DropDownList entity) {
        DropDownListDto dto = new DropDownListDto();
        BeanUtils.copyProperties(entity, dto);
        if(entity.getValues()!= null && !entity.getValues().isEmpty()){
           List<ValueObjectDto> valueObjectDtos=
                   valueObjectConverter.entityToDtoList(entity.getValues());
           dto.setValues(valueObjectDtos);
        }
        return dto;
    }

    @Override
    public DropDownList dtoToEntity(DropDownListDto dto) {
        DropDownList entity = new DropDownList();
        BeanUtils.copyProperties(dto, entity);
        if(dto.getValues()!= null && !dto.getValues().isEmpty()){
            List<ValueObject> valueObjects=
                    valueObjectConverter.dtoToEntityList(dto.getValues());
            entity.setValues(valueObjects);
        }
        return entity;
    }
}
