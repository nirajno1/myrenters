package com.renter.master.repository;

import com.renter.master.dto.dropdown.EmeterDropDownDto;
import com.renter.master.entity.ElectricMeter;
import com.renter.master.entity.MeterReading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ElectricMeterRepository extends JpaRepository<ElectricMeter, Long> {

    @Query("select em.meterReadings from ElectricMeter em where em.id = ?1")
    List<MeterReading> findAllMeterReadingsById(Long id);

    ElectricMeter findBySerialNumber(String sn);

    @Query("select new com.renter.master.dto.dropdown.EmeterDropDownDto(em.id,em.nickName, em.active)" +
            " from ElectricMeter em ")
    List<EmeterDropDownDto> findAllElectricMeter();

    List<ElectricMeter> findAllByActiveAndDeleted(boolean active, boolean deleted);

    List<ElectricMeter> findAllByActiveAndDeletedAndUsed(boolean active, boolean deleted, boolean used);

    @Query( value = "Select rt.RENTER_ID  from ELECTRIC_METER em join FLAT flt on " +
            "em.id =flt.ELECTRIC_METER_ID join RENT_FLAT rt on rt.FLAT_ID = flt.ID  " +
            "where em.id=:electricMeterId and rt.active=true and rt.deleted=false",
            nativeQuery = true)
    Long findRenterIdByEMeterId(@Param("electricMeterId") Long electricMeterId);


}