package com.renter.master.repository;

import com.renter.transaction.entity.payment.RentPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentPaymentRepository extends JpaRepository<RentPayment,Integer> {
}
