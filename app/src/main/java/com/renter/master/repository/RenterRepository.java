package com.renter.master.repository;

import com.renter.transaction.dto.FlatRenterDto;
import com.renter.master.dto.dropdown.RenterDropDownDto;
import com.renter.master.entity.Renter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RenterRepository extends CrudRepository<Renter,Long> {
/*   @Query(value ="select new com.renter.transaction.dto.FlatRenterDto(" +
           "rentedFlat.flatNumber, renter.name )" +
            "  from com.renter.master.entity.Renter as renter " +
           " join renter.rentedFlat rentedFlat ")
    List<FlatRenterDto> getAllFlatRenterLists();*/
    @Query("select " +
            "new com.renter.master.dto.dropdown.RenterDropDownDto(renter.id,renter.name, renter.active)" +
            " from Renter renter where renter.active = true and renter.deleted= false")
    public List<RenterDropDownDto> findAllRenters();

    List<Renter> findAllByActiveAndDeleted(Boolean active , Boolean deleted);
}
