package com.renter.master.repository;

import com.renter.master.entity.master.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {
    List<City> getAllCityByCountryCodeAndStateName(String countryCode, String stateName);
}