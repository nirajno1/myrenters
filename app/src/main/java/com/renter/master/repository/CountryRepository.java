package com.renter.master.repository;

import com.renter.master.entity.master.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
}