package com.renter.master.repository;

import com.renter.master.entity.ValueObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ValueObjectRepository extends JpaRepository<ValueObject, Long> {
    List<ValueObject> findAllByActiveAndDeleted(boolean active, boolean deleted);

    List<ValueObject> findAllByDdKey(String ddKey);

    @Query("Select distinct v.ddKey from ValueObject v ")
    List<String> findAllDdKey();

    List<ValueObject> findAllByDdKeyAndActiveAndDeleted(String ddVal, boolean active, boolean deleted);
}