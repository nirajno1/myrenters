package com.renter.master.repository;

import com.renter.master.dto.dropdown.FlatDropDownDto;
import com.renter.master.entity.Flat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlatRepository extends JpaRepository<Flat,Long> {

    @Query("select " +
            "new com.renter.master.dto.dropdown.FlatDropDownDto(f.id,f.flatNumber,f.active)" +
            " from Flat f ")
    List<FlatDropDownDto> findAllFlat();

    List<Flat> findAllByActiveAndDeleted(boolean active, boolean deleted);
}
