package com.renter.master.repository;

import com.renter.master.entity.MeterReading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MeterReadingRepository extends JpaRepository<MeterReading, Long> {
    @Query(" from MeterReading mreading where mreading.electricMeter.id = ?1")
    MeterReading findLatestMeterReadingsByMeterId(Long id);

    List<MeterReading> findAllByElectricMeterId(Long id);
}