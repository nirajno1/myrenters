package com.renter.master.repository;

import com.renter.master.dto.dropdown.RentFlatDropDownDto;
import com.renter.master.entity.House;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface HouseRepository extends JpaRepository<House, Long> {
    List<House> findAllByActiveAndDeleted(boolean active, boolean deleted);
    @Query("select " +
            "new com.renter.master.dto.dropdown.RentFlatDropDownDto(f.id,f.flatNumber)" +
            " from House h join Flat f on h.id=f.house.id where h.id=:id and f.occupied = false " +
            " and f.active= true and f.deleted= false ")
    List<RentFlatDropDownDto> getAvailableFlatsByHouseId(@Param("id") Long id);
}