package com.renter.master.repository;

import com.renter.transaction.entity.payment.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonthlyRentRepository extends JpaRepository<Invoice, Long> {
}