package com.renter.master.repository;

import com.renter.master.entity.DropDownList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DropDownListRepository extends JpaRepository<DropDownList, Long> {
    public Optional<DropDownList> findByName(String name);
}