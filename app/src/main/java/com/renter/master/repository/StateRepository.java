package com.renter.master.repository;

import com.renter.master.entity.master.State;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StateRepository extends JpaRepository<State, Long> {
    List<State> findAllByCountryCode(String countryCode);
}