package com.renter.master.repository;

import com.renter.master.entity.FlatType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlatTypeRepository extends JpaRepository<FlatType, Integer> {
}