package com.renter.master.repository;

import com.renter.transaction.entity.payment.PaymentMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentMasterRepository extends JpaRepository<PaymentMaster, Integer> {
    List<PaymentMaster> findAllByRenterId(Long renterid);
}