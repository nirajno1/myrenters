package com.renter.master.service;

import com.renter.master.converter.BaseConverter;
import com.renter.master.dto.master.CountryDto;
import com.renter.master.entity.master.Country;
import com.renter.master.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ElectricMeterServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class CountryServiceImpl implements CountryService {
    @Autowired
    CountryRepository countryRepository;
    @Autowired
    BaseConverter<CountryDto,Country> countryConverter;
    @Override
    public List<CountryDto> getAllCountries() {
        final var countries = countryRepository.findAll();
        return  countryConverter.entityToDtoList(countries);
    }
}
