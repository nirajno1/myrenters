package com.renter.master.service;

import com.renter.transaction.dto.RentPaymentDto;

import java.util.List;

public interface RentPaymentService {
    List<RentPaymentDto> getAllRentPayments();
}
