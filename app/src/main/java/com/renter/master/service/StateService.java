package com.renter.master.service;

import com.renter.master.dto.master.StateDto;

import java.util.List;

public interface StateService {
    List<StateDto> getAllStateByCountryCode(String countryCode);
}

