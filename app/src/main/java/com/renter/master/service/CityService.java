package com.renter.master.service;

import com.renter.master.dto.master.CityDto;

import java.util.List;

public interface CityService {

    List<CityDto> getAllCityByCountryCodeAndStateName(String countryCode, String state);
}

