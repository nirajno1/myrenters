package com.renter.master.service;

import com.renter.master.converter.DropDownListConverter;
import com.renter.master.converter.ValueObjectConverter;
import com.renter.master.dto.DropDownListDto;
import com.renter.master.dto.ValueObjectDto;
import com.renter.master.entity.DropDownList;
import com.renter.master.entity.ValueObject;
import com.renter.master.repository.DropDownListRepository;
import com.renter.master.repository.ValueObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * ElectricMeterServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class DropDownListServiceImpl implements DropDownListService {

    @Autowired
    private DropDownListRepository dropDownListRepository;

    @Autowired
    private DropDownListConverter downListConverter;

    @Autowired
    private ValueObjectConverter valueObjectConverter;


    @Autowired
    private ValueObjectRepository valueObjectRepository;


    @Override
    public List<DropDownListDto> getAllDropdownList() {
        List<DropDownList> dropDownList= dropDownListRepository.findAll();
        return downListConverter.entityToDtoList(dropDownList);
    }

    @Override
    public DropDownListDto saveDropdownList(DropDownListDto dropDownListDto){
        DropDownList dropDownList= dropDownListRepository.save(downListConverter.dtoToEntity(dropDownListDto));
        return downListConverter.entityToDto(dropDownList);
    }

    @Override
    public List<ValueObjectDto> findAll() {
        List<ValueObject> valueObjects=valueObjectRepository.findAll();
        return valueObjectConverter.entityToDtoList(valueObjects);
    }

    @Override
    public List<ValueObjectDto> findAllActiveAndUnDeleted() {
        List<ValueObject> valueObjects=valueObjectRepository.findAllByActiveAndDeleted(true,false);
        return valueObjectConverter.entityToDtoList(valueObjects);
    }

    @Override
    public ValueObjectDto findById(Long id) {
        Optional<ValueObject> vo=valueObjectRepository.findById(id);
        if(vo.isPresent()) {
          return  valueObjectConverter.entityToDto(vo.get());
        }
        return null;
    }

    @Override
    public ValueObjectDto save(ValueObjectDto voDto) {
        ValueObject vo= valueObjectConverter.dtoToEntity(voDto);
        if(vo.getId() != null){
            vo.setModifiedBy("System");
            vo.setModifiedOn(Calendar.getInstance());
        }else{
            vo.setCreatedBy("System");
            vo.setCreatedOn(Calendar.getInstance());
            vo.setDeleted(Boolean.FALSE);
        }
       return valueObjectConverter.entityToDto(valueObjectRepository.save(vo));
    }

    @Override
    public Boolean deleteById(Long id) {
        Optional<ValueObject> optional=valueObjectRepository.findById(id);
        if(optional.isPresent()){
            ValueObject valueObject=optional.get();
            if(Boolean.TRUE.equals(valueObject.getDeleted())){
                return Boolean.FALSE;
            }
            valueObject.setDeleted(true);
            valueObject.setDeletedOn(Calendar.getInstance());
            valueObjectRepository.save(valueObject);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    @Override
    public List<ValueObjectDto> getAllVoByDdKey(String ddKey) {
        List<ValueObject> dropDownList= valueObjectRepository.findAllByDdKey(ddKey);
        return valueObjectConverter.entityToDtoList(dropDownList);
    }

    @Override
    public List<String> getAllKeys() {
        return valueObjectRepository.findAllDdKey();
    }

    @Override
    public List<ValueObjectDto> findAllValidByDdKey(String ddVal) {
        List<ValueObject> valueObjects=valueObjectRepository.findAllByDdKeyAndActiveAndDeleted(ddVal,true,false);
        return valueObjectConverter.entityToDtoList(valueObjects);
    }
}
