package com.renter.master.service;

import com.renter.master.converter.BaseConverter;
import com.renter.master.dto.master.CityDto;
import com.renter.master.entity.master.City;
import com.renter.master.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ElectricMeterServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityRepository cityRepository;
    @Autowired
    BaseConverter<CityDto, City> cityConverter;
    @Override
    public List<CityDto> getAllCityByCountryCodeAndStateName(String countryCode, String stateName) {
        return cityConverter.entityToDtoList(cityRepository.getAllCityByCountryCodeAndStateName(countryCode,stateName));
    }
}
