package com.renter.master.service;

import com.renter.master.converter.RentPaymentConverter;
import com.renter.transaction.dto.RentPaymentDto;
import com.renter.master.repository.RentPaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * FlatServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class RentPaymentServiceImpl implements RentPaymentService {
    @Autowired
    RentPaymentRepository rentPaymentRepository;

    @Autowired
    RentPaymentConverter rentPaymentConverter;

    @Override
    public List<RentPaymentDto> getAllRentPayments() {
        return rentPaymentConverter.entityToDtoList(rentPaymentRepository.findAll());
    }

}
