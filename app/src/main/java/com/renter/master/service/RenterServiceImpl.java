package com.renter.master.service;

import com.renter.master.converter.BaseConverter;
import com.renter.master.dto.renter.RenterDto;
import com.renter.master.dto.renter.RenterListDto;
import com.renter.master.entity.Image;
import com.renter.master.entity.Renter;
import com.renter.master.repository.ImageRepository;
import com.renter.master.repository.RenterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * FlatServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class RenterServiceImpl implements RenterService {
    @Autowired
    private RenterRepository renterRepository;

    @Autowired
    private BaseConverter<RenterDto, Renter> renterConverter;

    @Autowired
    private BaseConverter<RenterListDto, Renter> renterListConverter;

    @Autowired
    private ImageRepository imageRepository;

    public List<RenterDto> getAllRenters() {
        return renterConverter.entityToDtoList((List<Renter>) renterRepository.findAll());
    }

    @Override
    public RenterDto findRenterById(Long id) {
        Optional<Renter> optional = renterRepository.findById(id);
        if (optional.isPresent()) {
            return renterConverter.entityToDto(optional.get());
        } else {
            return null;
        }
    }

    @Override
    public RenterDto saveRenter(RenterDto renterDto) {
        Renter renter= renterConverter.dtoToEntity(renterDto);
        Image renterImage= renter.getRenterImage();
        if(Objects.nonNull(renterImage)){
            renterImage =  imageRepository.save(renterImage);
            renter.setRenterImage(renterImage);
        }
        return renterConverter.entityToDto(renterRepository.save(renter));
    }

    @Override
    public Boolean deleteRenter(Long renterId) {
        Optional<Renter> optional = renterRepository.findById(renterId);
        if (optional.isPresent()) {
            Renter renter = optional.get();
            renter.setDeleted(true);
            renter.setDeletedOn(Calendar.getInstance());
            renterRepository.save(renter);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public List<RenterListDto> getAllRenterLists() {
        List<Renter> renterList = (List<Renter>) renterRepository.findAllByActiveAndDeleted(true, false);
        return renterListConverter.entityToDtoList(renterList);
    }

    @Override
    public List<RenterDto> getAllRenterListPages() {
        List<Renter> renterList = (List<Renter>) renterRepository.findAllByActiveAndDeleted(true, false);
        return renterConverter.entityToDtoList(renterList);
    }

}
