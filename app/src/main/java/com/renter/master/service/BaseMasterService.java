package com.renter.master.service;

import com.renter.master.dto.HouseDto;

import java.util.List;

public interface BaseMasterService<D> {
    List<D> findAll();

    D findById(Long id);

    D save(D dto);

    Boolean deleteById(Long id);

    List<D> findAllActiveAndUnDeleted();
}