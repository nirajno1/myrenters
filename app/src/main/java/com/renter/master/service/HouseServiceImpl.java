package com.renter.master.service;

import com.renter.master.converter.HouseConverter;
import com.renter.master.dto.HouseDto;
import com.renter.master.dto.dropdown.RentFlatDropDownDto;
import com.renter.master.entity.House;
import com.renter.master.entity.Renter;
import com.renter.master.repository.HouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * HouseServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class HouseServiceImpl implements HouseService {
    @Autowired
    private HouseRepository houseRepository;

    @Autowired
    private HouseConverter houseConverter;

    @Override
    public List<HouseDto> findAll() {
        return houseConverter.entityToDtoList(houseRepository.findAll());
    }

    @Override
    public HouseDto findById(Long id) {
        Optional<House> optionalHouse= houseRepository.findById(id);
        if(optionalHouse.isPresent()){
            return houseConverter.entityToDto(optionalHouse.get());
        }else {
            return null;
        }
    }

    @Override
    public HouseDto save(HouseDto houseDto) {
        House house=houseConverter.dtoToEntity(houseDto);
        if(house.getId()==null){
            house.setCreatedBy("System"); //Todo add real user
            house.setCreatedOn(Calendar.getInstance());
            house.setActive(Boolean.TRUE);
            house.setDeleted(Boolean.FALSE);
        }else{
            house.setModifiedBy("System"); //Todo add real user
            house.setModifiedOn(Calendar.getInstance());
        }
        house = houseRepository.save(house);
        return  houseConverter.entityToDto(house);
    }

    @Override
    public Boolean deleteById(Long houseId) {
        Optional<House> optional=houseRepository.findById(houseId);
        if(optional.isPresent()){
            House house=optional.get();
            if(house.getDeleted()){
                return Boolean.FALSE;
            }
            house.setDeleted(true);
            house.setDeletedOn(Calendar.getInstance());
            houseRepository.save(house);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public List<HouseDto> findAllActiveAndUnDeleted() {
        List<House> houseList= houseRepository.findAllByActiveAndDeleted(true,false);
        return houseConverter.entityToDtoList(houseList);
    }

    @Override
    public List<RentFlatDropDownDto> getAvailableFlatsByHouseId(Long id) {
        return houseRepository.getAvailableFlatsByHouseId(id);
    }
}
