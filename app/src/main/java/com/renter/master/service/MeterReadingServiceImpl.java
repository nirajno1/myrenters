package com.renter.master.service;

import com.renter.master.converter.ElectricMeterConverter;
import com.renter.master.converter.MeterReadingConverter;
import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.entity.ElectricMeter;
import com.renter.master.entity.Image;
import com.renter.master.entity.MeterReading;
import com.renter.master.repository.ElectricMeterRepository;
import com.renter.master.repository.ImageRepository;
import com.renter.master.repository.MeterReadingRepository;
import com.renter.transaction.service.InvoiceService;
import com.renter.transaction.util.MeterReadingDateDescComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.renter.transaction.common.Constants.USER_ID;

/**
 * MeterReadingServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class MeterReadingServiceImpl implements MeterReadingService {
    @Autowired
    private MeterReadingRepository meterReadingRepository;

    @Autowired
    private MeterReadingConverter meterReadingConverter;
    @Autowired
    private ElectricMeterConverter electricMeterConverter;
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private ElectricMeterRepository electricMeterRepository;

    @Autowired
    private InvoiceService invoiceService;

    public List<MeterReadingDto> findAllMeterReadings() {
        List<com.renter.master.entity.MeterReading> meterReadings=meterReadingRepository.findAll();

        List<MeterReadingDto> meterReadingDtos= meterReadingConverter.entityToDtoList(meterReadings);

        return meterReadingDtos;
    }

    @Override
    public MeterReadingDto findMeterReadingById(Long id) {
        Optional<com.renter.master.entity.MeterReading> optionalMeterReading= meterReadingRepository.findById(id);
        if(optionalMeterReading.isPresent()){
            return meterReadingConverter.entityToDto(optionalMeterReading.get());
        }else {
            return null;
        }
    }

    @Override
    public MeterReadingDto createMeterReading(MeterReadingDto meterReadingDto) {
        com.renter.master.entity.MeterReading meterReading=meterReadingConverter.dtoToEntity(meterReadingDto);
        final var electricMeter = electricMeterRepository.findById(meterReading.getElectricMeter().getId()).orElseThrow();
        final var meterReadings = electricMeter.getMeterReadings();
        meterReading.setElectricMeter(electricMeter);
        if(!meterReadings.isEmpty()) {
            MeterReading previousMeterReading= getPreviousMeterReading(meterReadings);
            meterReading.setPreviousReading(previousMeterReading);
        }

        if(Objects.nonNull(meterReading.getMeterImage())) {
            if (Objects.nonNull(meterReading.getMeterImage().getId())) {
                final var imageOptional = imageRepository.findById(meterReading.getMeterImage().getId());
                if (imageOptional.isPresent()) {
                    imageOptional.get().setImgBase64(meterReading.getMeterImage().getImgBase64());
                    imageOptional.get().setImgBase64Prefix(meterReading.getMeterImage().getImgBase64Prefix());
                    Image img = imageRepository.save(imageOptional.get());
                    meterReading.setMeterImage(img);
                }
            } else {
                Image img = imageRepository.save(meterReading.getMeterImage());
                meterReading.setMeterImage(img);
            }
        }
        Long renterId= electricMeterRepository.findRenterIdByEMeterId(electricMeter.getId());
        meterReading.setConsumedByRenter(renterId);

        meterReading.initAudit(USER_ID);
        meterReading = meterReadingRepository.save(meterReading);
        meterReadings.add(meterReading);
        electricMeterRepository.save(electricMeter);

        /* Generate invoice */

        invoiceService.generateInvoice(meterReading);

        return  meterReadingConverter.entityToDto(meterReading);
    }

    private MeterReading getPreviousMeterReading(List<MeterReading> meterReadings) {
        final var latestMeterReading = meterReadings.stream()
                .sorted(new MeterReadingDateDescComparator())
                .findFirst().orElseThrow();
        return latestMeterReading;
    }
}
