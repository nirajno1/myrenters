package com.renter.master.service;

import com.renter.master.converter.FlatConverter;
import com.renter.master.dto.flat.FlatDto;
import com.renter.master.entity.ElectricMeter;
import com.renter.master.entity.Flat;
import com.renter.master.repository.ElectricMeterRepository;
import com.renter.transaction.entity.payment.Invoice;
import com.renter.master.repository.FlatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static com.renter.transaction.common.Constants.USER_ID;

/**
 * FlatServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class FlatServiceImpl implements FlatService {
    @Autowired
    private FlatRepository flatRepository;
    @Autowired
    private FlatConverter flatConverter;

    @Autowired
    ElectricMeterRepository electricMeterRepository;

    @Override
    public List<FlatDto> findAll() {
        return flatConverter.entityToDtoList(flatRepository.findAll());
    }

    @Override
    public FlatDto findById(Long id) {
        Optional<Flat> optionalFlat = flatRepository.findById(id);
        if (optionalFlat.isPresent()) {
            return flatConverter.entityToDto(optionalFlat.get());
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public FlatDto save(FlatDto flatDto) {
        Flat flat = flatConverter.dtoToEntity(flatDto);
        flat.initAudit(USER_ID);
        flat = flatRepository.save(flat);
        return flatConverter.entityToDto(flat);
    }
    @Override
    public Boolean deleteById(Long id) {
        Optional<Flat> optional = flatRepository.findById(id);
        if (optional.isPresent()) {
            Flat flat = optional.get();
            if (flat.getDeleted()) {
                return Boolean.FALSE;
            }
            flat.setDeleted(true);
            flat.setDeletedOn(Calendar.getInstance());
            flatRepository.save(flat);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public List<FlatDto> findAllActiveAndUnDeleted() {
        List<Flat> houseList = flatRepository.findAllByActiveAndDeleted(true, false);
        return flatConverter.entityToDtoList(houseList);
    }


    private static Invoice initMonthlyRent(Invoice monthlyRentCurrent, Invoice oldMonthlyRent, Flat flat) {
        Calendar lastMonthRentFrom = (oldMonthlyRent != null) ? oldMonthlyRent.getToDate() : Calendar.getInstance();
        monthlyRentCurrent.setFromDate(lastMonthRentFrom);
        Calendar cal = Calendar.getInstance();
        cal.setTime(lastMonthRentFrom.getTime());
        cal.add(Calendar.MONTH, 1);
        monthlyRentCurrent.setToDate(cal);

        monthlyRentCurrent.setAmount(flat.getRentAmount());

        monthlyRentCurrent.setPaid(Boolean.FALSE);
        return monthlyRentCurrent;
    }
}
