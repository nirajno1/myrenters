package com.renter.master.service;

import com.renter.master.dto.flat.FlatTypeDto;

import java.util.List;

public interface FlatTypeService {

    List<FlatTypeDto> findAllFlatTypes();

    FlatTypeDto findFlatTypeById(Integer id);

    FlatTypeDto createFlatType(FlatTypeDto flatTypeDto);
}
