package com.renter.master.service;

import com.renter.master.dto.renter.RenterDto;
import com.renter.master.dto.renter.RenterListDto;

import java.util.List;

public interface RenterService {
    List<RenterDto> getAllRenters();

    RenterDto findRenterById(Long id);

    RenterDto saveRenter(RenterDto renterDto);

    Boolean deleteRenter(Long id);

    List<RenterListDto> getAllRenterLists();

    List<RenterDto> getAllRenterListPages();
}
