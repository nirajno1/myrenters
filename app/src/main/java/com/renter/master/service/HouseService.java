package com.renter.master.service;

import com.renter.master.dto.HouseDto;
import com.renter.master.dto.dropdown.RentFlatDropDownDto;

import java.util.List;

public interface HouseService extends BaseMasterService<HouseDto> {

    List<RentFlatDropDownDto> getAvailableFlatsByHouseId(Long id);
}
