package com.renter.master.service;

import com.renter.master.dto.ElectricMeterResponse;
import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.em.MeterReadingDto;

import java.util.List;

public interface ElectricMeterService extends BaseMasterService<ElectricMeterDto>{

    List<MeterReadingDto> findAllMeterReadingsByMeterId(Long id);

    ElectricMeterDto findElectricMeterBySn(String sn);

    List<ElectricMeterDto> findAllUnUsed();

    List<ElectricMeterResponse> findAllUsed();

    MeterReadingDto findLatestMeterReadingsByMeterId(Long id);
}
