package com.renter.master.service;

import com.renter.master.dto.DropDownListDto;
import com.renter.master.dto.ValueObjectDto;

import java.util.List;

public interface DropDownListService extends BaseMasterService<ValueObjectDto> {

    List<DropDownListDto> getAllDropdownList();

    DropDownListDto saveDropdownList(DropDownListDto dropDownListDto);
    ValueObjectDto findById(Long id);

    List<ValueObjectDto> getAllVoByDdKey(String ddKey);

    List<String> getAllKeys();

    List<ValueObjectDto> findAllValidByDdKey(String ddVal);
}

