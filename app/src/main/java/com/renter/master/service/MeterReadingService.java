package com.renter.master.service;

import com.renter.master.dto.em.MeterReadingDto;

import java.util.List;

public interface MeterReadingService {
    List<MeterReadingDto> findAllMeterReadings();


    MeterReadingDto findMeterReadingById(Long id);

    MeterReadingDto createMeterReading(MeterReadingDto meterReadingDto);
}
