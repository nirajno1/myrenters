package com.renter.master.service;

import com.renter.master.converter.StateConverter;
import com.renter.master.dto.master.StateDto;
import com.renter.master.entity.master.State;
import com.renter.master.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ElectricMeterServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class StateServiceImpl implements StateService {

    @Autowired
    private StateRepository stateRepository;
    @Autowired
    private StateConverter stateConverter;
    @Override
    public List<StateDto> getAllStateByCountryCode(String countryCode) {
        List<State> states=stateRepository.findAllByCountryCode(countryCode);
        return stateConverter.entityToDtoList(states);
    }
}
