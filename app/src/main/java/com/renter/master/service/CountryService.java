package com.renter.master.service;

import com.renter.master.dto.master.CountryDto;

import java.util.List;

public interface CountryService {
    List<CountryDto> getAllCountries();
}
