package com.renter.master.service;

import com.renter.master.converter.ElectricMeterConverter;
import com.renter.master.converter.MeterReadingConverter;
import com.renter.master.dto.ElectricMeterResponse;
import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.entity.ElectricMeter;
import com.renter.master.repository.ElectricMeterRepository;
import com.renter.master.repository.MeterReadingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * ElectricMeterServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class ElectricMeterServiceImpl implements ElectricMeterService {
    @Autowired
    private ElectricMeterRepository electricMeterRepository;

    @Autowired
    private ElectricMeterConverter electricMeterConverter;
    @Autowired
    private MeterReadingConverter meterReadingConverter;

    @Autowired
    private MeterReadingRepository meterReadingRepository;

    public List<ElectricMeterDto> findAll() {
        List<ElectricMeter> electricMetersList=electricMeterRepository.findAll();
        List<ElectricMeterDto> returnList= new ArrayList<>();
        for(ElectricMeter em:electricMetersList){
            ElectricMeterDto dto= electricMeterConverter.entityToDto(em);
            //dto.setMeterReadings(meterReadingConverter.entityToDtoList(em.getMeterReadings()));
            returnList.add(dto);
        }
        return returnList;
    }

    @Override
    public ElectricMeterDto findById(Long id) {
        Optional<ElectricMeter> optionalElectricMeter= electricMeterRepository.findById(id);
        if(optionalElectricMeter.isPresent()){
            return electricMeterConverter.entityToDto(optionalElectricMeter.get());
        }else {
            return null;
        }
    }

    @Override
    public ElectricMeterDto save(ElectricMeterDto electricMeterDto) {
        ElectricMeter electricMeter=electricMeterConverter.dtoToEntity(electricMeterDto);
        final var meterReadings = electricMeter.getMeterReadings();
        if(meterReadings != null && !meterReadings.isEmpty()) {
            meterReadingRepository.saveAll(meterReadings);
        }
        if(electricMeter.getId()==null){
            electricMeter.setCreatedBy("System"); //Todo add real user
            electricMeter.setCreatedOn(Calendar.getInstance());
            electricMeter.setActive(Boolean.TRUE);
            electricMeter.setDeleted(Boolean.FALSE);
        }else{
            electricMeter.setModifiedBy("System"); //Todo add real user
            electricMeter.setModifiedOn(Calendar.getInstance());
        }
        electricMeter = electricMeterRepository.save(electricMeter);
        return  electricMeterConverter.entityToDto(electricMeter);
    }

    @Override
    public Boolean deleteById(Long id) {
        Optional<ElectricMeter> optional=electricMeterRepository.findById(id);
        if(optional.isPresent()){
            ElectricMeter electricMeter=optional.get();
            if(electricMeter.getDeleted()){
                return Boolean.FALSE;
            }
            electricMeter.setDeleted(true);
            electricMeter.setDeletedOn(Calendar.getInstance());
            electricMeterRepository.save(electricMeter);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public List<ElectricMeterDto> findAllActiveAndUnDeleted() {
        List<ElectricMeter> electricMeters= electricMeterRepository.findAllByActiveAndDeleted(true,false);
        return electricMeterConverter.entityToDtoList(electricMeters);
    }
    @Override
    public List<ElectricMeterDto> findAllUnUsed() {
        List<ElectricMeter> electricMeters= electricMeterRepository.findAllByActiveAndDeletedAndUsed(true,false,false);
        return electricMeterConverter.entityToDtoList(electricMeters);
    }
    @Override
    public List<ElectricMeterResponse> findAllUsed() {
        List<ElectricMeter> electricMeters= electricMeterRepository.findAllByActiveAndDeletedAndUsed(true,false,true);
        return electricMeters.stream().map(em-> new ElectricMeterResponse(em.getId(),em.getSerialNumber(),em.getNickName())).toList();
    }

    @Override
    public MeterReadingDto findLatestMeterReadingsByMeterId(Long id) {
        final var latestMeterReadingsByMeterId = meterReadingRepository.findLatestMeterReadingsByMeterId(id);
        return meterReadingConverter.entityToDto(latestMeterReadingsByMeterId);
    }

    @Override
    public List<MeterReadingDto> findAllMeterReadingsByMeterId(Long id) {
       // List<com.renter.master.entity.MeterReading> meterReadings=electricMeterRepository.findAllMeterReadingsById(id);
        List<com.renter.master.entity.MeterReading> meterReadings= meterReadingRepository.findAllByElectricMeterId(id);
        return meterReadingConverter.entityToDtoList(meterReadings);
    }

    @Override
    public ElectricMeterDto findElectricMeterBySn(String sn) {
         ElectricMeter em=electricMeterRepository.findBySerialNumber(sn);
        ElectricMeterDto dto=null;
         if(em != null) {
             dto = electricMeterConverter.entityToDto(em);
         }
        return dto;
    }
}
