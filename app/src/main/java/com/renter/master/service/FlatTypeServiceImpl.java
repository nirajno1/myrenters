package com.renter.master.service;

import com.renter.master.converter.FlatTypeConverter;
import com.renter.master.dto.flat.FlatTypeDto;
import com.renter.master.entity.FlatType;
import com.renter.master.repository.FlatTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * FlatServiceImpl of demo
 *
 * @since 11-Dec-2022 3:50 PM
 */
@Service
public class FlatTypeServiceImpl implements FlatTypeService {
    @Autowired
    private FlatTypeRepository flatTypeRepository;

    @Autowired
    private FlatTypeConverter flatTypeConverter;

    public List<FlatTypeDto> findAllFlatTypes() {
        return flatTypeConverter.entityToDtoList(flatTypeRepository.findAll());
    }

    @Override
    public FlatTypeDto findFlatTypeById(Integer id) {
        Optional<FlatType> optionalFlat= flatTypeRepository.findById(id);
        if(optionalFlat.isPresent()){
            return flatTypeConverter.entityToDto(optionalFlat.get());
        }else {
            return null;
        }
    }

    @Override
    public FlatTypeDto createFlatType(FlatTypeDto flatTypeDto) {
        FlatType flatType= flatTypeConverter.dtoToEntity(flatTypeDto);
        flatType = flatTypeRepository.save(flatType);
        return  flatTypeConverter.entityToDto(flatType);
    }
}
