package com.renter.master.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * DropDownList of demo
 *
 * @author Neeraj Kumar
 * @since 23-Mar-2023 1:52 PM
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
public class DropDownList {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String displayName;
    private String description;
    @OneToMany(cascade = CascadeType.ALL)
    private List<ValueObject> values;
}
