package com.renter.master.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Calendar;

/**
 * Audit of my-renter
 *
 * @author Neeraj Kumar
 * @since 29-Mar-2023 11:11 AM
 */
@MappedSuperclass
@Getter
@Setter
public class Audit {
    private Calendar createdOn;
    private Calendar modifiedOn;
    private Calendar deletedOn;
    private String createdBy;
    private String modifiedBy;
    private Boolean active = Boolean.TRUE;
    private Boolean deleted = Boolean.FALSE;

    public void initAudit(Long id, String userId){
        if(id==null){
            setCreatedBy(userId); 
            setCreatedOn(Calendar.getInstance());
            setActive(Boolean.TRUE);
            setDeleted(Boolean.FALSE);
        }else{
            setModifiedBy(userId);
            setModifiedOn(Calendar.getInstance());
        }
    }
}
