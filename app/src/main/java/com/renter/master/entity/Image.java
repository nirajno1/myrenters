package com.renter.master.entity;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Image of my-renter
 *
 * @author Neeraj Kumar
 * @since 24-May-2023 5:28 PM
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
public class Image {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String imgBase64Prefix;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] imgBase64;
}
