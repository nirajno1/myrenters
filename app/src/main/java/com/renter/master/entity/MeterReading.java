package com.renter.master.entity;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;

import static jakarta.persistence.FetchType.LAZY;

/**
 * MeterReading Class
 *
 * @since 11-Dec-2022 10:06 AM
 */
@Entity
@Setter @Getter @NoArgsConstructor
public class MeterReading extends Audit{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private Integer currentReading;
    private Calendar currentReadingDate;
    private Long consumedByRenter;
    private Boolean paid = Boolean.FALSE;

    @ManyToOne
    @JoinColumn(name = "electric_meter_id")
    private ElectricMeter electricMeter;

    @OneToOne(fetch = LAZY)
    @JoinColumn(name = "image_id")
    private Image meterImage;

    @OneToOne(fetch = LAZY)
    @JoinColumn(name = "previous_reading_id")
    private MeterReading previousReading;

    public void initAudit(String userId) {
        super.initAudit(id, userId);
    }

}
