package com.renter.master.entity;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Address of demo
 *
 * @since 11-Dec-2022 10:22 AM
 */
@Embeddable
@Setter @Getter @NoArgsConstructor
public class Address {
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;

    private String distric;
    private String state;
    private String pincode;

}
