package com.renter.master.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Flat of demo
 *
 * @since 11-Dec-2022 10:06 AM
 */
@Entity
@Setter @Getter @NoArgsConstructor
public class ElectricMeter extends Audit {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private String serialNumber;
    private String type;
    private String nickName;
    private Boolean active = Boolean.TRUE;
    private Boolean used = Boolean.FALSE;

    @OneToMany(mappedBy = "electricMeter", cascade = CascadeType.ALL)
    private List<MeterReading> meterReadings = new java.util.ArrayList<>();
}
