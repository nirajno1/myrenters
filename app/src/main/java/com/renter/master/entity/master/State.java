package com.renter.master.entity.master;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * CountryDto of demo
 *
 * @since 11-Jan-2023 3:29 PM
 */
@Entity
@Setter
@Getter
@NoArgsConstructor

@ToString
public class State {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String countryCode;
    private String code;
    private String name;
    private String type;
}
