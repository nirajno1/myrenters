package com.renter.master.entity;

import com.renter.transaction.entity.payment.Invoice;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Calendar;
import java.util.List;

import static jakarta.persistence.FetchType.LAZY;

/**
 * Renter of demo
 *
 * @since 11-Dec-2022 10:05 AM
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Renter extends Audit {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String mobileNumber;
    private String occupation;
    private String address;
    private String city;
    private String state;

    @OneToOne(fetch = LAZY)
    @JoinColumn(name = "image_id")
    private Image renterImage;
    private Long renterImageId;

    private Calendar dob;
    private Boolean vacated = Boolean.FALSE;
    private Calendar vacatedOn;

    @OneToMany
    private List<Invoice> invoices;
    public void initAudit(String userId) {
        super.initAudit(id, userId);
    }
}
