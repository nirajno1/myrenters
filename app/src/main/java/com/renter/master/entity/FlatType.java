package com.renter.master.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Flat of demo
 *
 * @since 11-Dec-2022 10:06 AM
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
public class FlatType {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String flatTypeCode;
    private String flatTypeName;
    private String description;

    public FlatType(String flatTypeCode,String flatTypeName, String description) {
        this.flatTypeName = flatTypeName;
        this.flatTypeCode=flatTypeCode;
        this.description = description;
    }
}
