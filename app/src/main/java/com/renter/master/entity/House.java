package com.renter.master.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * House of demo
 *
 * @author Neeraj Kumar
 * @since 22-Mar-2023 1:16 PM
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
public class House extends Audit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;
    private String number;

    private String address;
    private String city;
    private String state;

    @OneToMany(mappedBy = "house")
    private List<Flat> flats;

}
