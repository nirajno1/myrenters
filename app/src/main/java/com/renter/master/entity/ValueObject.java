package com.renter.master.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * ValueObject of demo
 *
 * @author Neeraj Kumar
 * @since 23-Mar-2023 1:54 PM
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "unique_key_value",columnNames = {"ddKey","ddValue"})
})
public class ValueObject extends Audit {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "category can not be null")
    private String ddKey;
    @NotNull(message = "Dropdown value can not be null")
    private String ddValue;
    @NotNull(message = "Dropdown display value can not be null")
    private String display;

    private Boolean defaultVal;
}
