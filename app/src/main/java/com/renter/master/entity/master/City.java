package com.renter.master.entity.master;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * City of demo
 *
 * @since 12-Jan-2023 10:30 AM
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
public class City {
   @Id
   @Column(name = "id", nullable = false)
   @GeneratedValue(strategy= GenerationType.IDENTITY)
   private Long id;
   private String countryCode;
   private String postalCode;
   private String placeName;
   private String stateName;
   private String stateCode;
   private String cityName;
   private String cityCode;
   private String communityName;
   private String communityCode;
   private String latitude;
   private String longitude;
   private String accuracy;


}
