package com.renter.master.entity;

import com.renter.transaction.entity.payment.Invoice;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Flat of demo
 *
 * @since 11-Dec-2022 10:06 AM
 */
@Entity
@Setter @Getter @NoArgsConstructor
public class Flat extends Audit{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String flatNumber;
    private Double rentAmount;
    private Boolean occupied;

    private String flatType;

    @OneToOne
    private ElectricMeter electricMeter;

    @OneToMany(cascade =  CascadeType.ALL)
    private List<Invoice> invoices;

    @ManyToOne
    private House house;

    public void initAudit(String userId) {
        super.initAudit(id, userId);
    }
}
