package com.renter.master.controller;

import com.renter.master.dto.DropDownListDto;
import com.renter.master.dto.ValueObjectDto;
import com.renter.master.dto.dropdown.EmeterDropDownDto;
import com.renter.master.dto.dropdown.FlatDropDownDto;
import com.renter.master.dto.dropdown.RenterDropDownDto;
import com.renter.master.service.DropDownListService;
import com.renter.transaction.service.EmSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * DropDownController of demo
 *
 * @author Neeraj
 * @since 22-Feb-2023 3:00 PM
 */
@RestController
@RequestMapping(value = "/dropdown")
public class DropDownController implements BaseMasterController<ValueObjectDto>{
    @Autowired
    private EmSearchService emSearchService;

    @Autowired
    private DropDownListService dropDownListService;

    @GetMapping(value = "/emeters")
    public ResponseEntity<List<EmeterDropDownDto>> findAllDropdownMeters() {
        final var electricMeters = emSearchService.findAllElectricMeters();
        return Objects.nonNull(electricMeters)?new ResponseEntity<>(electricMeters,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/flats")
    public ResponseEntity<List<FlatDropDownDto>> findAllDropdownFlats() {
        final var flats = emSearchService.findAllFlats();
        return Objects.nonNull(flats)?new ResponseEntity<>(flats,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/renters")
    public ResponseEntity<List<RenterDropDownDto>> findAllDropdownRenters() {
        final var renters = emSearchService.findAllRenters();
        return Objects.nonNull(renters)?new ResponseEntity<>(renters,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/dd/{ddKey}")
    public ResponseEntity<List<ValueObjectDto>> getDropdownList(@PathVariable String ddKey) {
        List<ValueObjectDto> dropDownListDtos=dropDownListService.getAllVoByDdKey(ddKey);
        return Objects.nonNull(dropDownListDtos)?new ResponseEntity<>(dropDownListDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/dd")
    @CrossOrigin(origins = "http://localhost:8090/dropdown")
    public ResponseEntity<List<DropDownListDto>> getAllDropdownList() {
        List<DropDownListDto> dropDownListDtos=dropDownListService.getAllDropdownList();
        return Objects.nonNull(dropDownListDtos)?new ResponseEntity<>(dropDownListDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PostMapping(value = "/dd")
    public ResponseEntity<DropDownListDto> saveDropdownList(@RequestBody DropDownListDto dropDownListDto) {
        DropDownListDto dropDownList = dropDownListService.saveDropdownList(dropDownListDto);
        return Objects.nonNull(dropDownList)?new ResponseEntity<>(dropDownList,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/ddvals/{ddkey}")
    public ResponseEntity< List<ValueObjectDto>> findAll(@PathVariable("ddkey") String ddkey) {
        List<ValueObjectDto> ddValDto =  dropDownListService.getAllVoByDdKey(ddkey);
        return Objects.nonNull(ddValDto)?new ResponseEntity<>(ddValDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<List<ValueObjectDto>> findAll() {
        return null;
    }

    @GetMapping(value = "/ddval/{id}")
    public ResponseEntity< ValueObjectDto> findById(@PathVariable("id") Long id) {
        ValueObjectDto ddValDto =  dropDownListService.findById(id);
        return Objects.nonNull(ddValDto)?new ResponseEntity<>(ddValDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<List<ValueObjectDto>> findAllActiveAndUnDeleted() {
        return null;
    }

    @GetMapping(value = "/ddval/valid/{ddkey}")
    public ResponseEntity< List<ValueObjectDto>> findAllValidByDdKey(@PathVariable String ddkey) {
        List<ValueObjectDto> ddValDto =  dropDownListService.findAllValidByDdKey(ddkey);
        return Objects.nonNull(ddValDto)?new ResponseEntity<>(ddValDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/dd/{name}/ddval")
    public ResponseEntity<List<ValueObjectDto>> getDropdownListValues(@PathVariable String name) {
        List<ValueObjectDto> dropDownListDtos=dropDownListService.getAllVoByDdKey(name);
        return Objects.nonNull(dropDownListDtos)?new ResponseEntity<>(dropDownListDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    @PostMapping
    public ResponseEntity<ValueObjectDto> save(@RequestBody ValueObjectDto dto) {
        ValueObjectDto valueObjectDto = dropDownListService.save(dto);
        return Objects.nonNull(valueObjectDto) ? new ResponseEntity<>(valueObjectDto, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        boolean deleted= dropDownListService.deleteById(id);
        return  deleted?new ResponseEntity<>("Deleted Successfully",HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/dd/ddKeys")
    public ResponseEntity<List<String>> getAllKeys() {
        List<String> keys=dropDownListService.getAllKeys();
        return Objects.nonNull(keys)?new ResponseEntity<>(keys,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
