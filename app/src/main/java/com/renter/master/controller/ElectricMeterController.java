package com.renter.master.controller;

import com.renter.master.dto.ElectricMeterResponse;
import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.dto.em.MeterReadingCaptureDto;
import com.renter.master.service.ElectricMeterService;
import com.renter.master.service.MeterReadingService;
import com.renter.transaction.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * RentPaymentController of demo
 *
 * @since 11-Dec-2022 3:47 PM
 */
@RestController
@RequestMapping(value = "/elecmeter")
public class ElectricMeterController implements BaseMasterController<ElectricMeterDto> {

    @Autowired
    private ElectricMeterService electricMeterService;
    @Autowired
    private MeterReadingService meterReadingService;

    @Autowired
    private PaymentService paymentService;
    @GetMapping(value = "/health")
    public ResponseEntity<String> healthTest() {
    return new ResponseEntity<>("ElectricMeterController Health is fine",HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<ElectricMeterDto>> findAll() {
        return new ResponseEntity<>(electricMeterService.findAll(),HttpStatus.OK);
    }

    @GetMapping("/{id}/readings")
    @CrossOrigin(origins = "http://localhost:8090/elecmeter")
    public ResponseEntity<List<MeterReadingDto>> findMeterReadingById(@PathVariable Long id) {
        List<MeterReadingDto> meterReadingDtos=electricMeterService.findAllMeterReadingsByMeterId(id);
        return Objects.nonNull(meterReadingDtos)?new ResponseEntity<>(meterReadingDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping("/{id}")
    public ResponseEntity<ElectricMeterDto> findById(@PathVariable Long id) {
        ElectricMeterDto electricMeterDto=electricMeterService.findById(id);
        return Objects.nonNull(electricMeterDto)?new ResponseEntity<>(electricMeterDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    @GetMapping("/valid")
    public ResponseEntity<List<ElectricMeterDto>> findAllActiveAndUnDeleted() {
        List<ElectricMeterDto> electricMeterDtos=electricMeterService.findAllActiveAndUnDeleted();
        return Objects.nonNull(electricMeterDtos)?new ResponseEntity<>(electricMeterDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/unused")
    public ResponseEntity<List<ElectricMeterDto>> findAllUnUsed() {
        List<ElectricMeterDto> electricMeterDtos=electricMeterService.findAllUnUsed();
        return Objects.nonNull(electricMeterDtos)?new ResponseEntity<>(electricMeterDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PostMapping()
    public ResponseEntity<ElectricMeterDto> save(@RequestBody ElectricMeterDto electricMeterDto) {
        electricMeterDto=electricMeterService.save(electricMeterDto);
        return Objects.nonNull(electricMeterDto)?new ResponseEntity<>(electricMeterDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @PostMapping("/em")
    @CrossOrigin(origins = "http://localhost:8090/elecmeter/em")
    public ResponseEntity<ElectricMeterDto> findElectricMeterBySn(@RequestBody MeterReadingCaptureDto meterReadingCaptureDto) {
        electricMeterService.findElectricMeterBySn(meterReadingCaptureDto.getMeterSn());
        ElectricMeterDto electricMeterDto = paymentService.calculateElectricBill(meterReadingCaptureDto);
        return Objects.nonNull(electricMeterDto) ? new ResponseEntity<>(electricMeterDto, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        Boolean deleted= electricMeterService.deleteById(id);
        return  deleted?new ResponseEntity<>("Deleted Successfully",HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/used")
    public ResponseEntity<List<ElectricMeterResponse>> findAllUsed() {
        List<ElectricMeterResponse> electricMeterDtos=electricMeterService.findAllUsed();
        return Objects.nonNull(electricMeterDtos)?new ResponseEntity<>(electricMeterDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}/latest")
    @CrossOrigin(origins = "http://localhost:8090/elecmeter")
    public ResponseEntity<MeterReadingDto> findLatestMeterReadingById(@PathVariable Long id) {
        MeterReadingDto meterReadingDtos=electricMeterService.findLatestMeterReadingsByMeterId(id);
        return Objects.nonNull(meterReadingDtos)?new ResponseEntity<>(meterReadingDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
