package com.renter.master.controller;

import com.renter.master.dto.em.MeterReadingDto;
import com.renter.master.service.MeterReadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * RentPaymentController of demo
 *
 * @since 11-Dec-2022 3:47 PM
 */
@RestController
@RequestMapping(value = "/meterReading")
public class MeterReadingController {

    @Autowired
    private MeterReadingService meterReadingService;

    @GetMapping(value = "/health")
    public ResponseEntity<String> healthTest() {
    return new ResponseEntity<>("MeterReadingController Health is fine",HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<MeterReadingDto>> findAllMeterReadings() {
        return new ResponseEntity<>(meterReadingService.findAllMeterReadings(),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MeterReadingDto> findMeterReadingById(@PathVariable Long id) {
        MeterReadingDto meterReadingDto=meterReadingService.findMeterReadingById(id);
        return Objects.nonNull(meterReadingDto)?new ResponseEntity<>(meterReadingDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PostMapping()
    public ResponseEntity<MeterReadingDto> createMeterReading(@RequestBody MeterReadingDto meterReadingDto) {
        meterReadingDto=meterReadingService.createMeterReading(meterReadingDto);
        return Objects.nonNull(meterReadingDto)?new ResponseEntity<>(meterReadingDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
