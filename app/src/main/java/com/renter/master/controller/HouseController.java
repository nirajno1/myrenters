package com.renter.master.controller;

import com.renter.master.dto.HouseDto;
import com.renter.master.dto.dropdown.RentFlatDropDownDto;
import com.renter.master.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * RentPaymentController of demo
 *
 * @since 11-Dec-2022 3:47 PM
 */
@RestController
@RequestMapping(value = "/house")
public class HouseController implements BaseMasterController<HouseDto>{

    @Autowired
    private HouseService houseService;

    @GetMapping(value = "/health")
    public ResponseEntity<String> healthTest() {
    return new ResponseEntity<>("HouseController Health is fine",HttpStatus.OK);
    }

    @GetMapping("/valid")
    public ResponseEntity<List<HouseDto>> findAllActiveAndUnDeleted() {
        return new ResponseEntity<>(houseService.findAllActiveAndUnDeleted(),HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<HouseDto>> findAll() {
        return new ResponseEntity<>(houseService.findAll(),HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<HouseDto> findById(@PathVariable Long id) {
        HouseDto houseDto=houseService.findById(id);
        return Objects.nonNull(houseDto)?new ResponseEntity<>(houseDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping("/avlFlats/{id}")
    public ResponseEntity<List<RentFlatDropDownDto> > getAvailableFlatsByHouseId(@PathVariable Long id) {
        List<RentFlatDropDownDto> rentFlatDropDownDtos=houseService.getAvailableFlatsByHouseId(id);
        return Objects.nonNull(rentFlatDropDownDtos)?new ResponseEntity<>(rentFlatDropDownDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PostMapping()
    public ResponseEntity<HouseDto> save(@RequestBody HouseDto houseDto) {
        houseDto=houseService.save(houseDto);
        return Objects.nonNull(houseDto)?new ResponseEntity<>(houseDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        Boolean deleted= houseService.deleteById(id);
        return  deleted?new ResponseEntity<>("Deleted Successfully",HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
