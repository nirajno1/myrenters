package com.renter.master.controller;

import com.renter.master.dto.renter.RenterDto;
import com.renter.master.dto.renter.RenterListDto;
import com.renter.master.service.RenterService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

/**
 * RentPaymentController of demo
 *
 * @since 11-Dec-2022 3:47 PM
 */
@RestController
@RequestMapping(value = "/renter")
public class RenterController {
    @Autowired
    private RenterService renterService;
    @GetMapping(value = "/health")
    public ResponseEntity<String> healthTest() {
        return new ResponseEntity<>("FlatController Health is fine", HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:8090/renter")
    @GetMapping()
    public List<RenterDto> getAllRenters(){
        return renterService.getAllRenters();
    }

    @CrossOrigin(origins = "http://localhost:8090/renter/list")
    @GetMapping("/list")
    public List<RenterListDto> getAllRentersList(){
        return renterService.getAllRenterLists();
    }
    @CrossOrigin(origins = "http://localhost:8090/renter/list")
    @GetMapping("/listPage")
    public ResponseEntity<List<RenterDto>> getAllRentersListTable(){
        final var allRenterListPages = renterService.getAllRenterListPages();
        return Objects.nonNull(allRenterListPages)?new ResponseEntity<>(allRenterListPages,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping("/{id}")
    public ResponseEntity<RenterDto> findRenterById(@PathVariable Long id) {
        RenterDto dto=renterService.findRenterById(id);
        return Objects.nonNull(dto)?new ResponseEntity<>(dto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @GetMapping("/preview/{id}")
    public ResponseEntity<RenterDto> findRenterPreviewById(@PathVariable Long id) {
        RenterDto dto=renterService.findRenterById(id);
        /*if(dto != null && dto.getRentedFlat()!= null && !dto.getRentedFlat().isEmpty()){
            ElectricMeterDto electricMeter=dto.getRentedFlat().get(0).getElectricMeter();
            if(electricMeter!= null && electricMeter.getMeterReadings()!= null){
                List<MeterReadingDto> meterReadingDtos= electricMeter.getMeterReadings().stream()
                        .filter(e->e.getConsumedByRenter().equals(dto.getId()))
                        .limit(5).collect(Collectors.toList());
                electricMeter.setMeterReadings(meterReadingDtos);
            }
        }*/
        return Objects.nonNull(dto)?new ResponseEntity<>(dto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PostMapping()
    public RenterDto saveRenter(@RequestBody RenterDto renterDto){
        return renterService.saveRenter(renterDto);
    }


    @PostMapping("/upload")
    public ResponseEntity<URI> handleFileUpload(@RequestPart(value = "file") final MultipartFile uploadfile) throws IOException {
        return new ResponseEntity<>(saveUploadedFiles(uploadfile),HttpStatus.OK);
    }
    @GetMapping("/img/{name}")
    public ResponseEntity<String> getImageUpload(@PathVariable(value = "name") final String imgfile) throws IOException {
        final String filePath = "../img/"+imgfile;
        String suffix="data:image/png;base64,";
        byte[] fileContent = FileUtils.readFileToByteArray(new File(filePath));
        String encodedString = Base64.getEncoder().encodeToString(fileContent);
        return new ResponseEntity<>(suffix.concat(encodedString),HttpStatus.OK);
    }



    private URI saveUploadedFiles(final MultipartFile file) throws IOException {
        final byte[] bytes = file.getBytes();
        final Path path = Paths.get("../img/"+file.getOriginalFilename());
        Files.write(path, bytes);
        return path.toUri();
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteRenter(@PathVariable Long id){
        Boolean deleted= renterService.deleteRenter(id);
        return  deleted?new ResponseEntity<>("Deleted Successfully",HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
