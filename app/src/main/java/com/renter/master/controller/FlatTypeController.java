package com.renter.master.controller;

import com.renter.master.dto.flat.FlatTypeDto;
import com.renter.master.service.FlatTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * RentPaymentController of demo
 *
 * @since 11-Dec-2022 3:47 PM
 */
@RestController
@RequestMapping(value = "/flatType")
public class FlatTypeController {

    @Autowired
    private FlatTypeService flatTypeService;

    @GetMapping(value = "/health")
    public ResponseEntity<String> healthTest() {
    return new ResponseEntity<>("FlatType Controller Health is fine",HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<FlatTypeDto>> findAllFlats() {
        return new ResponseEntity<>(flatTypeService.findAllFlatTypes(),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FlatTypeDto> findFlatTypeById(@PathVariable Integer id) {
        FlatTypeDto flatTypeDto= flatTypeService.findFlatTypeById(id);
        return Objects.nonNull(flatTypeDto)?new ResponseEntity<>(flatTypeDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PostMapping()
    public ResponseEntity<FlatTypeDto> createFlat(@RequestBody FlatTypeDto flatTypeDto) {
        flatTypeDto= flatTypeService.createFlatType(flatTypeDto);
        return Objects.nonNull(flatTypeDto)?new ResponseEntity<>(flatTypeDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
