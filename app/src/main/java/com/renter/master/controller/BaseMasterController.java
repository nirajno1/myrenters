package com.renter.master.controller;

import org.springframework.http.ResponseEntity;

import java.util.List;

public interface BaseMasterController<D> {

    ResponseEntity<List<D>> findAll();

    ResponseEntity<D> findById(Long id);

    ResponseEntity<List<D>> findAllActiveAndUnDeleted();

    ResponseEntity<D> save(D dto);

    ResponseEntity<String> deleteById(Long id);
}
