package com.renter.master.controller;

import com.renter.master.dto.master.CityDto;
import com.renter.master.dto.master.CountryDto;
import com.renter.master.dto.master.StateDto;
import com.renter.master.service.CityService;
import com.renter.master.service.CountryService;
import com.renter.master.service.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * MasterController of demo
 *
 * @since 11-Jan-2023 5:02 PM
 */
@RestController
@RequestMapping(value = "/master")
public class MasterController {
    @Autowired
    CountryService countryService;
    @Autowired
    StateService stateService;
    @Autowired
    CityService cityService;
    @GetMapping(value = "/health")
    public ResponseEntity<String> healthTest() {
        return new ResponseEntity("MasterController Health is fine", HttpStatus.OK);
    }
    @GetMapping("/countries")
    public List<CountryDto> getAllCountries(){
        return countryService.getAllCountries();
    }
    @GetMapping("/states/{countryCode}")
    public List<StateDto> getAllCountries(@PathVariable String countryCode){
        return stateService.getAllStateByCountryCode(countryCode);
    }
    @GetMapping("/cities/{countryCode}/{state}")
    public List<CityDto> getAllCities(@PathVariable String countryCode, @PathVariable String state){
        return cityService.getAllCityByCountryCodeAndStateName(countryCode,state);
    }
}
