package com.renter.master.controller;

import com.renter.master.dto.em.ElectricMeterDto;
import com.renter.master.dto.flat.FlatDto;
import com.renter.master.service.FlatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

/**
 * RentPaymentController of demo
 *
 * @since 11-Dec-2022 3:47 PM
 */
@RestController
@RequestMapping(value = "/flat")
public class FlatController implements BaseMasterController<FlatDto> {
    @Autowired
    private FlatService flatService;


    @GetMapping()
    public ResponseEntity<List<FlatDto>> findAll() {
        return new ResponseEntity<>(flatService.findAll(),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FlatDto> findById(@PathVariable Long id) {
        FlatDto flatDto=flatService.findById(id);
        return Objects.nonNull(flatDto)?new ResponseEntity<>(flatDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PostMapping()
    public ResponseEntity<FlatDto> save(@RequestBody FlatDto flatDto) {
        flatDto=flatService.save(flatDto);
        return Objects.nonNull(flatDto)?new ResponseEntity<>(flatDto,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        Boolean deleted= flatService.deleteById(id);
        return  deleted?new ResponseEntity<>("Deleted Successfully",HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    @GetMapping("/valid")
    public ResponseEntity<List<FlatDto>> findAllActiveAndUnDeleted() {
        List<FlatDto> flatDtos=flatService.findAllActiveAndUnDeleted();
        return Objects.nonNull(flatDtos)?new ResponseEntity<>(flatDtos,HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
